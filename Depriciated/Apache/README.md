# Docker MAD Team Base

Docker-Compose is used here to spin up multiple services that are commonly used.

Services spun up
* Apache
* PHP
* Redis
* MySql

Docker-Compose
-

The docker-compose.yml file contains the configuration information for each of the services. Using ```docker-compose up -d``` will create and startup each containerized service. 

Further useful commands
```
docker-compose stop // Stops all running containers
docker-compose rm 	// Deletes all containers (Must be stopped)
```

Configuration
-



The Apache server must be completley configured.  You can configure this using ```apache/sites-enabled/defailt-ssl.conf```.  Minor changes need to be made to set this up for Dev/Prod.  Current setup is for local development.

Double check the docker-compose.yml file for further configurations.  For Dev/Prod remove the comments under __Apache -> Volumes__ allowing for a symlink being created to handle SSL Certs.

Defaults
-

Apache is setup for outputting on ports __80/443__

__Redis/PHP/MySql__ are not forwarding any ports externally but however can be accessed internally.

__MySql__ data is stored within the _mysql\_data_ folder.

Dockerfiles
-

These files create images indicated by their names.  
```
Dockerfile.apache 	// Creates our Apache image
Dockerfile.php		// Creates our base PHP image
```

These are setup in a way by taking a base __debian__ image and running commands within the container on build to create an image with these actions already invoked.
