FROM php:5.6-fpm-alpine
MAINTAINER Ruben Castaneda <rubennccsusb@gmail.com>

RUN apk add --no-cache \
        libpng-dev \
    && docker-php-ext-install \
        gd \
        pdo \
        pdo_mysql
