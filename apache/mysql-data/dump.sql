-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: 0.0.0.0    Database: feedback01
-- ------------------------------------------------------
-- Server version	5.5.53-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `PassportData`
--

DROP TABLE IF EXISTS `PassportData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PassportData` (
  `Date` varchar(50) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `StudentID` varchar(100) DEFAULT NULL,
  `Events` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PassportData`
--

LOCK TABLES `PassportData` WRITE;
/*!40000 ALTER TABLE `PassportData` DISABLE KEYS */;
INSERT INTO `PassportData` VALUES ('04/12/2017','Tan Do','005522789','Resume'),('04/12/2017','Jame Smith','0011223344','Workshop01'),('04/12/2017','Tan Do','005259367','Interview'),('04/15/2017','Tan Do','005259367','Interview'),('04/15/2017','Tan Do','005259367','SSC: Business Madness'),('04/15/2017','Tan Do','005259367','SSC: Business Madness'),('04/16/2017','Tan Do','005259367','MBA: E-mail Etiquette'),('04/20/2017','Tan Do','005259367','MBA Oktoberfest'),('04/30/17','Tan Do','005259367','Spririt of the Enterpreneur'),('05/15/2017','Tan Do','005259367','MBA Alumini Speaker'),('05/06/2017','Jame Smith','0011223344','Reading 101'),('05/15/2017','Lauren James','12345678','Business Communication'),('05/25/2017','Eugyo Jeong','0000115','Event0'),('05/12/2017','Eungyo Jeong','0000115','Event2'),('06/10/2017','Rick Nguyen','0000123','Event03'),('06/19/2017','Ricky Stars','9999999','Event#12'),('06/22/2017','Lily Gracia','004577112','event#3');
/*!40000 ALTER TABLE `PassportData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advising`
--

DROP TABLE IF EXISTS `advising`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advising` (
  `FIRSTNAME` varchar(100) DEFAULT NULL,
  `LASTNAME` varchar(100) DEFAULT NULL,
  `STUDENTID` varchar(100) DEFAULT NULL,
  `COYOTEEMAIL` varchar(100) DEFAULT NULL,
  `MESSAGES` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advising`
--

LOCK TABLES `advising` WRITE;
/*!40000 ALTER TABLE `advising` DISABLE KEYS */;
INSERT INTO `advising` VALUES ('Tan ','Do','005259367','005259367@csusb.edu','testing'),('Tim','Cook','0000111','0000111@apple.edu','testing testing!!'),('Laura','Hernandez','0032145','l.hernandez12@gmail.com','Hello MBA teams.'),('Lannister','Starks','22222223','l.starks22@starks.com','how can I become a graduate assistant?');
/*!40000 ALTER TABLE `advising` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appTB`
--

DROP TABLE IF EXISTS `appTB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appTB` (
  `FIRSTNAME` varchar(100) DEFAULT NULL,
  `LASTNAME` varchar(100) DEFAULT NULL,
  `STUDENTID` varchar(100) DEFAULT NULL,
  `COYOTEEMAIL` varchar(100) DEFAULT NULL,
  `DATE` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appTB`
--

LOCK TABLES `appTB` WRITE;
/*!40000 ALTER TABLE `appTB` DISABLE KEYS */;
INSERT INTO `appTB` VALUES ('Tan','Do','005259367','005259367@csusb.edu','THU 05-16-2017 9am'),('tan','do','005259367','005259367@csusb.edu','monday 12/05/2017'),('Tan','Do','005259367','005259367@gmail.com','Person01, Mon 10 am'),('Tan','Do','005259367','tdo000@csusb.edu','Monday 06/08/2017 12pm'),('Tiff','James','0022222','0022222@cse.csusb.net','06/21/17 12p.m.'),('Lily','Gracia','004577112','l.gracia12@gmail.edu','06/21/2017 1p.m.');
/*!40000 ALTER TABLE `appTB` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth`
--

DROP TABLE IF EXISTS `auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth` (
  `passcode` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth`
--

LOCK TABLES `auth` WRITE;
/*!40000 ALTER TABLE `auth` DISABLE KEYS */;
INSERT INTO `auth` VALUES ('MBA89'),('MBA91');
/*!40000 ALTER TABLE `auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dateTB`
--

DROP TABLE IF EXISTS `dateTB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dateTB` (
  `NAME` varchar(100) DEFAULT NULL,
  `DATE` varchar(50) DEFAULT NULL,
  `TIME` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dateTB`
--

LOCK TABLES `dateTB` WRITE;
/*!40000 ALTER TABLE `dateTB` DISABLE KEYS */;
INSERT INTO `dateTB` VALUES ('Person01','MON-THU','9am - 5pm'),('Person02','MON-THU','1pm - 5pm'),('Person03','THU-FRI','9am - 3pm');
/*!40000 ALTER TABLE `dateTB` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedbackTB`
--

DROP TABLE IF EXISTS `feedbackTB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedbackTB` (
  `FIRSTNAME` varchar(100) DEFAULT NULL,
  `LASTNAME` varchar(100) DEFAULT NULL,
  `STUDENTID` varchar(100) DEFAULT NULL,
  `COYOTEEMAIL` varchar(100) DEFAULT NULL,
  `MESSAGES` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedbackTB`
--

LOCK TABLES `feedbackTB` WRITE;
/*!40000 ALTER TABLE `feedbackTB` DISABLE KEYS */;
INSERT INTO `feedbackTB` VALUES ('Tan','Do','005259367','005259367@coyote.com','Testings....'),('Jill','Smith','0022134','j.smith0012@csusb.com','testing testing.'),('Tan','Do','005259367','tantdo89@gmail.com','Hello'),('Tan','Do','005259367','tan@gmail.com','Hello.'),('Tan','Do','123','123','testing'),('Ken','Lee','00000123','00000123@coyote.edu','I would like to be in MBA program.'),('Angela','Smiths','002233112','002233112@csusb.edu','Hello MBA!'),('Lily','Gracia','004587112','l.gracia12@cse.csusb.com','Hi there, I am testing your app.'),('Myro','z','004999913','mzamora@gmail.com','jgjgh');
/*!40000 ALTER TABLE `feedbackTB` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info` (
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_info`
--

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` VALUES ('Tan Do','tantdo89@yahoo.com');
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-20 21:21:13
