<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>PHPMailer - GMail SMTP test</title>
</head>
<body>
<?php

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require 'PHPMailerAutoload.php';

$name = $_POST['Name'];
$email = $_POST['email'];
$infomation = $_POST['additonal'];
$hidden = $_POST['hidden'];

$messages = array($name,$email,$infomation,$hidden);

$body = 'Name: ' . $messages[0] . '<br />' .  'Email: ' . $messages[1] . '<br />'. 'Information: ' . $messages[3];

//Create a new PHPMailer instance
$mail = new PHPMailer();
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
//Set who the message is to be sent from
$mail->setFrom($email, $name);
//Set an alternative reply-to address
$mail->addReplyTo($email, $name);
//Set who the message is to be sent to
$mail->addAddress('bringlstetter@yahoo.com', 'Brandon Ringlstetter');
//Set the subject line
$mail->Subject = 'Contact From Webstie';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->Body = $body;
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//Attach an image file

//send the message, check for errors
if (!$mail->send()) {
    echo '<script type="text/javascript">
		window.alert("Error sending message!");
		window.location.href="../index.html";
	</script>';
} else {
	echo '<script type="text/javascript">
		window.alert("Message sent!");
		window.location.href="../index.html";
	</script>';
}
?>
</body>
</html>