<!--=================================================
RAFFMA APP PAGE
==================================================-->
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/raffma.css">
<title>RAFFMA: A Mobile App for Robert and Frances Fullerton Museum of Art</title>

<div class="skip">
	<a href="#header">Skip Main Navigation</a>
</div>
<nav>
	<div class="container">
		<a href="../" class="logo">
			<img alt="California State University, San Bernardino" src="../static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>
		
		<div class="right-links">
			<div class="common">
				<a href="../csusbmobile">CSUSB MOBILE</a>
			</div>
			
			<ul class="dropdown">
				<li><a href="#">All Apps</a></li>
				<ul>
					<?php 
						$path = "../static/data/apps.json";
						$data =  json_decode( file_get_contents($path), true ) ;
						$apps = $data['apps'];
						for( $i=0; $i< sizeof($apps); $i++){
							if(substr($apps[$i]['url'], 0, 4) == "http"){
								echo "<li><a href='". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
							else{
								echo "<li><a href='../". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
						}
					?>
				</ul>
			</ul>
		</div>
	
	</div>
</nav>


<div id="header">
	<div id="main-background">
		<div class="image-overlay"></div>
	</div>
	<div class="container">
		<h1>RAFFMA</h1>
		<p>Robert and Frances Fullerton Museum of Art</p>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/raffma/id728778224?mt=8"><img alt="Download RAFFMA app from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=edu.csusb.raffma&hl=en"><img alt="Download RAFFMA app from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>

	</div>
</div>

 
<div class="down-arrow">
	
</div>

<div id="features">
	<div class="container">
		<div class="feature-box">
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/raffma/icon-photo.svg" width="26">
				</div>
				<div class="text">
					<h2>View Collections</h2>
					<p>RAFFMA is best recognized for its many collection ranging from Egyptian antiquities to contemporary art.</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/raffma/icon-palette.svg" width="26">
				</div>
				<div class="text">
					<h2>Creative Events</h2>
					<p>For artists and apreciators, check out RAFFMA's variety of creative events from workshops to artshows.</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/raffma/icon-info.svg" width="26">
				</div>
				<div class="text">
					<h2>Visitor Information</h2>
					<p>Check the app for gallery hours, directons, parking, visitor information, policies, and admission information.</p>
				</div>
			</div>

			</div>			
		</div>
	</div>	
</div>


<div id="app-display">
	<div class="container">
		<div class="content">
			<h2>Experience the Past!</h2>
			<p>When you visit the RAFFMA, you find it's more than a museum, it's an unforgettable experience. Guests of the museum can explore for themselves the cultural richness hidden within the tales of Ancient Egypt spreading beyond the barriers of time and into modern day.</p>
			<p>The Robert and Frances Fullerton Museum of Art has accumulated a collection of close to 1,200 objects focusing on ceramics, ancient and contemporary art; the largest of which includes some 500 pieces encompassing over 4,000 years of Egyptian history.</p>
		</div>
	</div>
</div>




<div class="rotated-screens">
</div>


<div id="app-info">
	<div class="container">
		
		<div class="specs">
			<h2>Specifications</h2>
			<ul>
				<li>
					<span class="title">Operation System:</span>
					<span>Android 4.0.3 or iOS 6+</span>
				</li>
				<li>
					<span class="title">Current Version:</span>
					<span>2.0</span>
				</li>
				<li>
					<span class="title">Updated:</span>
					<span>February 22, 2016</span>
				</li>
			</ul>
		</div>

		<div class="changelog">
			<h2>What's New</h2>
			<ul>
				<li>Update for stability and content changes</li>
			</ul>
		</div>


	</div>
</div>








<div id="download-bottom">
	<div class="container">
		<p>Stay connected with the campus community!</p>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img alt="Download CSUSB Mobile from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img alt="Download CSUSB Mobile from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>
	</div>
</div>




<footer>
	<div class="container">
		<div class="links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>			
		</div>
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>
	</div>
</footer>
</html>


