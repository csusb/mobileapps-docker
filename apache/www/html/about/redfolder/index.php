<!--=================================================
RED FOLDER APP PAGE
==================================================-->
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/redfolder.css">
<title>Red Folder: An Emergency Resource Mobile App Developed at CSUSB</title>

<div class="skip">
	<a href="#header">Skip Main Navigation</a>
</div>
<nav>
	<div class="container">
		<a href="../" class="logo">
			<img alt="California State University, San Bernardino" src="../static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>
		
		<div class="right-links">
			<div class="common">
				<a href="../csusbmobile">CSUSB MOBILE</a>
			</div>
			
			<ul class="dropdown">
				<li><a href="#">All Apps</a></li>
				<ul>
					<?php 
						$path = "../static/data/apps.json";
						$data =  json_decode( file_get_contents($path), true ) ;
						$apps = $data['apps'];
						for( $i=0; $i< sizeof($apps); $i++){
							if(substr($apps[$i]['url'], 0, 4) == "http"){
								echo "<li><a href='". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
							else{
								echo "<li><a href='../". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
						}
					?>

				</ul>
			</ul>
		</div>
	
	</div>
</nav>


<div id="header">
	<div id="main-background">
		<div class="image-overlay"></div>
	</div>
	<div class="container">
		<h1><img alt="Red Folder" src="../static/images/redfolder/logo.png" width="200"></h1>
		<p>Be the Difference.</p>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/red-folder/id913718903?mt=8"><img alt="Download Red Folder from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=edu.calstate.redfolder&hl=en"><img alt="Download Red Folder from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>

	</div>
</div>

 
<div class="down-arrow">
</div>

<div id="features">
	<div class="container">
		<div class="feature-box">
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/redfolder/icon-mic.svg" width="26">
				</div>
				<div class="text">
					<h2>Campus Resources</h2>
					<p>Red Folder utilizes the users location and displays a list of both local and campus resources.</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/redfolder/icon-happy.svg" width="26">
				</div>
				<div class="text">
					<h2>Health Assessment</h2>
					<p>Helps the user understand and assess the psychological state of a student while assessing their behavior.</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/redfolder/icon-alert.svg" width="26">
				</div>
				<div class="text">
					<h2>Indicators and Precautions</h2>
					<p>Informs the user of potentially dangerous behaviors and recommends the the appropriate reaction.</p>
				</div>
			</div>

			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/redfolder/icon-world.svg" width="26">
				</div>
				<div class="text">
					<h2>Works Everywhere</h2>
					<p>Red Folder works across all 23 California State University campuses.</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/redfolder/icon-eye.svg" width="26">
				</div>
				<div class="text">
					<h2>See the Signs</h2>
					<p>Learn and develop  skills needed to handle a variety of situations</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/redfolder/icon-check.svg" width="26">
				</div>
				<div class="text">
					<h2>Be the Difference</h2>
					<p>You may help save a student's academic career, or even their life.</p>
				</div>
			</div>			
		</div>
	</div>	
</div>


<div id="app-display">
	<div class="container">
		<div class="content">
			<h2>See. Say. Do.</h2>
			<p>Red Folder is a reference tool that contains safety tips and contact information for a wide variety of emergency campus resources. It also provides guidance on responding to incidents in which a community member might report being a danger to self or a threat to others. </p>
		</div>
	</div>
</div>




<div class="rotated-screens">
</div>


<div id="app-info">
	<div class="container">
		
		<div class="specs">
			<h2>Specifications</h2>
			<ul>
				<li>
					<span class="title">Operation System:</span>
					<span>Android 4.0.3 or iOS 6+</span>
				</li>
				<li>
					<span class="title">Current Version:</span>
					<span>2.0.1</span>
				</li>
				<li>
					<span class="title">Updated:</span>
					<span>November 23, 2015</span>
				</li>
			</ul>
		</div>

		<div class="changelog">
			<h2>What's New</h2>
			<ul>
				<li>Access to all 23 CSU Campuses</li>
				<li>Accessibility fixes</li>
				<li>Improvements for latest Android versions</li>
			</ul>
		</div>


	</div>
</div>




<div id="download-bottom">
	<div class="container">
		<p>Stay connected with the campus community!</p>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img alt="Download CSUSB Mobile from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img alt="Download CSUSB Mobile from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>
	</div>
</div>




<footer>
	<div class="container">
		<div class="links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>			
		</div>
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>
	</div>
</footer>
</html>

