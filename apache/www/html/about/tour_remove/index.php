<!--=================================================
TOUR APP PAGE
==================================================-->
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/tour.css">
<title>Tour CSUSB: A Campus Tour Mobile App Developed at CSUSB</title>

<a href="#header">Skip Main Navigation</a>
<nav>
	<div class="container">
		<a href="../" class="logo">
			<img alt="" src="../static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>
		
		<div class="right-links">
			<div class="common">
				<a href="../csusbmobile">CSUSB MOBILE</a>
			</div>
			
			<ul class="dropdown">
				<li><a href="#">MORE</a></li>
				<ul>
					<h2>All Apps</h2>
					<?php 
						$path = "../static/data/apps.json";
						$data =  json_decode( file_get_contents($path), true ) ;
						$apps = $data['apps'];
								
						for( $i=0; $i< sizeof($apps); $i++){
							echo "<li><a href='../". $apps[$i][url] ."' data-name='" . $apps[$i][name] . "'>" . $apps[$i][name] . "</a></li>";
						}

					?>
				</ul>
			</ul>
		</div>
	
	</div>
</nav>


<div id="header">
	<div id="main-background">
		<div class="image-overlay"></div>
	</div>
	<div class="container">
		<h5>TOUR CSUSB</h3>
		<h1>Discover what our Campus has to Offer</h1>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/tour-csusb/id589643165?mt=8"><img alt="Download Tour CSUSB from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=edu.csusb.tour&hl=en"><img alt="Download Tour CSUSB from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>

	</div>
</div>

 
<div class="down-arrow">
	
</div>

<div id="features">
	<div class="container">
		<div class="feature-box">
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/tour/icon-map.svg" width="26">
				</div>
				<div class="text">
					<h1>Tour the Campus</h1>
					<p>Check out CSUSB through a self guided tour with maps and pictures that make finding anything easy! </p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/tour/icon-info.svg" width="26">
				</div>
				<div class="text">
					<h1>Quick Information</h1>
					<p>Get quick information about CSUSB's many buildings and majors including contact information!</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/tour/icon-camera.svg" width="26">
				</div>
				<div class="text">
					<h1>Photos & Videos</h1>
					<p>Rich with text, video and images for more than 50 locations around our campus.</p>
				</div>
			</div>

			</div>			
		</div>
	</div>	
</div>


<div id="app-display">
	<div class="container">
		<div class="content">
			<h1>Take a look around!</h1>
			<p>Tour the CSUSB campus with your mobile device to learn about your future home as a Cal State San Bernardino Coyote.</p>

			<p>This self-guided tour app is rich with text, video and images for more than 50 locations around our campus. It's street-view enabled, so take some time to explore the highlights of CSUSB’s beautiful campus.</p>

			<p>Each stop features a general description and contact information for departments or offices housed within. Come Here, Go Anywhere!</p>
		</div>
	</div>
</div>




<div class="rotated-screens">
</div>


<div id="app-info">
	<div class="container">
		
		<div class="specs">
			<h5>Specifications</h5>
			<ul>
				<li>
					<span class="title">Operation System:</span>
					<span>Android 2.3.3 or iOS 6+</span>
				</li>
				<li>
					<span class="title">Current Version:</span>
					<span>2.0.2</span>
				</li>
				<li>
					<span class="title">Updated:</span>
					<span>May 11, 2015</span>
				</li>
			</ul>
		</div>

		<div class="changelog">
			<h5>What's New</h5>
			<ul>
				<li>Updated UI for all devices</li>
				<li>Added building photos and videos</li>
				<li>Added StreetView</li>
				<li>Improved responsiveness and speed</li>
			</ul>
		</div>


	</div>
</div>











<div id="download-bottom">
	<div class="container">
		<h1>Stay connected with the campus community!</h1>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img alt="Download CSUSB Mobile from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img alt="Download CSUSB Mobile from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>
	</div>
</div>




<footer>
	<div class="container">
		<div class="links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>			
		</div>
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>
	</div>
</footer>
</html>






