<!--=================================================
RECSPORTS APP PAGE
==================================================-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/recsports.css">


<nav>
	<div class="container">
		<a href="../" class="logo">
			<img src="../static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>
		
		<div class="right-links">
			<div class="common">
				<a href="../csusbmobile">CSUSB MOBILE</a>
			</div>
			
			<ul class="dropdown">
				<li><a href="#">MORE</a></li>
				<ul>
					<h2>All Apps</h2>
					<?php 
						$path = "../static/data/apps.json";
						$data =  json_decode( file_get_contents($path), true ) ;
						$apps = $data['apps'];
						for( $i=0; $i< sizeof($apps); $i++){
							if(substr($apps[$i][url], 0, 4) == "http"){
								echo "<li><a href='". $apps[$i][url] ."' data-name='" . $apps[$i][name] . "'>" . $apps[$i][name] . "</a></li>";
							}
							else{
								echo "<li><a href='../". $apps[$i][url] ."' data-name='" . $apps[$i][name] . "'>" . $apps[$i][name] . "</a></li>";
							}
						}
					?>
				</ul>
			</ul>
		</div>
	
	</div>
</nav>


<div id="header">
	<div id="main-background">
		<div class="image-overlay"></div>
	</div>
	<div class="container">
		<h5>RECREATIONAL SPORTS</h3>
		<h1>Healthy Minds and Bodies</h1>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-rec-sports/id589646055?mt=8"><img src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=edu.csusb.recsports&hl=en"><img src="../static/images/google-play-icon.svg"></a>
		</div>

	</div>
</div>

 
<div class="down-arrow">
	
</div>

<div id="features">
	<div class="container">
		<div class="feature-box">
			<div class="item">
				<div class="icon">
					<img src="../static/images/recsports/icon-schedule.svg" width="26">
				</div>
				<div class="text">
					<h1>Scheduling Information</h1>
					<p>Easily access to Recretionl Sport events, outdoor trips, and fitness center hours!</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img src="../static/images/recsports/icon-timer.svg" width="26">
				</div>
				<div class="text">
					<h1>Personal Training</h1>
					<p>Get information about our Certified CSUSB Personal Trainers including pricing and options.</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img src="../static/images/recsports/icon-info.svg" width="26">
				</div>
				<div class="text">
					<h1>Memborship Information</h1>
					<p>Get information regarding membership options, prices, and equipment rentals.</p>
				</div>
			</div>

			</div>			
		</div>
	</div>	
</div>


<div id="app-display">
	<div class="container">
		<div class="content">
			<h1>Enhance your physical fitness!</h1>
			<p>Wondering what time your favorite group exercise class is next quarter? Want to see all upcoming outdoor trips? Don't miss out on all that Rec Sports has to offer with the CSUSB Recreational Sports App.</p>
			<p>Keep up-to-date with the Student Recreation and Fitness Center hours, open rec hours, pool hours, all calendar events, and more.</p>
			<p>The CSUSB Recreational Sports App has all the information you need to help you get into shape and stay fit!</p>
		</div>
	</div>
</div>




<div class="rotated-screens">
</div>


<div id="app-info">
	<div class="container">
		
		<div class="specs">
			<h5>Specifications</h5>
			<ul>
				<li>
					<span class="title">Operation System:</span>
					<span>Android 2.3.3 or iOS 6+</span>
				</li>
				<li>
					<span class="title">Current Version:</span>
					<span>1.2.1</span>
				</li>
				<li>
					<span class="title">Updated:</span>
					<span>May 11, 2015</span>
				</li>
			</ul>
		</div>

		<div class="changelog">
			<h5>What's New</h5>
			<ul>
				<li>Minor bug fixes</li>
			</ul>
		</div>
	</div>
</div>



<div id="download-bottom">
	<div class="container">
		<h1>Stay connected with the campus community!</h1>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img src="../static/images/google-play-icon.svg"></a>
		</div>
	</div>
</div>




<footer>
	<div class="container">
		<div class="links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>			
		</div>
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>
	</div>
</footer>



