<!--=================================================
TEAM PAGE
==================================================-->
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/team.css">
<title>Meet CSUSB's Mobile App Development Team</title>

<div class="skip">
	<a href="#header-team">Skip Main Navigation</a>
</div>
<nav>
	<div class="container">
		<a href="../" class="logo">
			<img alt="California State University, San Bernardino" src="../static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>
		
		<div class="right-links">
			<div class="common">
				<a href="../csusbmobile">CSUSB MOBILE</a>
			</div>
			
			<ul class="dropdown">
				<li><a href="#">MORE</a></li>
				<ul>
					<h2>All Apps</h2>
					<?php 
						$path = "../static/data/apps.json";
						$data =  json_decode( file_get_contents($path), true ) ;
						$apps = $data['apps'];
						for( $i=0; $i< sizeof($apps); $i++){
							if(substr($apps[$i]['url'], 0, 4) == "http"){
								echo "<li><a href='". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
							else{
								echo "<li><a href='../". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
						}
					?>

				</ul>
			</ul>
		</div>
	</div>
</nav>

<div id="header-team">
</div>

 

<div id="team">
	<div class="container">
		<h1>Mobile App Development Team</h1>

<!--		Sunny-->
		<div class="member">
			<div class="profile-picture">
				<img alt="" src="../static/images/team/Sunny.png">
			</div>		
			<div class="bio">
				<h2 class="name">Sunny Lin</h2>
				<p class="position">Lead Operating System Analyst</p>
			</div>
			<a class="social-button" title="LinkedIn" href="https://www.linkedin.com/in/sunny-lin-40b98411">
				<p class="link">View LinkedIn</p>
			</a>
		</div>

<!--		Dr. C-->
		<div class="member">
			<div class="profile-picture">
				<img alt="" src="../static/images/team/Art.png">
			</div>
			<div class="bio">
				<h2 class="name">Arturo Concepcion</h2>
				<p class="position">Professor of Computer Science</p>
			</div>
			<a class="social-button" title="Website" href="http://cse.csusb.edu/concep/">
				<p class="link">View Website</p>
			</a>
		</div>

<!--		Michael Baaske-->
		<div class="member">
			<div class="profile-picture">
				<img alt="" src="../static/images/team/Michael.png">
			</div>
			<div class="bio">
				<h2 class="name">Michael Baaske</h2>
				<p class="position">Developer</p>
			</div>
			<a class="social-button" title="LinkedIn" href="https://www.linkedin.com/in/michael-baaske-1548baa">
				<p class="link">View LinkedIn</p>
			</a>
		</div>

<!--		Ruben Castaneda-->
		<div class="member">
			<div class="profile-picture">
				<img alt="" src="../static/images/team/Ruben.png">
			</div>
			<div class="bio">
				<h2 class="name">Ruben Castaneda</h2>
				<p class="position">Developer</p>
			</div>
<!--			<a class="social-button" title="LinkedIn" href="#"><img alt="" src="../static/images/team/backgrounds/Blue_Button.png" />-->
<!--				<p class="link">View LinkedIn</p>-->
<!--			</a>-->
		</div>

<!--		Kyle Holley-->
		<div class="member">
			<div class="profile-picture">
				<img alt="" src="../static/images/team/Kyle.png">
			</div>
			<div class="bio">
				<h2 class="name">Kyle Holley</h2>
				<p class="position">Developer</p>
			</div>
			<a class="social-button" title="LinkedIn" href="https://www.linkedin.com/in/kholley3">
				<p class="link">View LinkedIn</p>
			</a>
		</div> <br />

<!--		Sean Hamilton-->
		<div class="member">
			<div class="profile-picture">
				<img alt="" src="../static/images/team/Sean.png">
			</div>
			<div class="bio">
				<h2 class="name">Sean Hamilton</h2>
				<p class="position">Developer</p>
			</div>
<!--			<a class="social-button" title="LinkedIn" href="#"><img alt="" src="../static/images/team/backgrounds/Blue_Button.png" />-->
<!--				<p class="link">View LinkedIn</p>-->
<!--			</a>-->
		</div>

<!--		Jay Huan Ryu-->
		<div class="member">
			<div class="profile-picture">
				<img alt="" src="../static/images/team/Jay.png">
			</div>
			<div class="bio">
				<h2 class="name">Jay Huan Ryu</h2>
				<p class="position">Developer</p>
			</div>
			<a class="social-button" title="LinkedIn" href="http://linkedin.com/in/jay-huan-ryu-15882991">
				<p class="link">View LinkedIn</p>
			</a>
		</div>

<!--		Christopher Koenig-->
		<div class="member">
			<div class="profile-picture">
				<img alt="" src="../static/images/team/Chris.png">
			</div>
			<div class="bio">
				<h2 class="name">Christopher Koenig</h2>
				<p class="position">Developer</p>
			</div>
<!--			<a class="social-button" title="LinkedIn" href="#"><img alt="" src="../static/images/team/backgrounds/Blue_Button.png" />-->
<!--				<p class="link">View LinkedIn</p>-->
<!--			</a>-->
		</div>

<!--		Richard Cho-->
		<div class="member">
			<div class="profile-picture">
				<img alt="" src="../static/images/team/Richard.png">
			</div>
			<div class="bio">
				<h2 class="name">Richard Cho</h2>
				<p class="position">UI/UX Designer</p>
			</div>
<!--			<a class="social-button" title="LinkedIn" href="#"><img alt="" src="../static/images/team/backgrounds/Blue_Button.png" />-->
<!--				<p class="link">View LinkedIn</p>-->
<!--			</a>-->
		</div>

<!--		Aracely Munoz-->
		<div class="member">
			<div class="profile-picture">
				<img alt="" src="../static/images/team/Aracely.png">
			</div>
			<div class="bio">
				<h2 class="name">Aracely Mu&ntildeoz</h2>
				<p class="position">UI/UX Designer</p>
			</div>
<!--			<a class="social-button" title="LinkedIn" href="#"><img alt="" src="../static/images/team/backgrounds/Blue_Button.png" />-->
<!--				<p class="link">View LinkedIn</p>-->
<!--			</a>-->
		</div>


	</div>
</div>



<footer>
	<div class="container">
		<div class="links">
			<ul class="index">
				<li><a href="../">APPS</a></li>
				<li><a href="../team">TEAM</a></li>
				<li><a href="mailto:mobileapps@csusb.edu">CONTACT</a></li>
			</ul>
		</div>
		
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>
		
	</div>
</footer>
</html>


