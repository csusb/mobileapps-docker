<!--=================================================
BUBBLESHOT APP PAGE
==================================================-->
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/slidewinder.css">
<title>SlideWinder: A Mobile App Game Developed at CSUSB</title>

<div class="skip">
	<a href="#header">Skip Main Navigation</a>
</div>
<nav>
	<div class="container">
		<a href="../" class="logo">
			<img alt="California State University, San Bernardino" src="../static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>
		
		<div class="right-links">
			<div class="common">
				<a href="../csusbmobile">CSUSB MOBILE</a>
			</div>
			
			<ul class="dropdown">
				<li><a href="#">All Apps</a></li>
				<ul>
					<?php 
						$path = "../static/data/apps.json";
						$data =  json_decode( file_get_contents($path), true ) ;
						$apps = $data['apps'];
						for( $i=0; $i< sizeof($apps); $i++){
							if(substr($apps[$i]['url'], 0, 4) == "http"){
								echo "<li><a href='". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
							else{
								echo "<li><a href='../". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
						}
					?>
				</ul>
			</ul>
		</div>
	
	</div>
</nav>


<div id="header">
	<div id="main-background">
		<div class="image-overlay"></div>
	</div>
	<div class="container">
		<h1>SLIDEWINDER</h1>
		<p>An Addicting Action Game</p>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/slidewinder/id820526175?mt=8"><img alt="Download Slidewinder from the App store" src="../static/images/app-store-icon.svg"></a>
		</div>

	</div>
</div>

 
<div class="down-arrow">
	
</div>

<div id="features">
	<div class="container">
		<div class="feature-box">
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/slidewinder/icon-happy.svg" width="26">
				</div>
				<div class="text">
					<h2>Challenging, Yet Fun</h2>
					<p>Game play increases in difficulty the better you do!</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/slidewinder/icon-star.svg" width="26">
				</div>
				<div class="text">
					<h2>Compare High Scores</h2>
					<p>Compete with your all your friends for the highest score! </p>
				</div>
			</div>

			</div>			
		</div>
	</div>	
</div>


<div id="app-display">
	<div class="container">
		<div class="content">
			<h2>Addicting & Fun!</h2>
			<p>Slidewinder is a simple and addicting action game that is extremely easy to play. Players collect sliding colored balls to score as many points as possible. Spin the center wheel to align colored bars with their matching colored balls.</p>
			<p>- Game play increases in difficulty as duration progresses for an ever increasing challenge!<br>
			- High scores to compete with friends!<br>
			- Special balls that require you to adapt quickly to score as high as possible!</p>

		</div>
	</div>
</div>




<div class="rotated-screens">
</div>


<div id="app-info">
	<div class="container">
		
		<div class="specs">
			<h2>Specifications</h2>
			<ul>
				<li>
					<span class="title">Operation System:</span>
					<span>iOS 6+</span>
				</li>
				<li>
					<span class="title">Current Version:</span>
					<span>2.0</span>
				</li>
				<li>
					<span class="title">Updated:</span>
					<span>July 28, 2015</span>
				</li>
			</ul>
		</div>

		<div class="changelog">
			<h2>What's New</h2>
			<ul>
				<li>User interface and user experience redone. A whole new modern feel and look.</li>
			</ul>
		</div>
	</div>
</div>



<div id="download-bottom">
	<div class="container">
		<p>Stay connected with the campus community!</p>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img alt="Download CSUSB Mobile from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img alt="Download CSUSB Mobile from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>
	</div>
</div>




<footer>
	<div class="container">
		<div class="links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>			
		</div>
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>
	</div>
</footer>
</html>

