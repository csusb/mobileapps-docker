<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/soar.css">
<title>SOAR: A Mobile App for Student Orientation Advising and Registration Developed at CSUSB</title>

<div class="skip">
	<a href="#header">Skip Main Navigation</a>
</div>
<nav>
	<div class="container">
		<a href="../" class="logo">
			<img alt="California State University, San Bernardino" src="../static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>

		<div class="right-links">
			<div class="common">
				<a href="../csusbmobile">CSUSB MOBILE</a>
			</div>

			<ul class="dropdown">
				<li><a href="#">All Apps</a></li>
				<ul>
					<?php
						$path = "../static/data/apps.json";
						$data =  json_decode( file_get_contents($path), true ) ;
						$apps = $data['apps'];
						for( $i=0; $i< sizeof($apps); $i++){
							if(substr($apps[$i]['url'], 0, 4) == "http"){
								echo "<li><a href='". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
							else{
								echo "<li><a href='../". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
						}
					?>

				</ul>
			</ul>
		</div>

	</div>
</nav>


<div id="header">
	<div id="main-background">
		<div class="image-overlay"></div>
	</div>
	<div class="container">
		<h1>SOAR</h1>
		<p>Information at your finger tips!</p>
		<div class="store-buttons">
			<a href="https://play.google.com/store/apps/details?id=edu.csusb.soar&hl=en"><img alt="Download SOAR from Google Play" src="../static/images/google-play-icon.svg"></a>
			<a href="https://itunes.apple.com/us/app/soar-csusb-2016/id1111696789?mt=8"><img alt="Download SOAR from the App Store" src="../static/images/app-store-icon.svg"></a>
		</div>

	</div>
</div>


<div class="down-arrow">

</div>

<div id="features">
	<div class="container">
		<div class="feature-box">
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/soar/icon-profile.svg">
				</div>
				<div class="text">
					<h2>Manage Your Profile</h2>
					<p>Access to update and edit your profile!</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/soar/icon-view.svg">
				</div>
				<div class="text">
					<h2>View SOAR Information</h2>
					<p>View information about orientation, advising, and registration.</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/soar/icon-documents.svg">
				</div>
				<div class="text">
					<h2>View Documents</h2>
					<p>Easy and quick access to general documents at any time!</p>
				</div>
			</div>

			</div>
		</div>
	</div>
</div>


<div id="app-display">
	<div class="container">
		<div class="content">
			<h1>At Your Finger Tips!</h1>
			<p>Student Orientation Advising and Registration (SOAR) is the official students aid presented by California State University San Bernardino (CSUSB). This application is designed to make it easier for students to access information regarding advising, registration, etc. You're able to find others in yuor pack, view partners, and so much more!</p>
		</div>
	</div>
</div>




<div class="rotated-screens">
</div>


<div id="app-info">
	<div class="container">

		<div class="specs">
			<h5>Specifications</h5>
			<ul>
				<li>
					<span class="title">Operating System:</span>
					<span>Android and iOS</span>
				</li>
				<li>
					<span class="title">Current Version:</span>
					<span>1.1</span>
				</li>
				<li>
					<span class="title">Updated:</span>
					<span>June 22, 2016</span>
				</li>
			</ul>
		</div>

		<div class="changelog">
			<h5>What's New</h5>
			<ul>
				<li>-Fixed Several Bugs</li>
				<li>-Minor UI Cleanup</li>
			</ul>
		</div>
	</div>
</div>



<div id="download-bottom">
	<div class="container">
		<p>Stay connected with the campus community!</p>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img alt="Download CSUSB Mobile from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img alt="Download CSUSB Mobile from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>
	</div>
</div>




<footer>
	<div class="container">
		<div class="links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>
	</div>
</footer>
</html>
