<!--=================================================
BUBBLESHOT APP PAGE
==================================================-->
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/bubbleshot.css">
<title>Bubbleshot: A Mobile App Game Developed at CSUSB</title>

<div class="skip">
	<a href="#header">Skip Main Navigation</a>
</div>
<nav>
	<div class="container">
		<a href="../" class="logo">
			<img alt="California State University, San Bernardino" src="../static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>
		
		<div class="right-links">
			<div class="common">
				<a href="../csusbmobile">CSUSB MOBILE</a>
			</div>
			
			<ul class="dropdown">
				<li><a href="#" title="Dropdown Menu" aria-label="Dropdown Menu">All Apps</a></li>
				<ul>
					<?php 
						$path = "../static/data/apps.json";
						$data =  json_decode( file_get_contents($path), true ) ;
						$apps = $data['apps'];
						for( $i=0; $i< sizeof($apps); $i++){
							if(substr($apps[$i]['url'], 0, 4) == "http"){
								echo "<li><a href='". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
							else{
								echo "<li><a href='../". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
						}
					?>
				</ul>
			</ul>
		</div>
	
	</div>
</nav>


<div id="header">
	<div id="main-background">
		<div class="image-overlay"></div>
	</div>
	<div class="container">
		<h1>BUBBLESHOT ALPHA</h1>
		<p>A Fun Puzzle Shooter Game</p>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/bubbleshot-alpha/id1006000053?mt=8"><img alt="Download Bubbleshot Alpha from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=com.CSUSB.BubbleShot_Alpha&hl=en"><img alt="Download Bubbleshot Alpha from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>

	</div>
</div>

 
<div class="down-arrow">
	
</div>

<div id="features">
	<div class="container">
		<div class="feature-box">
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/bubbleshot/icon-maze.svg" width="26">
				</div>
				<div class="text">
					<h2>Unique Levels & Worlds</h2>
					<p>Explore the many levels and worlds of BubbleShot with over 30 levels and challenges!</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/bubbleshot/icon-star.svg" width="26">
				</div>
				<div class="text">
					<h2>Compare High Scores</h2>
					<p>Compete with your all your friends for the highest score! Try to collect all 35 coins!</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/bubbleshot/icon-palette.svg" width="26">
				</div>
				<div class="text">
					<h2>Fun Graphics & Visuals</h2>
					<p>Navigate the emersive worlds of BubbleShot, with Stunning visuals to match! </p>
				</div>
			</div>

			</div>			
		</div>
	</div>	
</div>


<div id="app-display">
	<div class="container">
		<div class="content">
			<h2>Challenge your mind!</h2>
			<p>Bubble Shot Alpha is a puzzle game meant to challenge the mind while being fun and easily accessible. It is a simple puzzle shooter where you must shoot a bubble to reach the gem. You have blocks you can place and rotate to help you reach the gem.</p>
			<p>- 5 unique worlds with different obstacles to keep the game fresh!<br>
			- 7 levels per world, for a total of 35 levels with different challenges!<br>
			- 35 coins to collect, which help you unlock new skins for your bubble!<br>
			- High scores to compete with your friends!</p>
</p>
		</div>
	</div>
</div>




<div class="rotated-screens">
</div>


<div id="app-info">
	<div class="container">
		
		<div class="specs">
			<h2>Specifications</h2>
			<ul>
				<li>
					<span class="title">Operation System:</span>
					<span>Android 2.3 or iOS 6+</span>
				</li>
				<li>
					<span class="title">Current Version:</span>
					<span>1.0</span>
				</li>
				<li>
					<span class="title">Updated:</span>
					<span>March 6, 2015</span>
				</li>
			</ul>
		</div>

		<div class="changelog">
			<h2>What's New</h2>
			<ul>
				<li>Information not available.</li>
			</ul>
		</div>
	</div>
</div>



<div id="download-bottom">
	<div class="container">
		<p>Stay connected with the campus community!</p>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img alt="Download CSUSB Mobile from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img alt="Download CSUSB Mobile from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>
	</div>
</div>




<footer>
	<div class="container">
		<div class="links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>			
		</div>
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>
	</div>
</footer>
</html>







