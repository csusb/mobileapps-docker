<!--=================================================
CSUSB MOBILE APP PAGE
==================================================-->
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/csusbmobile.css">
<title>CSUSB Mobile: A Mobile App Developed for CSUSB</title>

<div class="skip">
	<a href="#header">Skip Main Navigation</a>
</div>
<nav>
	<div class="container">
		<a href="../" class="logo">
			<img alt="California State University, San Bernardino" src="../static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>
		
		<div class="right-links">
			<div class="common">
				<a href="../csusbmobile">CSUSB MOBILE</a>
			</div>
			
			<ul class="dropdown">
				<li><a href="#">All Apps</a></li>
				<ul>
					<?php 
						$path = "../static/data/apps.json";
						$data =  json_decode( file_get_contents($path), true ) ;
						$apps = $data['apps'];
						for( $i=0; $i< sizeof($apps); $i++){
							if(substr($apps[$i]['url'], 0, 4) == "http"){
								echo "<li><a href='". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
							else{
								echo "<li><a href='../". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
						}
					?>
				</ul>
			</ul>
		</div>
	
	</div>
</nav>


<div id="header">
	<div id="main-background">
		<div class="image-overlay"></div>
	</div>
	<div class="container">
		<h1>CSUSB MOBILE</h1>
		<p>Your Guide to Campus</p>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img alt="Download CSUSB Mobile from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img alt="Download CSUSB Mobile from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>
	</div>
</div>

 
<div class="down-arrow">
	
</div>

<div id="features">
	<div class="container">
		<div class="feature-box">
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/csusbmobile/icon-mycoyote.svg" width="26">
				</div>
				<div class="text">
					<h2>MyCoyote</h2>
					<p>Get quick access to your schedule, finances, grades, and holds all in one place.</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/csusbmobile/icon-list.svg" width="26">
				</div>
				<div class="text">
					<h2>Course List</h2>
					<p>Know which classes are open with a complete up-to-date course catalog!</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/csusbmobile/icon-info.svg" width="26">
				</div>
				<div class="text">
					<h2>Important Info</h2>
					<p>Check grade postings, fee reminders, aide disbursements, and holds.</p>
				</div>
			</div>

			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/csusbmobile/icon-uiux.svg" width="26">
				</div>
				<div class="text">
					<h2>UI/UX Overhaul</h2>
					<p>Experience that adheres to Google's Material Design and iOS Human Interface.</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/csusbmobile/icon-speed.svg" width="26">
				</div>
				<div class="text">
					<h2>Speed Improvements</h2>
					<p>Switch from webview frameworks to native (Java, Swift, Objective-C).</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/csusbmobile/icon-place.svg" width="26">
				</div>
				<div class="text">
					<h2>Location Aware</h2>
					<p>Serves campus specific content based on GPS location.</p>
				</div>
			</div>			
		</div>
	</div>	
</div>


<div id="app-display">
	<div class="container">
		<div class="content">
			<h2>By Students, For Students!</h2>
			<p>Looking for a classroom or building? Want to access MyCoyote on the go? CSUSB Mobile Version 3 has up to date news and course information.</p>
			<p>CSUSB Mobile was built by students, for students to provide a convenient way access schedules, grades, news and more. CSUSB Mobile is entirely free and accessible by anyone, and uses the latest technology to provide a rich experience across mobile devices.</p>
		</div>
	</div>
</div>




<div class="rotated-screens">
</div>


<div id="app-info">
	<div class="container">
		
		<div class="specs">
			<h2>Specifications</h2>
			<ul>
				<li>
					<span class="title">Operation System:</span>
					<span>Android 4+ or iOS 6+</span>
				</li>
				<li>
					<span class="title">Current Version:</span>
					<span>3.0.2</span>
				</li>
				<li>
					<span class="title">Updated:</span>
					<span>May 11, 2015</span>
				</li>
			</ul>
		</div>

		<div class="changelog">
			<h2>What's New</h2>
			<ul>
				<li>- Fix for crash that occasionally happened on startup</li>
				<li>- Added option to open browser links in-app (will be done by default, see settings menu)</li>
				<li>- Removed prompts to install other apps. If the app is installed, it will be launched, otherwise it will follow the above new browser behavior</li>
				<li>- Rephrased a couple of things to avoid confusion</li>
			</ul>
		</div>
	</div>
</div>




<div id="download-bottom">
	<div class="container">
		<p>Stay connected with the campus community!</p>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img alt="Download CSUSB Mobile from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img alt="Download CSUSB Mobile from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>
	</div>
</div>




<footer>
	<div class="container">
		<div class="links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>			
		</div>
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>
	</div>
</footer>
</html>


