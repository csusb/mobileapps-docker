<!--=================================================
CoyoteRadio APP PAGE
==================================================-->
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/coyoteradio.css">
<title>Coyote Radio: A Mobile App for CSUSB's Student Radio Station</title>

<div class="skip">
	<a href="#header">Skip Main Navigation</a>
</div>
<nav>
	<div class="container">
		<a href="../" class="logo">
			<img alt="California State University, San Bernardino" src="../static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>

		<div class="right-links">
			<div class="common">
				<a href="../csusbmobile">CSUSB MOBILE</a>
			</div>

			<ul class="dropdown">
				<li><a href="#">All Apps</a></li>
				<ul>
					<?php
						$path = "../static/data/apps.json";
						$data =  json_decode( file_get_contents($path), true ) ;
						$apps = $data['apps'];
						for( $i=0; $i< sizeof($apps); $i++){
							if(substr($apps[$i]['url'], 0, 4) == "http"){
								echo "<li><a href='". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
							else{
								echo "<li><a href='../". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
						}
					?>

				</ul>
			</ul>
		</div>

	</div>
</nav>


<div id="header">
	<div id="main-background">
		<div class="image-overlay"></div>
	</div>
	<div class="container">
		<h1>COYOTE RADIO</h1>
		<p>Streaming Live To Your Phone!</p>
		<div class="store-buttons">
			<a href="https://play.google.com/store/apps/details?id=edu.csusb.CoyoteRadio&hl=en"><img alt="Download Coyote Radio from Google Play" src="../static/images/google-play-icon.svg"></a>
      <a href="https://itunes.apple.com/us/app/coyote-radio/id681286997?mt=8"><img alt="Download Coyote Radio from the App Store" src="../static/images/app-store-icon.svg"></a>
		</div>

	</div>
</div>


<div class="down-arrow">

</div>

<div id="features">
	<div class="container">
		<div class="feature-box">
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/coyoteradio/icon-clock.svg">
				</div>
				<div class="text">
					<h2>Streaming 24/7</h2>
					<p>Listen anywhere, at anytime!</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/coyoteradio/icon-headphones.svg">
				</div>
				<div class="text">
					<h2>Your Favorite Music</h2>
					<p>Including some of your favorite local bands and DJs!</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/coyoteradio/icon-microphone.svg">
				</div>
				<div class="text">
					<h2>Stay Updated</h2>
					<p>Our talk shows always keep you updated!</p>
				</div>
			</div>

			</div>
		</div>
	</div>
</div>


<div id="app-display">
	<div class="container">
		<div class="content">
			<h2>Streaming Live!</h2>
			<p>The number one college radio station in San Bernardino now has a mobile app! streaming live, we offer you 24 hour 7 days a week streaming to your smartphone of your favorite local bands from your favorite DJs! Listen in with our talk shows to keep you up to date with the latest in the San Bernardino area.</p>
		</div>
	</div>
</div>




<div class="rotated-screens">
</div>


<div id="app-info">
	<div class="container">

		<div class="specs">
			<h2>Specifications</h2>
			<ul>
				<li>
					<span class="title">Operating System:</span>
					<span>Android or iOS 8+</span>
				</li>
				<li>
					<span class="title">Current Version:</span>
					<span>3.0</span>
				</li>
				<li>
					<span class="title">Updated:</span>
					<span>Aug 12, 2016</span>
				</li>
			</ul>
		</div>

		<div class="changelog">
			<h2>What's New</h2>
			<ul>
				<li>-App Written in Native Code (Java and Swift 2.2)</li>
        <li>-Fixed Streaming Issues and Other Bugs</li>
			</ul>
		</div>
	</div>
</div>



<div id="download-bottom">
	<div class="container">
		<p>Stay connected with the campus community!</p>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img alt="Download CSUSB Mobile from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img alt="Download CSUSB Mobile from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>
	</div>
</div>




<footer>
	<div class="container">
		<div class="links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>
	</div>
</footer>
</html>
