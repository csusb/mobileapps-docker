<!--=================================================
MAIN MOBILE PAGE > INDEX
==================================================-->
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<link rel="stylesheet" href="static/css/skeleton.css">
<link rel="stylesheet" href="static/css/index.css">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<title>CSUSB Mobile App Development Team : Homepage</title>

<?php
	$path = "static/data/apps.json";
	$data =  json_decode( file_get_contents($path), true ) ;
	$apps = $data['apps'];
?>

<div class="skip">
	<a href="#header">Skip Main Navigation</a>
</div>
<nav>
	<div class="container">
		<a href="#" class="logo">
			<img alt="California State University, San Bernardino" src="static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="">Apps</a>
			<a href="team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>

		<div class="right-links">
			<div class="common">
				<a href="csusbmobile">CSUSB MOBILE</a>
			</div>

			<ul class="dropdown">
				<li><a>All Apps</a></li>
				<ul>
					<?php
						for( $i=0; $i< sizeof($apps); $i++){
							echo "<li><a href='". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
						}
					?>
				</ul>
			</ul>
		</div>

	</div>
</nav>

<div id="header">
	<div class="container">
		<div class="left-header-text">
			<h1 id="title">Mobile App Development Team</h1>
			<p id="veti">This project is supported by the Vital and Expanded Technology Initiative (VETI) Grant, a Student Success Initiative (SSI) fund</p>
			<p id="description">Specializing in drafting and developing mobile applications and websites for California State University students, staff and contracted clients.</p>
		</div>
	</div>


</div>



<div id="app-list">
	<div class="container">
		<?php
			for( $i=0; $i< sizeof($apps); $i++){
				echo "<a href='". $apps[$i]['url'] ."' title='". $apps[$i]['desc']  ."' class='app' data-name='" . $apps[$i]['name'] . "' data-platform='". $apps[$i]['platform'] ."' data-category='something'><div class='app-image'><img alt='' src='static/" . $apps[$i]['icon'] . "' width='120'></div><div class='app-text'><strong class='name'>" . $apps[$i]['name'] . "</strong><span class='description'>" . $apps[$i]['desc'] . "</span></div></a>";
			}
		?>
	</div>
</div>


<footer>
	<div class="container">
		<div class="links">
			<ul class="index">
				<li><a href="">Apps</a></li>
				<li><a href="team">Team</a></li>
				<li><a href="mailto:mobileapps@csusb.edu">Contact</a></li>
			</ul>
		</div>
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>

	</div>
</footer>
</html>
