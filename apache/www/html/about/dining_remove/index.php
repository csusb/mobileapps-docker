<!--=================================================
dining APP PAGE
==================================================-->
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/dining.css">
<title>Dining: A Mobile App for CSUSB Campus Dining</title>

<a href="#header">Skip Main Navigation</a>
<nav>
	<div class="container">
		<a href="../" class="logo">
			<img alt="California State University, San Bernardino" src="../static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>
		
		<div class="right-links">
			<div class="common">
				<a href="../csusbmobile">CSUSB MOBILE</a>
			</div>
			
			<ul class="dropdown">
				<li><a href="#">MORE</a></li>
				<ul>
					<h2>All Apps</h2>
					<?php 
						$path = "../static/data/apps.json";
						$data =  json_decode( file_get_contents($path), true ) ;
						$apps = $data['apps'];
						for( $i=0; $i< sizeof($apps); $i++){
							if(substr($apps[$i][url], 0, 4) == "http"){
								echo "<li><a href='". $apps[$i][url] ."' data-name='" . $apps[$i][name] . "'>" . $apps[$i][name] . "</a></li>";
							}
							else{
								echo "<li><a href='../". $apps[$i][url] ."' data-name='" . $apps[$i][name] . "'>" . $apps[$i][name] . "</a></li>";
							}
						}
					?>

				</ul>
			</ul>
		</div>
	
	</div>
</nav>


<div id="header">
	<div id="main-background">
		<div class="image-overlay"></div>
	</div>
	<div class="container">
		<h5>DINING</h3>
		<h1>All of Your Campus Food Options!</h1>
		<div class="store-buttons">
			<a href="https://play.google.com/store/apps/details?id=com.csusb.dining.csusbdining&hl=en"><img alt="Download CSUSB Dining from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>

	</div>
</div>

 
<div class="down-arrow">
	
</div>

<div id="features">
	<div class="container">
		<div class="feature-box">
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/dining/icon-building.svg">
				</div>
				<div class="text">
					<h1>All In One Place</h1>
					<p>Explore all the campus food selections in one place!</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/dining/icon-hours.svg">
				</div>
				<div class="text">
					<h1>Hours of Operation</h1>
					<p>See when all your favorite food places are open for business!</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/dining/icon-menu.svg">
				</div>
				<div class="text">
					<h1>Full Menus</h1>
					<p>View full featured menu items and prices.</p>
				</div>
			</div>

			</div>			
		</div>
	</div>	
</div>


<div id="app-display">
	<div class="container">
		<div class="content">
			<h1>Satisfy your hunger!</h1>
			<p>CSUSB Dining is an intuitive, user friendly application that conveniently brings California State University, San Bernardino Dining Services to the users. With hours and full menus at your fingertips you can find the perfect menu between classes</p>
		</div>
	</div>
</div>




<div class="rotated-screens">
</div>


<div id="app-info">
	<div class="container">
		
		<div class="specs">
			<h5>Specifications</h5>
			<ul>
				<li>
					<span class="title">Operation System:</span>
					<span>Android 2.3</span>
				</li>
				<li>
					<span class="title">Current Version:</span>
					<span>1.0</span>
				</li>
				<li>
					<span class="title">Updated:</span>
					<span>June 5, 2015</span>
				</li>
			</ul>
		</div>

		<div class="changelog">
			<h5>What's New</h5>
			<ul>
				<li>Updated UI</li>
			</ul>
		</div>
	</div>
</div>



<div id="download-bottom">
	<div class="container">
		<h1>Stay connected with the campus community!</h1>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img alt="Download CSUSB Mobile from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img alt="Download CSUSB Mobile from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>
	</div>
</div>




<footer>
	<div class="container">
		<div class="links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>			
		</div>
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>
	</div>
</footer>
</html>







