<!--=================================================
RoomQuest APP PAGE
==================================================-->
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/roomquest.css">
<title>RoomQuest: A Campus Navigation Mobile App Developed at CSUSB</title>

<div class="skip">
    <a href="#header">Skip Main Navigation</a>
</div>
<nav>
    <div class="container">
        <a href="../" class="logo">
            <img alt="California State University, San Bernardino" src="../static/images/csusb-logo.svg">
        </a>

        <div class="left-links">
            <a href="../">Apps</a>
            <a href="../team">Team</a>
            <a href="mailto:mobileapps@csusb.edu">Contact</a>
        </div>

        <div class="right-links">
            <div class="common">
                <a href="../csusbmobile">CSUSB MOBILE</a>
            </div>

            <ul class="dropdown">
                <li><a href="#">All Apps</a></li>
                <ul>
                    <?php
                    $path = "../static/data/apps.json";
                    $data =  json_decode( file_get_contents($path), true ) ;
                    $apps = $data['apps'];
                    for( $i=0; $i< sizeof($apps); $i++){
                        if(substr($apps[$i]['url'], 0, 4) == "http"){
                            echo "<li><a href='". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
                        }
                        else{
                            echo "<li><a href='../". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
                        }
                    }
                    ?>

                </ul>
            </ul>
        </div>

    </div>
</nav>


<div id="header">
    <div id="main-background">
        <div class="image-overlay"></div>
    </div>
    <div class="container">
        <h1>ROOMQUEST</h1>
            <p>Never Feel Lost Again!</p>
            <div class="store-buttons">
                <!--<a href=""><img alt="Download RoomQuest from Google Play" src="../static/images/google-play-icon.svg"></a> -->
            </div>

    </div>
</div>


<div class="down-arrow">

</div>

<div id="features">
    <div class="container">
        <div class="feature-box">
            <div class="item">
                <div class="icon">
                    <img alt="" src="../static/images/roomquest/icon-3d.svg">
                </div>
                <div class="text">
                    <h2>3D Navigation</h2>
                    <p>This feature allows students to navigate to their destination.</p>
                </div>
            </div>
            <div class="item">
                <div class="icon">
                    <img alt="" src="../static/images/roomquest/icon-phone.svg">
                </div>
                <div class="text">
                    <h2>Touch Feature</h2>
                    <p>Allows maps to display room number, professor's name, and contact information.</p>
                </div>
            </div>
            <div class="item">
                <div class="icon">
                    <img alt="" src="../static/images/roomquest/icon-geo.svg">
                </div>
                <div class="text">
                    <h2>Geodatabase</h2>
                    <p>This makes it possible to locate rooms and faculty.</p>
                </div>
            </div>

        </div>
    </div>
</div>
</div>


<div id="app-display">
    <div class="container">
        <div class="content">
            <h2>Find Your Way!</h2>
            <p>RoomQuest is the ideal app for those who are still becoming familiar with the CSUSB campus. Students at both the main campus and the Palm Desert campus can use this app to quickly find where they are going. With RoomQuest, you'll never have trouble finding your destination again!</p>
        </div>
    </div>
</div>




<div class="rotated-screens">
</div>


<div id="app-info">
    <div class="container">

        <div class="specs">
            <h2>Specifications</h2>
            <ul>
                <li>
                    <span class="title">Operating System:</span>
                    <span>Android</span>
                </li>
                <li>
                    <span class="title">Current Version:</span>
                    <span>3.1</span>
                </li>
                <li>
                    <span class="title">Updated:</span>
                    <span>Not yet released.</span>
                </li>
            </ul>
        </div>

        <div class="changelog">
            <h2>What's New</h2>
            <ul>
                <li>-Information not yet available.</li>
            </ul>
        </div>
    </div>
</div>



<div id="download-bottom">
    <div class="container">
        <p>Stay connected with the campus community!</p>
        <div class="store-buttons">
            <a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img alt="Download CSUSB Mobile from the App Store" src="../static/images/app-store-icon.svg"></a>
            <a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img alt="Download CSUSB Mobile from Google Play" src="../static/images/google-play-icon.svg"></a>
        </div>
    </div>
</div>




<footer>
    <div class="container">
        <div class="links">
            <a href="../">Apps</a>
            <a href="../team">Team</a>
            <a href="mailto:mobileapps@csusb.edu">Contact</a>
        </div>
        <p class="copyright">
            Copyright &copy; 2016 CSUSB Mobile. All rights reserved
        </p>
    </div>
</footer>
</html>
