<!--=================================================
Bubblein APP PAGE
==================================================-->
<!DOCTYPE html>

<html lang="en-US">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="../static/css/skeleton.css">
<link rel="stylesheet" href="../static/css/bubblein.css">
<title>Bubble-In: A Test Taking App Developed at CSUSB</title>

<a href="#header">Skip Main Navigation</a>
<nav>
	<div class="container">
		<a href="../" class="logo">
			<img alt="California State University, San Bernardino" src="../static/images/csusb-logo.svg">
		</a>

		<div class="left-links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>

		<div class="right-links">
			<div class="common">
				<a href="../csusbmobile">CSUSB MOBILE</a>
			</div>

			<ul class="dropdown">
				<li><a href="#">MORE</a></li>
				<ul>
					<h2>All Apps</h2>
					<?php
						$path = "../static/data/apps.json";
						$data =  json_decode( file_get_contents($path), true ) ;
						$apps = $data['apps'];
						for( $i=0; $i< sizeof($apps); $i++){
							if(substr($apps[$i]['url'], 0, 4) == "http"){
								echo "<li><a href='". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
							else{
								echo "<li><a href='../". $apps[$i]['url'] ."' data-name='" . $apps[$i]['name'] . "'>" . $apps[$i]['name'] . "</a></li>";
							}
						}
					?>

				</ul>
			</ul>
		</div>

	</div>
</nav>


<div id="header">
	<div id="main-background">
		<div class="image-overlay"></div>
	</div>
	<div class="container">
		<h5>BUBBLE-IN</h3>
		<h1>Forget the Pencil at Home!</h1>
		<div class="store-buttons">
		</div>

	</div>
</div>


<div class="down-arrow">

</div>

<div id="features">
	<div class="container">
		<div class="feature-box">
			<div class="item">
				<div class="icon">
					<img src="../static/images/bubblein/icon-answersheet.svg">
				</div>
				<div class="text">
					<h1>Intuitive Answer Sheet</h1>
					<p>Bubble-In mimics features of an actual answer Sheet!</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/bubblein/icon-secure.svg">
				</div>
				<div class="text">
					<h1>Secure</h1>
					<p>Built in security features guarantee students will not cheat.</p>
				</div>
			</div>
			<div class="item">
				<div class="icon">
					<img alt="" src="../static/images/bubblein/icon-feedback.svg">
				</div>
				<div class="text">
					<h1>Instant Feedback</h1>
					<p>Scores are immediately calculated and stored. No more waiting!</p>
				</div>
			</div>

			</div>
		</div>
	</div>
</div>


<div id="app-display">
	<div class="container">
		<div class="content">
			<h1>Just Bring Your Phone!</h1>
			<p>Bubble-In is an app to help teachers and students take tests in an increasingly paperless world. Bubble-In replaces paper answer sheets with an application that allows students to take tests on their phones. With built in security features teachers can feel confident that students are focused on their answer sheet, and not looking up answers. In an increasingly digital world of instant feedback, leave the paper at home!</p>
		</div>
	</div>
</div>




<div class="rotated-screens">
</div>


<div id="app-info">
	<div class="container">

		<div class="specs">
			<h5>Specifications</h5>
			<ul>
				<li>
					<span class="title">Operating System:</span>
					<span>Android or iOS 7+</span>
				</li>
				<li>
					<span class="title">Current Version:</span>
					<span>1.0</span>
				</li>
				<li>
					<span class="title">Updated:</span>
					<span>Not yet released</span>
				</li>
			</ul>
		</div>

		<div class="changelog">
			<h5>What's New</h5>
			<ul>
				<li>Information not yet available.</li>
			</ul>
		</div>
	</div>
</div>



<div id="download-bottom">
	<div class="container">
		<h1>Stay connected with the campus community!</h1>
		<div class="store-buttons">
			<a href="https://itunes.apple.com/us/app/csusb-mobile/id473101799?mt=8#"><img alt="Download CSUSB Mobile from the App Store" src="../static/images/app-store-icon.svg"></a>
			<a href="https://play.google.com/store/apps/details?id=mobile.coyote&hl=en"><img alt="Download CSUSB Mobile from Google Play" src="../static/images/google-play-icon.svg"></a>
		</div>
	</div>
</div>




<footer>
	<div class="container">
		<div class="links">
			<a href="../">Apps</a>
			<a href="../team">Team</a>
			<a href="mailto:mobileapps@csusb.edu">Contact</a>
		</div>
		<p class="copyright">
			Copyright &copy; 2016 CSUSB Mobile. All rights reserved
		</p>
	</div>
</footer>
</html>
