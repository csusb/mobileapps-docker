<?php

//create a new curl resourse
$ch = curl_init();
//set URL and other appropriate options
curl_setopt($ch, CURLOPT_PROXY, '139.182.75.5:3128');
curl_setopt($ch, CURLOPT_URL,"https://search.csusb.edu/events/type/Celebration");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//grab URL and pass it to the browser
$result = curl_exec($ch);
$events = array();

// use regular expresion to capture events each $match captures contents
// between ()
preg_match_all('!<strong>(.*?)<.*?><.*?>\s\s\s\s\s<.*?>(.*?)<.*>\s\s\s<.*?>'.
'\s\s\s<.*?>\s\s\s\s\s<.*?><a href="(.*?)"\s.*?>(.*?)<\/a><.*?>\s\s\s\s\s\s'.
'\s\s\s\s\s<.*>\s\s\s\s\s\s\s\s\s<.*?><.*?><.*?>\s(.*?)<!',$result,$match);
$months = $match[1];
$days = $match[2];
$times = $match[5];
$title = $match[4];
$link = $match[3];

// for loop takes the arrays created by using $match and populates $events array
// counter starts at 4 because events in "upcomming events" table in the corner 
//of page are structured identicaly to events needed
for($i = 4; $i < sizeof($months);$i++)
{
    $events[] = array(
        "date"  => $months[$i] . "," .$days[$i],
        "time"  => $times[$i],
        "name"  => $title[$i],
        "link"  => 'https://search.csusb.edu'. $link[$i],
        "image" => null);
}

// close cURL resource, and free up system resources
curl_close($ch);

echo json_encode($events);




?>
