<?php
    session_start();
?>

<?php
$host='mysql';
$username='root';
$password='secret'; 
$db='feedback01';

$conn = new PDO("mysql:host=$host;dbname=$db", $username, $password);

if (!$conn)
  {
  die('Could not connect: ');
  }
  
  $query="SELECT * From auth WHERE passcode=:passcode";
  $stmt = $conn->prepare($query);
  $stmt->bindParam(":passcode", $_POST['passcode']);
  $stmt->execute();
  $result = $stmt->fetchAll();
  
  $response = array();
  if($stmt->rowCount() > 0)
    {
		$response["code"] = "login_success";
		echo json_encode(array($response));
    }
    else{
        $response["code"] = "login_failed";
        echo json_encode(array($response));
    }
 ?>
