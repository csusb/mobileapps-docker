<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
require 'libphp-phpmailer/autoload.php';
session_start();
$servername = "mysql";
$username = "root";
$password = "secret";
$dbname = "feedback01";

try {
//Creating connection for mysql
$conn = new PDO("mysql:host=$servername; dbname=$dbname", $username, $password);
// set the PDO error mode to exception
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$stmt = $conn->prepare("INSERT INTO feedbackTB VALUES (:firstname, :lastname, :studentID, :email, :messages)");
    $stmt->bindParam(':firstname', $_POST['FIRSTNAME']);
    $stmt->bindParam(':lastname', $_POST['LASTNAME']);
    $stmt->bindParam(':studentID', $_POST['STUDENTID']);
    $stmt->bindParam(':email', $_POST['COYOTEEMAIL']);
    $stmt->bindParam(':messages', $_POST['MESSAGES']);
    
$stmt->execute(); 
echo "Submitted Successfully";
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}

//Create a email body
$body = 
    'First name: '.$_POST['FIRSTNAME'].' <br/>
    Last name: '.$_POST['LASTNAME'].' <br/>
    Student ID: '.$_POST['STUDENTID'].' <br/>
    Student Email: '.$_POST['COYOTEEMAIL'].' <br/>
    Message: '.$_POST['MESSAGES'].'
    ';

//SMTP
try {
    
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
	$mail->IsSMTP(); // enable SMTP
	$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
	$mail->SMTPAuth = false; // authentication enabled
	$mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail ssl
	$mail->Host = "mailgateway.csusb.edu"; //This shuold be mail server
	$mail->Port = 25; // or 587 465
	$mail->IsHTML(true);
	//Authentication
//	$mail->Username = "stmp username";
//	$mail->Password = "stmp password";
	//Compose
	$mail->SetFrom($_POST['COYOTEEMAIL'], $_POST['FIRSTNAME']." ". $_POST['LASTNAME']);
	$mail->AddReplyTo($_POST['COYOTEEMAIL']);
    $mail->Subject = "Feedback";
	$mail->Body = $body;
	//Send to
    $mail->AddAddress('grijalva@csusb.edu'); // Add a recipient
    $mail->addCC('mba@csusb.edu');
//	$mail->AddAddress("004999913@coyote.csusb.edu", "MBA"); // Add a recipient
    //mail function only works when you have mail server 
    $mail->send();
}
catch (PDOException $e) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
    

$conn = null;
?>
