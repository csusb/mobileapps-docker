<?php
     $content = [ "Calendar" => [
    [
            "Date"  =>"Sep. 13. 2017",
            "Note" =>"Academic Year Begins",
    ],
    [
            "Date"  =>"Sep. 21. 2017",
            "Note" =>"Fall 2017 First Day of Classes",
    ],
    [
            "Date"  =>"Sep. 23. 2017",
            "Note" =>"Fall 2017 First Day of Saturday Classes",
    ],
    [
            "Date"  =>"Sep. 28. 2017",
            "Note" =>"Fall 2017 Last Day to Add Open Classes",
    ],
    [
            "Date"  =>"Oct. 11. 2017",
            "Note" =>"Fall 2017 Census",
    ],
    [
            "Date"  =>"Oct. 16-27. 2017",
            "Note" =>"Winter 2018 Graduate Advising",
    ],
    [
            "Date"  =>"Oct. 20. 2017",
            "Note" =>"Winter 2018 Registration Access e-mailed",
    ],
    [
            "Date"  =>"Nov. 10. 2017",
            "Note" =>"Veterans Day Holiday—Campus Closed",
    ],
    [
            "Date"  =>"Nov. 24-26. 2017",
            "Note" =>"Thanksgiving Day Holiday—Campus Closed",
    ],
    [
            "Date"  =>"Nov. 10. 2017",
            "Note" =>"Fall 2017 Last Day of Monday classes",
    ],
    [
            "Date"  =>"Dec. 02. 2017",
            "Note" =>"Fall 2017 Last Day of Saturday Classes",
    ],
    [
            "Date"  =>"Dec. 04. 2017",
            "Note" =>"Fall 2017 Last Day of Classes",
    ],
    [
            "Date"  =>"Dec. 05-09. 2017",
            "Note" =>"Fall 2017 Final Exams",
    ],
    [
            "Date"  =>"Dec. 09. 2017",
            "Note" =>"Fall Commencement",
    ],
    [
            "Date"  =>"Dec. 23-31. 2017",
            "Note" =>"Winter Break—Campus Closed",
    ],
    [
            "Date"  =>"Jan. 01. 2018",
            "Note" =>"New Year’s Day—Campus Closed",
    ],
    [
            "Date"  =>"Jan. 05. 2018",
            "Note" =>"Winter 2018 Full Refund Deadline",
    ],
    [
            "Date"  =>"Jan. 06. 2018",
            "Note" =>"Winter 2018 First Day of Saturday Classes",
    ],
    [
            "Date"  =>"Jan. 08. 2018",
            "Note" =>"Winter 2018 First Day of Classes",
    ],
    [
            "Date"  =>"Jan. 12. 2018",
            "Note" =>"Winter 2018 Last Day to Add Open Classes",
    ],
    [
            "Date"  =>"Jan. 13-15. 2018",
            "Note" =>"Martin Luther King Holiday—Campus Closed
                      \nNo Saturday and Monday classes",
    ],
    [
            "Date"  =>"Jan. 29. 2018",
            "Note" =>"Winter 2018 Census",
    ],
    [
            "Date"  =>"Jan. 29-31. 2018",
            "Note" =>"Spring 2018 Graduate Advising begins",
    ],
    [
            "Date"  =>"Feb. 01-02. 2018",
            "Note" =>"Spring 2018 Graduate Advising",
    ],
    [
            "Date"  =>"Feb. 02. 2018",
            "Note" =>"Spring 2018 Registration Access e-mailed",
    ],
    [
            "Date"  =>"Feb. 05. 2018",
            "Note" =>"Spring 2018 Advising begins",
    ],
    [
            "Date"  =>"March. 17. 2018",
            "Note" =>"Winter 2018 Last Day of Saturday Classes",
    ],
    [
            "Date"  =>"March. 19. 2018",
            "Note" =>"Winter 2018 Last Day of Classes",
    ],
    [
            "Date"  =>"March. 20-24. 2018",
            "Note" =>"Winter 2018 Final Exams",
    ],
    [
            "Date"  =>"March. 26-29. 2018",
            "Note" =>"Spring 2018 Break",
    ],
    [
            "Date"  =>"March. 30. 2018",
            "Note" =>"Cesar Chavez Holiday—Campus Closed",
    ],
    [
            "Date"  =>"March. 31. 2018",
            "Note" =>"Spring 2018 First Day of Saturday Classes",
    ],
    [
            "Date"  =>"April. 02. 2018",
            "Note" =>"Spring 2018 First Day of Classes",
    ],
    [
            "Date"  =>"April. 06. 2018",
            "Note" =>"Spring 2018 Last Day to Add Open Classes",
    ],
    [
            "Date"  =>"April. 16-27. 2018",
            "Note" =>"Summer 2018 Graduate Advising",
    ],
    [
            "Date"  =>"April. 20. 2018",
            "Note" =>"Spring 2018 Census",
    ],
    [
            "Date"  =>"April. 30. 2018",
            "Note" =>"Fall 2018 Graduate Advising begins",
    ],
    [
            "Date"  =>"May. 01-11. 2018",
            "Note" =>"Fall 2018 Graduate Advising",
    ],
    [
            "Date"  =>"May. 04. 2018",
            "Note" =>"Fall 2018 Registration Access e-mailed",
    ],
    [
            "Date"  =>"May. 07. 2018",
            "Note" =>"Fall 2018 Advising begins",
    ],
    [
            "Date"  =>"May. 26-28. 2018",
            "Note" =>"Memorial Day Holiday
            \nNo Saturday and Monday classes",
    ],
    [
            "Date"  =>"June. 09. 2018",
            "Note" =>"Spring 2018 Last Day of Saturday Classes",
    ],
    [
            "Date"  =>"June. 11. 2018",
            "Note" =>"Spring 2018 Last Day of Classes",
    ],
    [
            "Date"  =>"June. 12-16. 2018",
            "Note" =>"Spring 2018 Final Exams",
    ],
    [
            "Date"  =>"June. 16. 2018",
            "Note" =>"Spring Commencement",
    ],
    ]
];

  // Convert Array to JSON String
  $objectJSON = json_encode($content);
  echo $objectJSON;
?>