<?php
     $someArray = [ "Event" => [
    [
            "Date"  =>"October 14 — 2:00 p.m.-5:00 p.m",
            "Event" =>"MBA Oktoberfest",
            "Hosts" =>"Hosted By MBA students, faculty, and staff. (RSVP REQUIRED)",
            "Location" => "Location: Escape Brewery, 721 Nevada St #401, Redlands, CA 92373",
    ],
    [
            "Date"  =>"November 11, 2017",
            "Event" =>"Garner Holt Student Fast Pitch Competition",
            "Hosts" =>"Hosted By Inland Empire Center for Entrepreneurship (IECE)",
            "Location" => "Location: TBA",
    ],
    [
            "Date"  =>"November 14, 2017",
            "Event" =>"Spirit of the Entrepreneur",
            "Hosts" =>"Hosted By: TBA. (RSVP REQUIRED)",
            "Location" => "Location: Riverside Convention Center, 3637 Fifth Street, Riverside, CA 92501",
    ],
    [
            "Date"  =>"October 25, 2017 4:30 pm—5:30 pm",
            "Event" =>"MBA Alumni Speaker Series",
            "Hosts" =>"Hosted by: Deborah Grijalva",
            "Location" => "Location: Jack Brown Hall, Room 276",
    ],
    [
            "Date"  =>"Date: Please contact Student Success Center for event information 909-537-3358",
            "Event" =>"Jack H. Brown College Student Alliance",
            "Hosts" =>"Hosted by: TBA",
            "Location" => "Location: TBA",
    ],
    
    [
            "Date"  =>"October 6, 2017 12:00 pm—2:00pm",
            "Event" =>"Resumes 101",
            "Hosts" =>"Hosted by Student Success Center",
            "Location" => "Location: Jack Brown Hall, Room 111",
    ],
    
    [
            "Date"  =>"October 13, 2017 12:00 pm—2:00pm",
            "Event" =>"Interview Skills",
            "Hosts" =>"Hosted by Student Success Center",
            "Location" => "Location: Jack Brown Hall, Room 111",
    ],
    
    [
            "Date"  =>"October 20, 2017 12:00 pm—2:00pm",
            "Event" =>"Intro to Internship",
            "Hosts" =>"Hosted by Student Success Center",
            "Location" => "Location: Jack Brown Hall, Room 111",
    ],
    
    [
            "Date"  =>"October 27, 2017 12:00 pm—2:00pm",
            "Event" =>"LinkedIn Basics",
            "Hosts" =>"Hosted by Student Success Center",
            "Location" => "Location: Jack Brown Hall, Room 111",
    ],
    
    [
            "Date"  =>"October 2, 2017 4:00 pm—6:00 pm",
            "Event" =>"Career Services: Career Planning",
            "Hosts" =>"Hosted by Pamela Abell",
            "Location" => "Location: Jack Brown Hall, Room 280",
    ],
    
    [
            "Date"  =>"October 16, 2017 4:00 pm—6:00 pm",
            "Event" =>"Career Services: Speech Craft",
            "Hosts" =>"Hosted by Pamela Abell",
            "Location" => "Location: Jack Brown Hall, Room 280",
    ],
    
    [
            "Date"  =>"October 30, 2017 4:00 pm—6:00 pm",
            "Event" =>"Career Services: Interview Skills",
            "Hosts" =>"Hosted by Pamela Abell",
            "Location" => "Location: Jack Brown Hall, Room 280",
    ],
    
    [
            "Date"  =>"February 2, 2018 12:00 pm—2:00pm",
            "Event" =>"Resumes 101",
            "Hosts" =>"Hosted by Student Success Center",
            "Location" => "Location: TBD",
    ],
    
    [
            "Date"  =>"February 9, 2018 2018 12:00 pm—2:00pm",
            "Event" =>"Interview Skills",
            "Hosts" =>"Hosted by Student Success Center",
            "Location" => "Location: TBD",
    ],
    
    [
            "Date"  =>"February 16, 2018, 2018 12:00 pm—2:00pm",
            "Event" =>"Intro to Internship",
            "Hosts" =>"Hosted by Student Success Center",
            "Location" => "Location: TBD",
    ],
    
    [
            "Date"  =>"February 23, 2018 12:00 pm—2:00pm",
            "Event" =>"LinkedIn Basics",
            "Hosts" =>"Hosted by Student Success Center",
            "Location" => "Location: TBD",
    ],
    
    [
            "Date"  =>"January 29, 2018 4:00 pm—6:00 pm",
            "Event" =>"Career Services: Career Planning",
            "Hosts" =>"Hosted by Pamela Abell",
            "Location" => "Location: Jack Brown Hall, Room 280",
    ],
    
    [
            "Date"  =>"February 12, 2018 4:00 pm—6:00 pm",
            "Event" =>"Career Services: Speech Craft",
            "Hosts" =>"Hosted by Pamela Abell",
            "Location" => "Location: Jack Brown Hall, Room 280",
    ],
    
    [
            "Date"  =>"February 26, 2018 4:00 pm—6:00 pm",
            "Event" =>"Career Services: Interview Skills",
            "Hosts" =>"Hosted by Pamela Abell",
            "Location" => "Location: Jack Brown Hall, Room 280",
    ],
    
    [
            "Date"  =>"Sunday, February 18, 2018—10:00 a.m.—12:00 p.m",
            "Event" =>"Brunch with an Alumni: RSVP REQUIRED",
            "Hosts" =>"Hosted by MBA",
            "Location" => "Location: The Castaway—670 N. Kendall Drive, San Bernardino, CA 92407",
    ],
    
    [
            "Date"  =>"February 21, 2018 4:30 pm—5:30 pm",
            "Event" =>"MBA: Alumni Speaker Series",
            "Hosts" =>"Hosted by Deborah Grijalva",
            "Location" => "Location: Jack Brown Hall, Room 276",
    ],
    
    [
            "Date"  =>"Please contact Student Success Center for event information 909-537-335",
            "Event" =>"SSC: Business Madness",
            "Hosts" =>"Hosted by: TBA",
            "Location" => "Location: TBA",
    ],
    
    [
            "Date"  =>"Please contact Student Success Center for event information 909-537-335",
            "Event" =>"Jack H. Brown Student Alliance",
            "Hosts" =>"Hosted by: TBA",
            "Location" => "Location: TBA",
    ],
    
    [
            "Date"  =>"Spring 2018 Date TBD, 7:00 p.m.- 9:00 p.m",
            "Event" =>"MBA Alumni Mixer",
            "Hosts" =>"Hosted by Robert & Francis Fullerton Museum of Art (RAFFMA) RSVP REQUIRED.",
            "Location" => "Location: 5500 University Parkway, San Bernardino, CA 92407-2397",
    ],
    [
            "Date"  =>"Spring 2018",
            "Event" =>"Jack H. Brown Student Alliance",
            "Hosts" =>"Hosted by: TBA",
            "Location" => "Location: Please contact Student Success Center for event information 909-537-335",
    ],
    [
            "Date"  =>"Spring 2018",
            "Event" =>"Graduate Studies: Meeting of the Minds",
            "Hosts" =>"Hosted by: TBA",
            "Location" => "Location: Please contact Graduate Studies for event information 909-537-5030",
    ],
    [
            "Date"  =>"Spring 2018, various Dates/times",
            "Event" =>"Professional Tool Box Series ",
            "Hosts" =>"Hosted by IECE Department",
            "Location" => "Location: TBA",
    ],
    
    ]
];

  // Convert Array to JSON String
  $someJSON = json_encode($someArray);
  echo $someJSON;
?>