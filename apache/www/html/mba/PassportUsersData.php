<?php
//mailer
require 'libphp-phpmailer/autoload.php';

$servername = "mysql";
$username = "root";
$password = "secret";
$dbname = "feedback01";
try {
//Creating connection for mysql
$conn = new PDO("mysql:host=$servername; dbname=$dbname", $username, $password);
// set the PDO error mode to exception
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$stmt = $conn->prepare("INSERT INTO PassportData VALUES (:date, :name, :studentID, :events)");
    $stmt->bindParam(':date', $_POST['Date']);
    $stmt->bindParam(':name', $_POST['Name']);
    $stmt->bindParam(':studentID', $_POST['StudentID']);
    $stmt->bindParam(':events', $_POST['Events']);
    
$stmt->execute(); 
echo "Submitted Successfully";
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}

//Create a email body
$body = 
    'Date: '.$_POST['Date'].' <br/>
    Name: '.$_POST['Name'].' <br/>
    Student ID: '.$_POST['StudentID'].' <br/>
    Event: '.$_POST['Events'].'
    ';
//SMTP
try {
    
	//PHP mailer functions
	// right here...
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
	$mail->IsSMTP(); // enable SMTP
	$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
	$mail->SMTPAuth = false; // authentication enabled
	$mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail ssl
	$mail->Host = "mailgateway.csusb.edu"; //This shuold be mail server
	$mail->Port = 25; // or 587 465
	$mail->IsHTML(true);	
	
	//Compose
	$mail->SetFrom('noreply@csusb.edu', $_POST['Name']);
//	$mail->AddReplyTo($_POST['COYOTEEMAIL']);
    $mail->Subject = "MBA: Event attendance";
	$mail->Body = $body;
	//Send to
    $mail->AddAddress('grijalva@csusb.edu'); // Add a recipient
    $mail->addCC('mba@csusb.edu');
//	$mail->AddAddress("005259367@coyote.csusb.edu", "MBA"); // Add a recipient
    //$mail->addCC('MBA@csusb.edu');
    //mail function only works when you have mail server 
    $mail->send();
}
catch (PDOException $e) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}


$conn = null;
?>
