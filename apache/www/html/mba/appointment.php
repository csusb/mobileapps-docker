<?php

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
require 'libphp-phpmailer/autoload.php';

$servername = "mysql";
$username = "root";
$password = "secret";
$dbname = "feedback01";
try {
//Creating connection for mysql
$conn = new PDO("mysql:host=$servername; dbname=$dbname", $username, $password);
// set the PDO error mode to exception
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$stmt = $conn->prepare("INSERT INTO appTB VALUES (:firstname, :lastname, :studentID, :email, :date, :time)");
    $stmt->bindParam(':firstname', $_POST['FIRSTNAME']);
    $stmt->bindParam(':lastname', $_POST['LASTNAME']);
    $stmt->bindParam(':studentID', $_POST['STUDENTID']);
    $stmt->bindParam(':email', $_POST['COYOTEEMAIL']);
    $stmt->bindParam(':date',  $_POST['DATE']);
	$stmt->bindParam(':time',  $_POST['TIME']);
    
$stmt->execute(); 
echo "Submitted Successfully, we will send you a confirmation email soon!";
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}

//Create a email body
$body = 
    'First name: '.$_POST['FIRSTNAME'].' <br/>
    Last name: '.$_POST['LASTNAME'].' <br/>
    Student ID: '.$_POST['STUDENTID'].' <br/>
    Student Email: '.$_POST['COYOTEEMAIL'].' <br/>
    Date: '.$_POST['DATE'].'<br/>
    Time: '.$_POST['TIME'].'
    ';
//SMTP
try {
    
	//PHP mailer functions
	// right here...
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
	$mail->IsSMTP(); // enable SMTP
	$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
	$mail->SMTPAuth = false; // authentication enabled
	$mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail ssl
	$mail->Host = "mailgateway.csusb.edu"; //This shuold be mail server
	$mail->Port = 25; // or 587 465
	$mail->IsHTML(true);	
	
	//Compose
	$mail->SetFrom($_POST['COYOTEEMAIL'], $_POST['FIRSTNAME']." ". $_POST['LASTNAME']);
	$mail->AddReplyTo($_POST['COYOTEEMAIL']);
    $mail->Subject = "MBA: Request appointment";
	$mail->Body = $body;
	//Send to

    $mail->AddAddress('grijalva@csusb.edu'); // Add a recipient
    $mail->addCC('mba@csusb.edu');
//	$mail->AddAddress("005259367@coyote.csusb.edu", "MBA"); // Add a recipient
    //$mail->addCC('MBA@csusb.edu'); 
   //mail function only works when you have mail server 
    $mail->send();
}
catch (PDOException $e) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}



$conn = null;
?>
