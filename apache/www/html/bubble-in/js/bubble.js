$(document).ready(function () {

    console.log('ready');

    if (typeof bank !== 'undefined') {
        $('#addQ').on('submit', function (e) {
            e.preventDefault(e);
            var that = this;

            $.ajax({

                type:"POST",
                url:'/course/'+course+'/banks/'+bank+'/addQuestion',
                data:$(this).serialize(),
                dataType: 'json',
                success: function(data){
                    that.reset();
                },
                error: function(data){

                }
            });

            that.reset();
            $('#questions').load('/course/'+course+'/banks/'+bank+' #questions', function () {
                console.log('Load was performed');
            });

        });
    }


});