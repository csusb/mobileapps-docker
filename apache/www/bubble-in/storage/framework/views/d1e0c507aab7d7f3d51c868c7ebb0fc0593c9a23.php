<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="heading-left"><?php echo e($name); ?> Test Bank</div>
                    <div class="heading-right"><button href="#" role="button" data-toggle='modal' data-target="#newQuestion" >Add Questions</button></div>
                    <div class="heading-right"><button href="#" role="button" data-toggle='modal' data-target="#bulkBank" >Bulk Upload</button></div>
                </div>
                <div class="panel-body">
                    <ol id="questions">
                    <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $q): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <li class="panel-title"><?php echo e($q->question); ?> </li>
                            <ol>
                            <?php $__currentLoopData = $q->options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <li type="a"
                                    <?php if($option->correct): ?>
                                        class="correct"
                                    <?php endif; ?>
                                ><?php echo e($option->name); ?></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </ol>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </ol>

                </div>
            </div>
        </div>
    </div>
</div>

<!------------------------------ Add Question Modal -------------------------------->
<div class="modal fade" id="newQuestion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Test Bank</h4>
            </div>
            <?php echo e(Form::open(['url'=>'addQuestion','id'=>'addQ'])); ?>

            <div class="modal-body">
                <?php echo e(Form::label('question', 'Question')); ?> <br />
                <?php echo e(Form::text('question', null,['required' => true])); ?>

                <br />

                <?php echo e(Form::label('correct', 'correct answer')); ?> <br />
                <?php echo e(Form::text('correct', null,['required' => true])); ?>

                <br />

                <?php echo e(Form::label('wrong[]', 'wrong answer')); ?> <br />
                <?php echo e(Form::text('wrong[]', null,['required' => true])); ?>

                <br />
                <?php echo e(Form::text('wrong[]')); ?>

                <br />
                <?php echo e(Form::text('wrong[]')); ?>

                <br />
                <?php echo e(Form::text('wrong[]')); ?>

                <br />
                <?php echo e(Form::text('wrong[]')); ?>

                <br />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?php echo e(Form::submit('Add Question', ['class' => 'btn btn-primary'])); ?>

            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
</div>
<!------------------------------ Add Question Modal -------------------------------->

<!------------------------------ Bulk Upload Modal -------------------------------->
<div class="modal fade" id="bulkBank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Test Bank</h4>
            </div>
            <?php echo e(Form::open(['url'=>'/course/'.$bank->course_id.'/banks/'.$bank->id.'/bulkAddQuestion','id'=>'addQ','file'=>true, 'enctype'=>'multipart/form-data'])); ?>

            <div class="modal-body">
                <?php echo e(Form::label('file', 'Select a .csv file to upload')); ?> <br />
                <?php echo e(Form::file('file', ['required' => true])); ?>

                <br />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?php echo e(Form::submit('Add Question', ['class' => 'btn btn-primary'])); ?>

            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
</div>
<!------------------------------ Bulk Upload Modal -------------------------------->
    <script>
        var bank = <?php echo e($bank->id); ?>

        var course = <?php echo e($bank->course_id); ?>

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>