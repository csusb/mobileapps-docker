<?php $__env->startSection('content'); ?>

    <?php
        $stu_answer = str_split($student->answer);
        $answers = str_split($test->answer_key);
    ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="heading-left">Test Results For <a href="<?php echo e(url('/scores/'.$test->id)); ?>"><?php echo e($test->name); ?> : <?php echo e($test->code); ?></a></div>
                    <div class="heading-right"><?php echo e($course); ?></div>
                </div>

                <div class="panel-body">
                    Name: <?php echo e($student->stu_name); ?> <br />
                    ID: <?php echo e($student->stu_id); ?> <br />
                    Score: <?php echo e($score); ?>/<?php echo e(strlen($test->answer_key)); ?> <br />

                    <ol>
                        <?php $__currentLoopData = $answers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$answer): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <li>
                            <?php echo e($answer); ?>  <?php echo e($stu_answer[$i]); ?>

                                <?php if($answer == $stu_answer[$i]): ?>
                                    <p class="testmark correct">&#10004</p>
                                <?php else: ?>
                                    <p class="testmark wrong">&#10006</p>
                                <?php endif; ?>
                          </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </ol>

                    <a class="printme" href="javascript:window.print()">print this page</a>


                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>