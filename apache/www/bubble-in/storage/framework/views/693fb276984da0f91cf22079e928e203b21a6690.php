<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <ol>
                            <?php $__currentLoopData = $vals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <li><?php echo e($val['question']->question); ?></li>
                                <ol type="a">
                                    <?php $__currentLoopData = $val['options']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$option): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <li
                                            <?php if($option->correct): ?>
                                                class="correct"
                                            <?php elseif($val['answer'] === ($i)): ?>
                                                class="wrong"
                                            <?php endif; ?>
                                        ><?php echo e($option->name); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </ol>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>