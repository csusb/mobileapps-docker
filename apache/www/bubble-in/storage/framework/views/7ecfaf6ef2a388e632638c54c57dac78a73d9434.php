<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create A New Test</div>

                <div class="panel-body">
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger" role="alert">
                            <strong><?php echo e($errors->first()); ?></strong>
                            <a href="<?php echo e(url('course/'.$course.'/banks/'.session('bank')[0])); ?>">View Bank</a>
                        </div>
                    <?php endif; ?>

                    <?php echo e(Form::open(['url' => '/course/'.$course.'/tests','method'=>'POST'])); ?>

                        <?php echo e(Form::hidden('cid',$course)); ?>

                        <?php echo e(Form::label('name', 'test name')); ?> <br />
                        <?php echo e(Form::text('name')); ?>

                    <br />

                    <?php echo e(Form::label('questions', 'number of questions')); ?> <br />
                    <?php echo e(Form::number('questions')); ?>

                    <br />

                    <?php echo e(Form::label('tests', 'number of tests')); ?> <br />
                    <?php echo e(Form::number('tests')); ?>

                    <br />

                    <?php echo e(Form::label('banks', 'Select a test bank')); ?> <br />
                    <?php echo e(Form::select('banks', $banks, null, ['placeholder' => 'None'])); ?>

                    <br />

                    <?php echo e(Form::label('sameQs', 'Use same questions on all tests')); ?> <br />
                    <?php echo e(Form::checkbox('sameQs','yes', false)); ?>

                    <br />

                    <?php echo e(Form::label('sameT', 'Generate Exact same test for all students')); ?> <br />
                    <?php echo e(Form::checkbox('sameT','yes', false)); ?>

                    <br />

                    <?php echo e(Form::label('sdate', 'start date')); ?> <br />
                    <?php echo e(Form::date('sdate')); ?>

                    <br />

                    <?php echo e(Form::label('stime', 'start time')); ?> <br />
                    <?php echo e(Form::time('stime')); ?>

                    <br />

                    <?php echo e(Form::label('edate', 'end date')); ?> <br />
                    <?php echo e(Form::date('edate')); ?>

                    <br />

                    <?php echo e(Form::label('etime', 'end time')); ?> <br />
                    <?php echo e(Form::time('etime')); ?>

                    <br />

                        <?php echo e(Form::submit('new test')); ?>

                    <?php echo e(Form::close()); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>