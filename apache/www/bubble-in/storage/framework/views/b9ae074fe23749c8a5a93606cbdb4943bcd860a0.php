<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tests</div>

                <div class="panel-body">

                    <?php if(count($tests) < 1): ?>
                        <a href="<?php echo e(url('/create/'.$course)); ?>">Create Your First Test!</a>
                    <?php else: ?>
                        <table class="testtable">
                            <thead>
                                <th>Test Name</th>
                                <th></th>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $tests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $test): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <tr class="testrow">
                                    <td><?php echo e($test->name); ?></td>
                                    <td><a href="<?php echo e(url('/course/'.$test->course_id.'/tests/'.$test->id.'/submissions')); ?>">scores</a></td>
                                    <td><a href="<?php echo e(url('/course/'.$test->course_id.'/tests/'.$test->id.'/edit')); ?>">edit</a></td>
                                    <td> <?php echo Form::open(['method'=>'DELETE', 'url'=>'/course/'.$test->course_id.'/tests/'.$test->group]); ?>

                                        <button data-toggle="tooltip" data-placement="top" class="delIcon" title="Delete" type="submit" onclick="return confirm('Are you sure you want to delete this item?');"><img src="/img/ic_delete_forever_black_48px.svg" class="delete icon"></img></button>
                                        <?php echo Form::close(); ?>

                                    </td>

                                </tr>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </tbody>
                        </table>

                        <button class="newtest"><a href="<?php echo e(url('/course/'.$course.'/tests/create')); ?>">New Test</a></button>

                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>