<?php 
    $prefix = '/course/'.$test->course_id.'/tests/'.$test->id.'/submissions/';
 ?>

<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="heading-left">Test Results For <?php echo e($test->name); ?> : <?php echo e($test->code); ?></div>
                        <div class="heading-right"><?php echo e($test->code); ?></div>

                    </div>

                    <div class="panel-body">

                        <table>
                            <thead>
                            <th>Test CODE</th>
                            <th>Student ID</th>
                            <th>Student Name</th>
                            <th>Scores</th>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $tests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$test): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <?php $__currentLoopData = $test->submissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $submission): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <tr>
                                        <td><?php echo e($test->code); ?></td>
                                        <td><?php echo e($submission->stu_id); ?></td>
                                        <td><?php echo e($submission->stu_name); ?></td>
                                        <td><?php echo e($scores[$i]); ?>/<?php echo e($questions); ?></td>
                                        <td>
                                            <a href="<?php echo e(url('/course/'.$test->course_id.'/tests/'.$test->id.'/submissions/'.$submission->id)); ?>">Review</a>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                <?php if(count($test->submissions) < 1): ?>
                                    <tr>
                                        <td><?php echo e($test->code); ?></td>
                                        <td>No Submissions</td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <a href="<?php echo e(url('/course/'.$test->course_id.'/tests/'.$test->id)); ?>">Review</a>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </tbody>

                        </table>

                        <a class="btn btn-default" href="<?php echo e(url('/scores/'.$test->id.'/export')); ?>">Download</a>
                        <a class="btn btn-default"
                           href="<?php echo e(url('/course/'.$test->course_id.'/tests/'.$test->id.'/stats')); ?>">View
                            Statistics</a>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>