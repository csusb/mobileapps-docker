<?php $__env->startSection('content'); ?>

    <?php

        $answers = str_split($test->answer_key);
        $today = new \Carbon\Carbon();

            $start = explode(" ", $test->start);
            $startdate = new \Carbon\Carbon($start[0]);
            $starttime = new \Carbon\Carbon($start[1]);

            $end = explode(" ", $test->end);
            $enddate = new \Carbon\Carbon($end[0]);
            $endtime = new \Carbon\Carbon($end[1]);

    ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="heading-left">Edit <?php echo e($test->name); ?> : <?php echo e($test->code); ?></div>
                    <div class="heading-right"><?php echo e($course); ?></div>

                </div>

                <div class="panel-body">

                    <?php echo e(Form::open(['url'=>'/course/'.$test->course_id.'/tests/'.$test->id, 'method'=>'PUT'])); ?>

                    <?php if($test->answer_key == null): ?>
                        <h3>This test was generated from a test bank</h3>
                        <h4>Bank: <?php echo e($bank); ?></h4>
                        <p>Number of questions: <?php echo e($questions); ?></p>
                        <p>Number of tests generated: <?php echo e($tests); ?></p>
                    <?php else: ?>
                        <?php for($i=0; $i < strlen($test->answer_key); $i++): ?>
                                <?php echo e($i+1); ?>

                                <?php echo e(Form::label( $i,'A')); ?>

                                <?php echo e(Form::radio($i,'A',($answers[$i] == 'A'),['class'=>'ans','required'])); ?>

                                <?php echo e(Form::label($i,'B')); ?>

                                <?php echo e(Form::radio($i,'B',($answers[$i] == 'B'),['class'=>'ans','required'])); ?>

                                <?php echo e(Form::label($i,'C')); ?>

                                <?php echo e(Form::radio($i,'C',($answers[$i] == 'C'),['class'=>'ans','required'])); ?>

                                <?php echo e(Form::label($i,'D')); ?>

                                <?php echo e(Form::radio($i,'D',($answers[$i] == 'D'),['class'=>'ans','required'])); ?>

                                <br />
                        <?php endfor; ?>
                    <?php endif; ?>

                    <?php if($answers[0] != "0"): ?>


                            <?php echo e(Form::label('sdate', 'start date')); ?> <br />
                            <?php echo e(Form::date('sdate', $startdate)); ?>

                            <br />

                            <?php echo e(Form::label('stime', 'start time')); ?> <br />
                            <?php echo e(Form::time('stime', $starttime->format('H:m'))); ?>

                            <br />

                        <?php if($test->end < $today): ?>
                            <p class="expired">This test has expired. Change the end date to open it again</p>
                        <?php endif; ?>
                            <?php echo e(Form::label('edate', 'end date')); ?> <br />
                            <?php echo e(Form::date('edate', $enddate)); ?>

                            <br />

                            <?php echo e(Form::label('etime', 'end time')); ?> <br />
                            <?php echo e(Form::time('etime', $endtime->format('H:m'))); ?>

                            <br />

                    <?php endif; ?>


                    <?php echo e(Form::submit('Save')); ?>

                    <?php echo e(Form::close()); ?>



                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>