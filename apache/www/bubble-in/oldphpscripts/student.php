<?php

require_once "connect.php";

//check that student test ID has been entered
if (isset ($_POST["Test"]) && $_POST["Test"] != "")
{
	$TestID = $_POST["Test"];
	
	//check that student ID has been entered
	if (isset ($_POST["Id"]) && $_POST["Id"] != "")
	{
		$StudentID = $_POST["Id"];
		
		//check that student name has been entered
		if (isset ($_POST["Name"]) && $_POST["Name"] != "")
		{
			$Name = $_POST["Name"];
			$test_exists = false;
		//check that the test time is available
		$currentDateAndTime = $_POST["currentDateAndTime"];
			$mysqli = new mysqli($db_server, $db_user, $db_pass, $db_name);
			$mysqli2 = new mysqli($db_server, $db_user, $db_pass, $db_name);
			if($mysqli->connect_error)
			{
				die( "Connection Failed: " . $mysqli->connect_error );
			}
			
			$stmt = $mysqli->prepare( "SELECT test_id, questions, start_datetime, end_datetime FROM test_info WHERE test_id = ?" );
			$stmt->bind_param('s', $TestID);
			$stmt->execute();
			
			$stmt->bind_result($test, $qs, $start, $end);

			if ($stmt->fetch() == true )
                        {
                                if (($currentDateAndTime > $start) && ($currentDateAndTime < $end)) {

                                        $stmt2 = $mysqli2->prepare ( "SELECT student_id FROM student_solution WHERE test_id =? AND student_id =?");
                                        $stmt2->bind_param('ss', $TestID, $StudentID);
                                        $stmt2->execute();
                                        $stmt2->bind_result($testAlreadyTaken);
                                        while ($stmt2->fetch()) {
                                        }
                                        if ($testAlreadyTaken !== NULL) {
                                                echo "This student ID was already used on this exam.";
                                        }
                                        else {
                                                $stmt2->close();
                                                $test_exists = true;
                                                echo $test_exists.$qs;
                                                $stmt2 = $mysqli2->prepare( "INSERT INTO student_solution (test_id, student_id, name, start_time) VALUES (?, ?, ?, ?)" );
                                                $stmt2->bind_param('ssss', $TestID, $StudentID, $Name, $currentDateAndTime );
                                                $stmt2->execute();
                                                $stmt2->close();
                                        }
                                }
                                else {
                                        echo "The test you are trying to access is not available to students at this time.";
                                }
                        }else 
			{
				echo "The test ID that was entered is invalid. Please try again.";
			}
			
			$stmt->close();
			$mysqli->close();
		}
		//error catching on empty student name
		else 
		{
			echo "Please enter your name.";
		}
	}
	//error catching on empty student ID
	else 
	{
		echo "Please enter your student ID.";
	}
}

//error catching on empty username
else 
{
	echo "Please enter a test ID.";
}
?>
