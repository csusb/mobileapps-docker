<?php

  require_once ("connect.php");
  
  $user_name = $_POST["login_name"];
  
  $titles = array();
  
  $mysqli = new mysqli($db_server, $db_user, $db_pass, $db_name);
			if($mysqli->connect_error)
			{
				die( "Connection Failed: " . $mysqli->connect_error );
			}
			
			$stmt = $mysqli->prepare( "SELECT test_id, course, title FROM test_info WHERE user_name = ? ORDER BY course");
			$stmt->bind_param("s", $user_name);
			$stmt->execute();
			$stmt->bind_result($test, $course, $title);
			
			while ($stmt->fetch())
			{	
				array_push($titles, $title);
			}
			
			foreach ($titles as $v){
				echo $v.",";
			}
?>
