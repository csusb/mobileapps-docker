<?php
require_once "connect.php";

//check that testID has been entered
if (isset ($_POST["Generate"]) && $_POST["Generate"] != "")
{
	$badString = "GENERATE";
	$testID = $_POST["Generate"];
	$temp = strtoupper($testID);
	$validate = strcmp($temp, $badString);
	if ($validate != 0) {
		//check that title has been entered
		if (isset ($_POST["title"]) && $_POST["title"] != "")
		{
			$title = $_POST["title"];
		
			//check that course has been entered
			if (isset ($_POST["course"]) && $_POST["course"] != "")
			{
				$course = $_POST["course"];
				
					//check that course has been entered
					if (isset ($_POST["question"]) && $_POST["question"] >= 1 && $_POST["question"] <=250)
					{
						$question = $_POST["question"];
						$uname = $_POST["uname"];
						$startDateAndTime = $_POST["formattedStartTime"];
						$endDateAndTime = $_POST["formattedEndTime"];
						$test_success = false;
						
						$mysqli = new mysqli($db_server, $db_user, $db_pass, $db_name);
						if($mysqli->connect_error)
						{
							die( "Connection Failed: " . $mysqli->connect_error );
						}
						
						$stmt = $mysqli->prepare( "SELECT test_id FROM test_info WHERE test_id = ?" );
						$stmt->bind_param('s', $testID);
						$stmt->execute();
						$stmt->bind_result($exists);
						
						if ($stmt->fetch() == true)
						{
							echo "This test key already exists. Please generate a new one.";
						}
						else 
						{
							$stmt = $mysqli->prepare( "INSERT INTO test_info (course, test_id, title, questions, user_name, start_datetime, end_datetime)VALUES (?, ?, ?, ?, ?, ?, ?)" );
							$stmt->bind_param('sssssss', $course, $testID, $title, $question, $uname, $startDateAndTime, $endDateAndTime);
							$stmt->execute();
							
							$test_success = true;
							echo $test_success;
							
							$stmt->close();
							$mysqli->close();
						}
					}
				
				//error catching on empty number of questions
				else 
				{
					echo "Please enter a valid number of questions. Valid range is from 1-250.";
				}
			}
			
			//error catching on empty coursename
			else 
			{
				echo "Please enter a course name.";
			}
		}
	
		//error catching on empty title
		else 
		{
			echo "Please enter a title.";
		}
	}
	//error catching on unsubstantiated test ID
	else 
	{
		echo "Please generate a test ID.";
	}
}
//error catching on empty testID
else 
{
	echo "Please generate a test ID.";
}
?>
