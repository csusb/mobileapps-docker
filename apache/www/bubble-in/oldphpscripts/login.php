<?php

require_once "connect.php";

//check that username has been entered
if (isset ($_POST["login_name"]) && $_POST["login_name"] != "")
{
	$user_name = $_POST["login_name"];
	
	//check that password has been entered
	if (isset ($_POST["login_pass"]) && $_POST["login_pass"] != "")
	{
		$user_pass = $_POST["login_pass"];
	
		$mysqli = new mysqli($db_server, $db_user, $db_pass, $db_name);

        if($mysqli->connect_error)
        {
            die( "Connection Failed: " . $mysqli->connect_error );
        }
                $login_token = false;

        $mysqli2 = new mysqli($db_server, $db_user, $db_pass, $db_name);

        $stmt3 = $mysqli->prepare( "SELECT user_name, login_attempts, lastAttempt, DATE_ADD(lastAttempt, Interval 30 MINUTE) FROM user_info WHERE user_name = ?" );
        echo $mysqli->error;
        $stmt3->bind_param('s', $user_name);
        $stmt3->execute();
        $stmt3->bind_result($un, $unsuccessfulAttempts, $timeLastAttempted, $timeCanAttemptAgain);
        while ($stmt3->fetch()) {
                //get all the data on the current user trying to login
        }
        $stmt3->close();

        $stmt = $mysqli->prepare( "SELECT user_name, user_pass FROM user_info WHERE user_name = ? AND user_pass = ?" );
        echo $mysqli->error;
        $stmt->bind_param('ss', $user_name, $user_pass );
        $stmt->execute();
        $stmt->bind_result($name, $pass);

        $date = date("Y-m-d H:i:s");
        if ( ($date > $timeCanAttemptAgain ) ) {
                $resetAttempts = 0;
                $stmt2 = $mysqli2->prepare( "UPDATE user_info SET login_attempts = ? WHERE user_name = ?" );
                $stmt2->bind_param('is', $resetAttempts, $user_name);
                $stmt2->execute();
                $stmt2->close();
        }
        else {
                //do nothing, there is no need to reset the counter
        }

        if ($unsuccessfulAttempts > 3 ) {
                //$diff = $date->diff($timeLastAttempted);
                //$minutes = $diff->i;
                echo "This account is locked. You must wait 30 minutes before trying again.";
        }
		
		else if ( $stmt->fetch() == true )
        {
                $login_token = true;
                echo $login_token;
                $resetAttempts = 0;
                $stmt2 = $mysqli2->prepare( "UPDATE user_info SET login_attempts = ? WHERE user_name = ?" );
                $stmt2->bind_param('is', $resetAttempts, $user_name);
                $stmt2->execute();
                $stmt2->close();
        }

        else
        {
                echo "Invalid username or password. Please try again.";
                $stmt2 = $mysqli2->prepare( "UPDATE user_info SET login_attempts = login_attempts + 1 WHERE user_name = ?" );
                $stmt2->bind_param('s', $user_name);
                $stmt2->execute();
                $stmt2->close();
        }

        $stmt->close();
        $mysqli->close();
	}
	
	//error catching on empty password
	else 
	{
		echo "Please enter a password.";
	}
}

//error catching on empty username
else 
{
	echo "Please enter a username.";
}
?>
