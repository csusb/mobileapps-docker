<?php
require_once "connect.php";

	$username = $_POST["username"];
	$testID = $_POST["testID"];
	$teacherAnswers = $_POST["teacherAnswers"];
			
	$mysqli = new mysqli($db_server, $db_user, $db_pass, $db_name);
	if($mysqli->connect_error)
	{
		die( "Connection Failed: " . $mysqli->connect_error );
	}
	
	$stmt = $mysqli->prepare( "UPDATE test_info SET answers = ? WHERE user_name = ? AND test_id = ?" );
	$stmt->bind_param('sss', $teacherAnswers, $username, $testID);
	$stmt->execute();
	
	$stmt->close();
	$mysqli->close();
?>

