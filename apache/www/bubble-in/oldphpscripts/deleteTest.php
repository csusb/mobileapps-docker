<?php

  require_once ("connect.php");
  
  $deleteTest = $_POST["testIDToDelete"];
  
  $mysqli = new mysqli($db_server, $db_user, $db_pass, $db_name);
			if($mysqli->connect_error)
			{
				die( "Connection Failed: " . $mysqli->connect_error );
			}
			
			$stmt = $mysqli->prepare( "DELETE FROM student_solution WHERE test_id = ?");
			$stmt->bind_param("s", $deleteTest);
			$stmt->execute();
			$stmt->close();
			
			$stmt = $mysqli->prepare( "DELETE FROM test_info WHERE test_id = ?");
			$stmt->bind_param("s", $deleteTest);
			$stmt->execute();
			$stmt->close();
?>
