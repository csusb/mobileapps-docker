<?php
require_once "connect.php";

	$studentID = $_POST["studentId"];
	$testID = $_POST["studentTestId"];
	$studentAnswers = $_POST["studentAnswers"];
			
	$mysqli = new mysqli($db_server, $db_user, $db_pass, $db_name);
	if($mysqli->connect_error)
	{
		die( "Connection Failed: " . $mysqli->connect_error );
	}
	
	$stmt = $mysqli->prepare( "UPDATE student_solution SET answers = ? WHERE student_id = ? AND test_id = ?" );
	$stmt->bind_param('sss', $studentAnswers, $studentID, $testID);
	$stmt->execute();
	
	$stmt->close();
	$mysqli->close();
?>

