<?php
require_once "connect.php";

//check that username has been entered
if (isset ($_POST["user_name"]) && $_POST["user_name"] != "")
{
	$user_name = $_POST["user_name"];
	//check that password has been entered
	if (isset ($_POST["user_pass"]) && $_POST["user_pass"] != "")
	{
		$user_pass = $_POST["user_pass"];
		//check that password has been entered
		if (isset ($_POST["name"]) && $_POST["name"] != "")
		{
			$name = $_POST["name"];
			$registration_token = false;
			
			$mysqli = new mysqli($db_server, $db_user, $db_pass, $db_name);
			if($mysqli->connect_error)
			{
				die( "Connection Failed: " . $mysqli->connect_error );
			}
			
			$stmt = $mysqli->prepare( "SELECT user_name FROM user_info WHERE user_name = ?" );
			$stmt->bind_param('s', $user_name);
			$stmt->execute();
			$stmt->bind_result($exists);
			
			if ($stmt->fetch() == true)
			{
				echo "This username already exists.";
			}
			else 
			{
				$stmt = $mysqli->prepare( "INSERT INTO user_info (user_name, user_pass, name) VALUES (?, ?, ?)" );
				$stmt->bind_param('sss', $user_name, $user_pass, $name );
				$stmt->execute();
				
				$registration_token = true;
				echo $registration_token;
				
				$stmt->close();
				$mysqli->close();
			}
		}
		
		//error catching on empty name
		else 
		{
			echo "Please enter a name.";
		}
	}
	
	//error catching on empty password
	else 
	{
		echo "Please enter a password.";
	}
}

//error catching on empty username
else 
{
	echo "Please enter a username.";
}
?>

