<?php

use Illuminate\Database\Seeder;
use App\Bank;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bank::getQuery()->delete();

        $bank = new Bank();
        $bank->name = "History";
        $bank->user_id = 1;
        $bank->course_id = 1;
        $bank->save();

        $bank = new Bank();
        $bank->name = "Math";
        $bank->user_id = 1;
        $bank->course_id = 1;
        $bank->save();

        $bank = new Bank();
        $bank->name = "Physics";
        $bank->user_id = 1;
        $bank->course_id = 1;
        $bank->save();
    }
}
