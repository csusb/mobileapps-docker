<?php

use Illuminate\Database\Seeder;
use App\Submission;

class SubmissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Submission::getQuery()->delete();

        $sub = new Submission();
        $sub->stu_id = 5555555;
        $sub->test_id = 1;
        $sub->stu_name = "Michael";
        $sub->answer = "AAAAA";
        $sub->submission_number = 1;
        $sub->save();

        $sub = new Submission();
        $sub->stu_id = 2222222;
        $sub->test_id = 1;
        $sub->stu_name = "Michael";
        $sub->answer = "BBBBB";
        $sub->submission_number = 1;
        $sub->save();

        $sub = new Submission();
        $sub->stu_id = 3333333;
        $sub->test_id = 1;
        $sub->stu_name = "David";
        $sub->answer = "CCCCC";
        $sub->submission_number = 1;
        $sub->save();

        $sub = new Submission();
        $sub->stu_id = 4444444;
        $sub->test_id = 1;
        $sub->stu_name = "David";
        $sub->answer = "DDDDD";
        $sub->submission_number = 1;
        $sub->save();

        $sub = new Submission();
        $sub->stu_id = 4444444;
        $sub->test_id = 2;
        $sub->stu_name = "Geoff";
        $sub->answer = "EEEEE";
        $sub->submission_number = 1;
        $sub->save();

        $sub = new Submission();
        $sub->stu_id = 5555555;
        $sub->test_id = 2;
        $sub->stu_name = "Johns";
        $sub->answer = "ABABA";
        $sub->submission_number = 1;
        $sub->save();

        $sub = new Submission();
        $sub->stu_id = 5034455;
        $sub->test_id = 3;
        $sub->stu_name = "Sal Goodman";
        $sub->answer = "AAAAA";
        $sub->submission_number = 1;
        $sub->save();

        $sub = new Submission();
        $sub->stu_id = 1483369;
        $sub->test_id = 4;
        $sub->stu_name = "Jesse Pinkman";
        $sub->answer = "ABABA";
        $sub->submission_number = 1;
        $sub->save();

        $sub = new Submission();
        $sub->stu_id = 1178987;
        $sub->test_id = 5;
        $sub->stu_name = "Walter White";
        $sub->answer = "ABABA";
        $sub->submission_number = 1;
        $sub->save();

    }
}
