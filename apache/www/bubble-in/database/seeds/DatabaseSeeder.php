<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CourseSeeder::class);
        $this->call(BankSeeder::class);
//        $this->call(ClassroomSeeder::class);
        $this->call(TestTableSeeder::class);
        $this->call(BankTestSeeder::class);
        $this->call(BankQuestionSeeder::class);
        $this->call(BankOptionSeeder::class);
        $this->call(TestQuestionSeeder::class);
        $this->call(TestOptionSeeder::class);
        $this->call(SubmissionTableSeeder::class);

    }
}