<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::getQuery()->delete();

        $user = new User();
        $user->name = 'admin';
        $user->email = 'admin@admin.com';
        $user->password = bcrypt('admin');
        $user->api_token = str_random(60);
        $user->type = 0;  //0 = teacher 1 = student
        $user->save();

        $user = new User();
        $user->name = 'demo';
        $user->email = 'demo@admin.com';
        $user->password = bcrypt('demo');
        $user->api_token = str_random(60);
        $user->type = 1;
        $user->stu_id = 1;
        $user->save();

    }
}
