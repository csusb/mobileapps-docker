<?php

use Illuminate\Database\Seeder;
use App\BankQuestion;

class BankQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        BankQuestion::getQuery()->delete();

        $bq = new BankQuestion();
        $bq->question = "The Emancipation Proclamation";
        $bq->bank_id = 1;
        $bq->tf = false;
        $bq->save();

        $bq = new BankQuestion();
        $bq->question = "Union capture of Vicksburg was strategically important because it";
        $bq->bank_id = 1;
        $bq->tf = false;
        $bq->save();

        $bq = new BankQuestion();
        $bq->question = "Which of the following Union objectives proved to be the hardest and took the longest to accomplish?";
        $bq->bank_id = 1;
        $bq->tf = false;
        $bq->save();

        $bq = new BankQuestion();
        $bq->question = "In 1864 Gen. Sherman pursued a policy of mass destruction as he moved his army through the South because he";
        $bq->bank_id = 1;
        $bq->tf = false;
        $bq->save();

        $bq = new BankQuestion();
        $bq->question = "The battle that allowed Lincoln to issue the Emancipation Proclamation was";
        $bq->bank_id = 1;
        $bq->tf = false;
        $bq->save();
    }
}
