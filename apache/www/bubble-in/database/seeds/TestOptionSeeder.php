<?php

use Illuminate\Database\Seeder;
use App\TestOption;

class TestOptionSeeder extends Seeder
{

    private function randomGen($min, $max, $quantity) {
        $numbers = range($min, $max);
        shuffle($numbers);
        return array_slice($numbers, 0, $quantity);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TestOption::getQuery()->delete();

        $question = 0;
        $min = 1;
        $max = 5;

        for ($q = 1; $q <=20; $q++ ) {

            $bo = $this->randomGen($min, $max, 5);

            TestOption::insert([
                ['tq_id' => $q, 'bo_id' => $bo[0]],
                ['tq_id' => $q, 'bo_id' => $bo[1]],
                ['tq_id' => $q, 'bo_id' => $bo[2]],
                ['tq_id' => $q, 'bo_id' => $bo[3]],
                ['tq_id' => $q, 'bo_id' => $bo[4]]
            ]);

            $min = $min+5;
            $max = $max+5;

            if( $max >= 26) {
                $min = 1;
                $max = 5;
            }
        }

    }
}
