<?php

use Illuminate\Database\Seeder;
use App\Classroom;

class ClassroomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Classroom::getQuery()->delete();

        $class = new Classroom();
        $class->course_id = 1;
        $class->stu_id = 1;
        $class->save();

        $class = new Classroom();
        $class->course_id = 1;
        $class->stu_id = 2;
        $class->save();

        $class = new Classroom();
        $class->course_id = 1;
        $class->stu_id = 3;
        $class->save();

        $class = new Classroom();
        $class->course_id = 1;
        $class->stu_id = 4;
        $class->save();

        $class = new Classroom();
        $class->course_id = 1;
        $class->stu_id = 5;
        $class->save();

    }
}
