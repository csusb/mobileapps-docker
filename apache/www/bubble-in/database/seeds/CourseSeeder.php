<?php

use Illuminate\Database\Seeder;
use App\Course;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Course::getQuery()->delete();

        $course = new Course();
        $course->code = "AAAAAAA";
        $course->name = "Maths";
        $course->user_id = 1;
        $course->save();


    }
}
