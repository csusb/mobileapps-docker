<?php

use Illuminate\Database\Seeder;
use App\BankTest;
use App\Test;

class BankTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $test = new Test();
        $test->name = "Final";
        $test->group = 3;
        $test->code = "CCCCC";
        $test->course_id = 1;
        $test->start = new \Carbon\Carbon('March 3, 2015');
        $test->end = new \Carbon\Carbon('March 3, 2019');
        $test->save();

        $test = new Test();
        $test->name = "Final";
        $test->group = 3;
        $test->code = "DDDDD";
        $test->course_id = 1;
        $test->start = new \Carbon\Carbon('March 3, 2015');
        $test->end = new \Carbon\Carbon('March 3, 2019');
        $test->save();

        $test = new Test();
        $test->name = "Final";
        $test->group = 3;
        $test->code = "EEEEE";
        $test->course_id = 1;
        $test->start = new \Carbon\Carbon('March 3, 2015');
        $test->end = new \Carbon\Carbon('March 3, 2019');
        $test->save();

        $test = new Test();
        $test->name = "Final";
        $test->group = 3;
        $test->code = "FFFFF";
        $test->course_id = 1;
        $test->start = new \Carbon\Carbon('March 3, 2015');
        $test->end = new \Carbon\Carbon('March 3, 2019');
        $test->save();
    }
}
