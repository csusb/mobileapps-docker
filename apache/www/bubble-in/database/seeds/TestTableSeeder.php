<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Test::getQuery()->delete();

        $test = new Test();
        $test->code = "AAAAA";
        $test->course_id = 1;
        $test->name = "Marvel";
        $test->answer_key = "ABABA";
        $test->group = 1;
        $test->start = new \Carbon\Carbon('March 3, 2016');
        $test->end = new \Carbon\Carbon('March 3, 2016');
        $test->save();

        $test = new Test();
        $test->code = "BBBBB";
        $test->course_id = 1;
        $test->name = "DC";
        $test->group = 2;
        $test->answer_key = "ABABA";
        $test->start = new \Carbon\Carbon('March 3, 2016');
        $test->end = new \Carbon\Carbon('March 3, 2016');
        $test->save();
    }
}
