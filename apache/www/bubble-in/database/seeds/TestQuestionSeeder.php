<?php

use Illuminate\Database\Seeder;
use App\TestQuestion;

class TestQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TestQuestion::getQuery()->delete();

        //Test 1
        $tq = new TestQuestion();   //1
        $tq->bt_id = 3;
        $tq->bq_id = 1;
        $tq->save();

        $tq = new TestQuestion();   //2
        $tq->bt_id = 3;
        $tq->bq_id = 2;
        $tq->save();

        $tq = new TestQuestion();   //3
        $tq->bt_id = 3;
        $tq->bq_id = 3;
        $tq->save();

        $tq = new TestQuestion();   //4
        $tq->bt_id = 3;
        $tq->bq_id = 4;
        $tq->save();

        $tq = new TestQuestion();   //5
        $tq->bt_id = 3;
        $tq->bq_id = 5;
        $tq->save();

        //Test 2
        $tq = new TestQuestion();   //6
        $tq->bt_id = 4;
        $tq->bq_id = 5;
        $tq->save();

        $tq = new TestQuestion();   //7
        $tq->bt_id = 4;
        $tq->bq_id = 4;
        $tq->save();

        $tq = new TestQuestion();   //8
        $tq->bt_id = 4;
        $tq->bq_id = 3;
        $tq->save();

        $tq = new TestQuestion();   //9
        $tq->bt_id = 4;
        $tq->bq_id = 2;
        $tq->save();

        $tq = new TestQuestion();   //10
        $tq->bt_id = 4;
        $tq->bq_id = 1;
        $tq->save();

        //Test 3
        $tq = new TestQuestion();   //11
        $tq->bt_id = 5;
        $tq->bq_id = 1;
        $tq->save();

        $tq = new TestQuestion();   //12
        $tq->bt_id = 5;
        $tq->bq_id = 3;
        $tq->save();

        $tq = new TestQuestion();   //13
        $tq->bt_id = 5;
        $tq->bq_id = 2;
        $tq->save();

        $tq = new TestQuestion();   //14
        $tq->bt_id = 5;
        $tq->bq_id = 5;
        $tq->save();

        $tq = new TestQuestion();   //15
        $tq->bt_id = 5;
        $tq->bq_id = 4;
        $tq->save();

        //Test 4
        $tq = new TestQuestion();   //16
        $tq->bt_id = 6;
        $tq->bq_id = 3;
        $tq->save();

        $tq = new TestQuestion();   //17
        $tq->bt_id = 6;
        $tq->bq_id = 5;
        $tq->save();

        $tq = new TestQuestion();   //18
        $tq->bt_id = 6;
        $tq->bq_id = 1;
        $tq->save();

        $tq = new TestQuestion();   //19
        $tq->bt_id = 6;
        $tq->bq_id = 4;
        $tq->save();

        $tq = new TestQuestion();   //20
        $tq->bt_id = 6;
        $tq->bq_id = 2;
        $tq->save();
    }
}
