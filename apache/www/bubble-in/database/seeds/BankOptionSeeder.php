<?php

use Illuminate\Database\Seeder;
use App\BankOption;

class BankOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BankOption::getQuery()->delete();

        $bo = new BankOption();
        $bo->name = "freed the slaves and abolished slavery in all the states of the Union and the Confederacy";
        $bo->bq_id = 1;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "freed slaves only in areas in rebellion against the U.S. but not in areas that remained loyal";
        $bo->bq_id = 1;
        $bo->correct = true;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "was formulated by the Radical Republicans and issued by Lincoln despite his strong personal objections";
        $bo->bq_id = 1;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "convinced England and France to enter the war on behalf of the Union in order to win the crusade against slavery";
        $bo->bq_id = 1;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "was hailed in the South for its courage and directness";
        $bo->bq_id = 1;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "opened the way to Richmond";
        $bo->bq_id = 2;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "completed Union control over the Atlantic coast";
        $bo->bq_id = 2;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "gave Lincoln the victories he was waiting for to issue the Emancipation Proclamation";
        $bo->bq_id = 2;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "gave the North control over the entire Mississippi River";
        $bo->bq_id = 2;
        $bo->correct = true;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "ended the war";
        $bo->bq_id = 2;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "taking Richmond";
        $bo->bq_id = 3;
        $bo->correct = true;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "gaining control of the Mississippi River";
        $bo->bq_id = 3;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "taking New Orleans";
        $bo->bq_id = 3;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "seizing the islands off the South's Atlantic coast in order to strengthen the blockade";
        $bo->bq_id = 3;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "gaining control of the border states";
        $bo->bq_id = 3;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "was a ruthless and heartless military leader";
        $bo->bq_id = 4;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "had lost control over his troops";
        $bo->bq_id = 4;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "wanted to break the South's will and its ability to resist";
        $bo->bq_id = 4;
        $bo->correct = true;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "had been ordered to do so by Pres. Lincoln";
        $bo->bq_id = 4;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "it was the only way to get the guerrilla fighters out of hiding";
        $bo->bq_id = 4;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "Bull Run";
        $bo->bq_id = 5;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "Gettysburg";
        $bo->bq_id = 5;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "Vicksburg";
        $bo->bq_id = 5;
        $bo->correct = false;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "Antietam";
        $bo->bq_id = 5;
        $bo->correct = true;
        $bo->save();

        $bo = new BankOption();
        $bo->name = "Cold Harbor";
        $bo->bq_id = 5;
        $bo->correct = false;
        $bo->save();
    }
}
