<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('type');
            $table->integer('stu_id')->unsigned()->nullable();
            $table->integer('login_attempts')->unsigned()->default(0);
            $table->dateTime('last_attempt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('users')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('type');
                $table->dropColumn('login_attempts');
                $table->dropColumn('last_attempt');
                $table->dropColumn('stu_id');
            });
        }
    }
}
