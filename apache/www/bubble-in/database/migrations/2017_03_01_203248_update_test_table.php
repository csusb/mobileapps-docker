<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tests', function (Blueprint $table) {
            $table->integer('subs')->unsigned()->default(1);
            $table->integer('group')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->foreign('course_id')->references('id')->on('courses');
            $table->string('answer_key')->nullable()->change();
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
            $table->dropColumn('course');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('tests')) {
            Schema::table('tests', function (Blueprint $table) {
                $table->dropColumn('subs');
                $table->dropForeign(['course_id']);
                $table->dropColumn('group');
                $table->integer('user_id')->unsigned();
                $table->string('course');
            });
        }
    }
}
