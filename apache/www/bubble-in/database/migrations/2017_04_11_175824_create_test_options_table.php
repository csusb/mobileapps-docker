<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_options', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('tq_id')->unsigned();
            $table->integer('bo_id')->unsigned();
            $table->foreign('tq_id')->references('id')->on('test_questions');
            $table->foreign('bo_id')->references('id')->on('bank_options');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_options');
    }
}
