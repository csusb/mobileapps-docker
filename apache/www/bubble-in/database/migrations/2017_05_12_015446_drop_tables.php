<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Bank_tests and classrooms are not being used, use this script to drop them both.
        Schema::dropIfExists('bank_tests');
        Schema::dropIfExists('classrooms');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //The best way to undo this is to just delete this script so it never runs in the first place
        //or remake the create_table files for the two dropped tables.
    }
}
