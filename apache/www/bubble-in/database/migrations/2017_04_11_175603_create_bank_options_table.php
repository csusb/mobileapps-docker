<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_options', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('name');
            $table->integer('bq_id')->unsigned();
            $table->foreign('bq_id')->references('id')->on('bank_questions');
            $table->boolean('correct')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_options');
    }
}
