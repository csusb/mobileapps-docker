<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//uses api_token to authenticate requests
//processes api requests from mobile app.
Route::group(['middleware'=>'auth:api'], function() {

    //route for testing purposes, returns the information of the user
    //assigned to the api_token.
    Route::get('testing', function(Request $request){
        return $request->user();
    });

    //Gets user information - same as function above, except info is returned as a string
    Route::get('user', 'MobileController@getUser');

    //Deletes test from database
    Route::delete('test', 'MobileController@delete');

    //Adds test to the database
    Route::post('test', 'MobileController@create');

    //Returns answer sheet for a given test
    Route::get('test', 'MobileController@answersheet');

    //Returns students scores for a given test
    Route::get('scores', 'MobileController@scores');

    //Returns the students that took a specific test
    Route::get('students', 'MobileController@students');

    //Allows teacher to change the course code
    Route::post('resetCourseID', 'MobileController@resetCourseID');

    //Get the list of students registered for a given course
    Route::get('courseList', 'MobileController@getCourseList');

    //Create a new course for student registration
    Route::post('newCourse', 'MobileController@createCourse');

    //This returns the teachers dashboard
    Route::get('dashboard', 'MobileController@getDash');

    //Delete a course
    Route::delete('course', 'MobileController@deleteCourse');

    //Delete a student
    Route::delete('student', 'MobileController@deleteStudent');

    //returns a submission and test answer key
    Route::get('submission', 'MobileController@getSubmission');

    //returns a list of scores and student names for a given test
    Route::get('grades', 'MobileController@getGrades');

    //registers a student with a class
    Route::post('registerClass', 'MobileController@registerForClass');

    //removes all students registered for a given course
    Route::delete('class', 'MobileController@deleteClass');

    //resets generates a new course code
    Route::get('courseCode', 'MobileController@newCourseCode');

    //Handles 'login' requests for the student


    //returns data to populate the students dashboard in the mobile app.
    Route::get('stuDash', 'MobileController@studentDash');

    //returns a string containing all tests created for a specified course
    Route::get('course', 'MobileController@getCourse');

    //Updates the answersheet for a given test
    Route::put('test', 'MobileController@updateTest');



    //update the title of a course
    Route::put('courseTitle', 'MobileController@updateCourseTitle');

    //update the information for the logged in user
    Route::put('updateUser', 'MobileController@updateUser');

    //returns all tests a student has taken for a given course, and returns the test name and grades
    Route::get('tests', 'MobileController@getTests');



    //Resets a class - Deletes all students from class, and resets all test codes (tests aren't deleted)
    Route::post('resetClass', 'MobileController@resetClass');

});



//these routes do not use the verify token
//-----------------------------------------

//Generates a random string of length 5 to be used as a test code
Route::get('testCode', 'MobileController@testCode');

//takes in a test ID and student information and returns the number
//of questions on success
Route::post('start', 'SubmissionController@start');

//Handles test submissions from the student
Route::post('submit', 'SubmissionController@submit');

//Returns the title of a given test
Route::get('title', 'SubmissionController@title');

//returns the time Boundary for a test
Route::get('time', 'SubmissionController@time');

//Handles login requests - sets a token.
Route::post('login', 'MobileController@login');

//Creates a new user
Route::post('register', 'MobileController@register');

