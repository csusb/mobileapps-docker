<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Auth::routes();


// These Routes are used for the admin system
// for api routing from mobile app see the api.php route file.

//==========Courses==============
Route::get('/', 'CourseController@index');
Route::get('/home', 'CourseController@index');
Route::get('/students/{id}', 'CourseController@getStudents');

Route::resource('course','CourseController');


Route::group(['prefix' => 'course/{course}/'], function () {

    //===========Banks===============
    Route::post('/banks/{bank}/addQuestion', 'BankController@addQuestion');
    Route::post('/banks/{bank}/bulkAddQuestion', 'BankController@BulkAddQuestions');
    Route::get('/banks/{bank}/stats', 'BankController@stats');

    Route::resource('banks','BankController');

    //===========Tests===============
    Route::post('tests/{group}/printTests', 'TestController@printTests');
    Route::get('/tests/{test}/stats', 'TestController@stats');

    Route::resource('tests','TestController');

    //===========Submissions=========
    Route::group(['prefix' => 'tests/{test}/'], function () {


        Route::resource('submissions', 'TestSubmissionController');
    });
});



Route::get('/scores/{id}/print', [
    'uses' => 'HomeController@printall',
    'middleware' => 'valid'
]);

Route::get('/answers/{id}/{stu_id}/print', [
    'uses' => 'HomeController@printsheet',
    'middleware' => 'valid'
]);

Route::get('/pdf/view', function() {
    $html = view('home')->render();

    return PDF::load($html)->show();
});
Route::get('/scores/{id}/export', [
    'uses' => 'HomeController@export',
    'middleware' => 'valid'
]);


Route::get('/testing', 'HomeController@TESTINGSTUFF');
