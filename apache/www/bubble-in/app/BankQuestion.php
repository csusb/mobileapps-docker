<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TestQuestion;
use Illuminate\Database\Eloquent\SoftDeletes;
use Log;

class BankQuestion extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['question', 'bank_id', 'tf'];

    public function bank() {
        return $this->belongsTo('App\Bank');
    }

    public function options() {
        return $this->hasMany('App\BankOption', 'bq_id', 'id');
    }

    /**
     *
     */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function($question) {
            //delete all bank_options associated with this one.
            if($question->isForceDeleting()) {
                $question->options()->get()->each(function($option) {
                    $option->forceDelete();
                });
            } else {
                $question->options()->get()->each(function($option) {
                    $option->delete();
                });
            }
        });
    }

}
