<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = ['code','user','name','course','answer_key','start','end'];

    public function submissions() {
        return $this->hasMany('App\Submission');
    }

    public function course() {
        return $this->belongsTo('App\Course');
    }

    public function questions() {
        return $this->hasMany('App\TestQuestion', 'bt_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($test) {

            //deleting a test deletes all the questions for that test.
            $test->questions()->get()->each(function($question) {
                $question->delete();
            });

            //deleting a test deletes all submissions for that test
            $test->submissions()->delete();
        });
    }
}
