<?php

namespace App\Http\Middleware;

use App\Course;
use App\Submission;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse as Response;
use App\Test;
use Auth;
use Log;
use Closure;

class ValidateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {



        if(isset($request->submission)) {
            Log::info('validate submission');

            try {
                $course = Course::findorfail($request->course)->first();
                $test = $course->tests()->where('id',$request->test)->first();
                $sub = Submission::findorfail($request->submission);

                if(Auth::id() == $course->user_id && isset($test) && $test->id == $sub->test_id) {
                    return $next($request);
                } else {
                    return redirect('/');
                }

            } catch (ModelNotFoundException $e) {
                return redirect('/');
            }
        }

        if(isset($request->bank)) {
            Log::info('validate bank');
            try {
                $course = Course::findorfail($request->course)->first();
                $banks = $course->banks()->where('id',$request->bank)->first();
                Log::info($course);
                Log::info($banks);

                if(Auth::id() == $course->user_id && !is_null($banks)) {
                    return $next($request);
                } else {
                    return redirect('/');
                }

            } catch (ModelNotFoundException $e) {
                return redirect('/');
            }
        }

        if(isset($request->test)) {
            Log::info('verifying test');
            Log::info($request->course);
            Log::info($request->test);
            try {
                $course = Course::findorfail($request->course)->first();
                $tests = $course->tests()->where('id',$request->test)->first();

                if(Auth::id() == $course->user_id && !is_null($tests)) {
                    return $next($request);
                } else {
                    return redirect('/');
                }

            } catch (ModelNotFoundException $e) {
                return redirect('/');
            }
        }

        try {
            $course = Course::findorfail($request->course)->first();
            Log::info('validate course');

            if(Auth::id() == $course->user_id) {
                return $next($request);
            } else {
                return redirect('/');
            }

        } catch (ModelNotFoundException $e) {
            return redirect('/');
        }

    }
}
