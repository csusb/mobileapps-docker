<?php

namespace App\Http\Controllers;

use App\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use Response;

use App\Http\Requests;
use App\Submission;
use Illuminate\Support\Facades\Auth;


//submission controller is used by students submitting a test
//through the mobile app - this does not have any authentication checks
//anyone can submit a test, as long as they meet the following requirements
//The time frame for the test is open, they enter the correct test code
//and they use a student ID that's not already used for this test.

class SubmissionController extends Controller
{

    public $restful = true;

    /**
     * Returns the title of a given test
     *
     * @param request->id
     * @return string
     */
    public function title(Request $request)
    {
        $code = $request->code;

        try{
            $test = Test::where("code", $code)->first();
            return $test->name;

        } catch (Exception $e) {
            return "error";
        }
    }

    /**
     * Verifies that a student can access a test.
     * If so returns the number of questions for that test
     *
     * @param  $request->code
     * @param   $request->id
     * @param $request->name
     *
     * @return string
     */
    public function start(Request $request)
    {
        $data = $request->json()->all();

        $testCode = $data['TestCode'];
        $st_name = $data['StudentName'];
        $st_id = $data['StudentID'];

        $test = Test::where('code', $testCode)->first();

        //check if test exists
        if (!isset($test))
        {
            $res = "The test ID that was entered is invalid. Please try again.";
            return Response::json(['Reason' => $res],403);
        }

        //check that the test window is open
        $date = date("Y-m-d");
        $time = date("h:i:sa");
        $datetime = new Carbon($date." ".$time);
        $end = new Carbon($test->end);

        $TimeRemaining= $datetime->diffInSeconds($end);

        if ($test->answer_key == null) {
            $numQ = $test->questions()->count();
        } else {
            $numQ = strlen($test->answer_key);
        }
        $answer_string = "";
        for($i = 0 ; $i < $numQ; $i++)
        {
            $answer_string = $answer_string.'0';
        }

        if ( !($datetime > $test->start && $datetime < $test->end) )
        {
            $res = "The test you are trying to access is not available to students at this time.";
            return Response::json(['Reason' => $res],403);
        }

       $submission = Submission::where(
           ['test_id' => $test->id],
           ['stu_id' => $st_id]
        )->first();

        $subtestCode = Submission::where(['test_id' => $test->id])->get();
        $subStuId = Submission::where(['stu_id' => $st_id])->get();
        $vtest = Test::where('code', $testCode)->first();
        // logs to see what is happening
        Log::info("===============submission============");
        Log::info($submission);
        Log::info($testCode);
        Log::info($st_id);
        Log::info("===============Broken up submissions============");
        Log::info($subtestCode);
        Log::info($subStuId);
        Log::info($vtest);
        // delete above
        if ($submission != null) {
            if ($submission->submission_number >= $test->subs)
            {
                $res = "You have already taken this exam the maximum number of times";
                return Response::json(['Reason' => $res],403);
            }

            $subID = $submission->id;
        } else {

            //create a new submission entry for this user
            $sub = new Submission();
            $sub->stu_id = $st_id;
            $sub->stu_name = $st_name;
            $sub->test_id = $test->id;
            $sub->answer = $answer_string;
            $sub->submission_number = 0;
            $sub->save();

            $subID = $sub->id;
        }


        //return the number of questions for the test
        //also need to return sub->id
        // and test->name
        //Formats a string into a json object

        return Response::json([
            'TestName'=> $test->name,
            'NumberOfQuestions' => $numQ,
            'TestTime'=> $TimeRemaining,
            'SubmissionID'=>$subID,
            'TestCode'=>$testCode,
        ],200);

    }

    /**
     * Updates the entry for the student submission
     * to include the answers submitted from the app
     *
     * Double NOTE: May need to add a check to verify the test is still open - so students cant submit tests after the test is closed
     * Triple NOTE: App will auto submit test when time is up, may want app to send submission time, or add 10-30m to the end time to ensure
     * tests are processed when sent at the last minute.
     * Quadruple NOTE: If the app is using a broadcast receiver it may need to send a submission time for when it was saved for submission
     * (this may open up security holes).
     *
     * @param Request $request->code   Test code
     * @param Request $request->answers  String containing students answers
     *
     * @return null
     *
     */
    public function submit(Request $request) {

        $data = $request->json()->all();
        Log::info("=================Submit TEST==============");
        Log::info($request);


        $testCode = $data['TestCode'];
        $subID = $data['SubmissionID'];
        $studentAnswers = $data['StudentAnswers'];

        //get student submission then update the answers with the new submitted answers
        $test = Test::where('code', $testCode)->first();
        $submission = Submission::where(
            ['test_id' => $test->id],
            ['id' => $subID]
        )->first();

        //update the answers and the submission number.
        $submission->answer = $studentAnswers;
        $submission->submission_number = $submission->submission_number + 1;
        $submission->save();

        Log::info("=================Submission With answer change==============");
        Log::info($submission);


        return $submission;

    }

    /**
     * Returns the endtime for the test
     * A clock is created within the app, auto submitting the answers when time runs out.
     *
     * NOTE: This should probably be changed to calculate the difference between current time, and end time
     * and then return a value in seconds that can be returned to the app for creating the clock.
     *
     * @param Request $request->code    Test Code
     *
     * @return string;
     */
    public function time(Request $request) {

        //get the test from submitted code
        $test = Test::where('code', $request->code)->first();

        //return end time
        return $test->end;
    }


}
