<?php

namespace App\Http\Controllers;

use App\BankOption;
use App\BankQuestion;
use App\Course;
use App\TestOption;
use App\TestQuestion;
use Barryvdh\DomPDF\PDF;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Test;
use App\User;
use App\Submission;
use League\Csv\Writer;
use App\Bank;
use Log;
use Illuminate\Support\Facades\File;
use View;
use App;

class HomeController extends Controller
{

    const keyEnum = [
        'A' => 0,
        'B' => 1,
        'C' => 2,
        'D' => 3,
        'E' => 4
    ];
    private $pdf;

    /**
     * The function mmmr (Mean, Median, Mode, Range) will calculate the Mean,
     * Median, Mode, or Range of an array. It automatically defaults to Mean (average).
     *
     * @param $array
     * @param string $output
     * @return bool|int|mixed|number|string
     */
    public static function mmmr($array, $output = 'mean')
    {
        if (!is_array($array)) {
            return 0;
        } else {
            switch ($output) {
                case 'mean':
                    $count = count($array);
                    $sum = array_sum($array);
                    $total = $sum / $count;
                    break;
                case 'median':
                    rsort($array);
                    $middle = round(count($array), 2);
                    $total = $array[$middle - 1];
                    break;
                case 'mode':
                    $v = array_count_values($array);
                    arsort($v);
                    foreach ($v as $k => $v) {
                        $total = $k;
                        break;
                    }
                    break;
                case 'range':
                    sort($array);
                    $sml = $array[0];
                    rsort($array);
                    $lrg = $array[0];
                    $total = $lrg - $sml;
                    break;
            }
            return $total;
        }
    }

    /**
     * Generates a random code of length 7
     * This is used as the test code
     *
     * @param int $length
     * @return string
     */
    public static function randomString($length = 7)
    {
        $str = "";
        $characters = array_merge(range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    //takes a test and returns an array
    public static function scores($test)
    {

        //get answer key for the test
        $key = $test->answer_key;
        $keyArray = str_split($key);

        //calculate the scores for each student.
        $scores = [];
        $submissions = $test->submissions()->get();

        foreach ($submissions as $submission) {
            $answer = $submission->answer;
            $ansArray = str_split($answer);
            $points = 0;

            foreach ($ansArray as $i => $ans) {
                if ($ans == $keyArray[$i]) {
                    $points += 1;
                }
            }

            array_push($scores, $points);
        }

        return $scores;
    }

    /**
     * Takes in a no bank test, and calculates how many times a
     * specific option was selected for each question
     **/
    public static function NoBankStats($test)
    {
        $stats = [];

        $questions = strlen($test->answer_key);
        for ($i = 0; $i < $questions; $i++) {
            $options = [
                'A' => 0,
                'B' => 0,
                'C' => 0,
                'D' => 0,
                'E' => 0
            ];

            array_push($stats, $options);
        }

        $submissions = $test->submissions()->get();
        foreach ($submissions as $submission) {
            $answers = str_split($submission->answer);
            foreach ($answers as $i => $answer) {
                $stats[$i][$answer] = $stats[$i][$answer] + 1;
            }
        }

        return $stats;

    }

    /**
     * Takes in a test group and returns all questions used for all tests
     * in that group. How many times each question was used, and how many times
     * a specific option was selected.
     */
    public static function BankTestStats($group)
    {
        $tests = Test::where('group', $group)->get();
        $count = Test::where('group', $group)->count();
        $questions = collect([]);
        $options = collect([]);

        foreach ($tests as $test) {

            $answers = [];
            $qs = $test->questions()->get();
            $submissions = $test->submissions()->get();

            foreach ($submissions as $submission) {
                array_push($answers, str_split($submission->answer));
            }

            foreach ($qs as $i => $q) {
                $bq = $q->bankQuestion()->with('options')->first();
                $tos = $q->options()->get();

                foreach ($answers as $answer) {
                    if ($answer[$i] != '0') {
                        $bo_id = $tos[HomeController::keyEnum[$answer[$i]]]->bo_id;
                        $options = $options->push(collect(['bo' => $bo_id]));
                    }
                }

                $questions = $questions->push($bq);
            }
        }

        foreach ($questions as $i => $question) {
            $count = $questions->where('id', $question->id)->count();
            $question = collect($question);
            $questions[$i] = $question->merge(['count' => $count]);

        }

        foreach ($options as $i => $option) {
            $count = $options->where('bo', $option['bo'])->count();
            $options[$i] = $option->merge(['count' => $count]);
        }

        $questions = $questions->unique('id');
        $options = $options->unique('bo');

        $results = [
            'questions' => $questions,
            'options' => $options
        ];

        return $results;

    }

    /**
     * Takes in a Bank id and calculates how many times a question was used
     * and how many times a specific option was selected.
     */
    public static function BankStats($id)
    {

    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    //returns a list of all students registered for a given course
    public function students($id)
    {
        $students = Auth::user()->courses()->where("id", $id)->first()->classroom()->get();
        return view('students', ['students' => $students]);
    }

    //retrieves all submissions for a specified test
    //and returns to user as a csv file for download.
    public function export(Request $request, $id)
    {

        $submissions = Submission::all();

        //create a csv file in memory.
        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        //get the test
        $test = Test::find($id);
        //get answer key for the test
        $key = $test->answer_key;
        $keyArray = str_split($key);

        //calculate the scores for each student.
        $scores = [];
        $submissions = $test->submissions()->get();

        foreach ($submissions as $submission) {
            $answer = $submission->answer;
            $ansArray = str_split($answer);
            $points = 0;

            foreach ($ansArray as $i => $ans) {
                if ($ans == $keyArray[$i]) {
                    $points += 1;
                }
            }

            array_push($scores, $points);
        }

        //insert headers
        $csv->insertOne(['id', 'name', 'score']);

        //insert row for each submission
        foreach ($submissions as $i => $submission) {
            $csv->insertOne([$submission->stu_id, $submission->stu_name, $scores[$i]]);
        }

        //return csv file.
        $csv->output('submissions.csv');
    }

    public function TESTINGSTUFF(Request $request)
    {
        $pdf = App::make('dompdf.wrapper');
        $view = view('testing')->render();
        $pdf->loadHTML($view);
        $pdf->stream();

        return $pdf->download('testing.pdf');
    }

}
