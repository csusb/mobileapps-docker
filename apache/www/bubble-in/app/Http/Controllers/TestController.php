<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\TestQuestion;
use App\TestOption;
use App\Bank;
use App\BankQuestion;
use App\BankOption;
use App\Http\Controllers\HomeController;

use Auth;
use Illuminate\Support\Facades\Redirect;
use Log;
use App;

class TestController extends Controller
{

    //include authentication middleware
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('valid', ['except' => ['destroy', 'printTests']]);

    }

    public function index($id)
    {
        $tests = Auth::user()->courses()->where("id", $id)->first()->tests()->groupBy('group')->get();
        return view('tests', ['tests' => $tests, 'course' => $id]);
    }

    public function show(Request $request, $course, $id)
    {

        $test = [];
        $qs = Test::find($id)->questions()->with('options')->get();

        foreach ($qs as $i => $q) {
            $options = [];
            foreach ($q->options as $option) {
                $bo = BankOption::withTrashed()->where('id', $option->bo_id)->first();
                array_push($options, $bo);
            }

            $bq = BankQuestion::withTrashed()->where('id', $q->bq_id)->first();

            $question = collect([
                'question' => $bq,
                'options' => $options
            ]);

            array_push($test, $question);
        }

        log::info($test);
        return view('review_test', ['questions' => $test]);
    }

    public function create($id)
    {
        $banks = Bank::where('course_id', $id)->pluck('name', 'id');
        Log::info($banks);

        return view('create', ['banks' => $banks, 'course' => $id]);
    }

    public function store(Request $request)
    {
        Log::info('store new test');
        Log::info($request);

        if ($request->banks != null) {
            //if the user wants to create a test from a test bank, find out how many questions they want
            //it cannot exceed the number of questions in the bank itself.
            $bqs = Bank::find($request->banks)->questions()->count();
            if ($bqs < $request->questions) {
                return Redirect::back()->withErrors(['Cannot create ' . $request->questions . ' questions. Bank contains ' . $bqs . ' questions'])->with('bank', [$request->banks]);
            }

            $questions = Bank::find($request->banks)->questions()->with('options')->inRandomOrder()->take($request->questions)->get();
            Log::info("====QUESTIONS====");
            Log::info($questions);
        }
        $gid = Test::all()->max('group');
        $gid++;


        for ($i = 0; $i < $request->tests; $i++) {
            $code = HomeController::randomString();
            $test = new Test();
            $test->code = $code;
            $test->name = $request->name;
            $test->group = $gid;
            $test->course_id = $request->cid;
            $test->start = new \Carbon\Carbon($request->sdate . ' ' . $request->stime);
            $test->end = new \Carbon\Carbon($request->edate . ' ' . $request->etime);
            $test->save();

            //if the professor did not select a test bank to use, allow them to generate an answer key
            if ($request->banks == null) {
                $key = "";
                for ($i = 0; $i < $request->questions; $i++) {
                    $key = $key . "0";
                }

                $test->answer_key = $key;
                $test->save();
                Log::info('edit new test');
                Log::info($test->course_id);
                Log::info($test->id);
                return redirect('/course/' . $test->course_id . '/tests/' . $test->id . '/edit');
            }

            //otherwise generate the test from the bank
            Log::info("======Generating Test===========");
            foreach ($questions as $question) {
                Log::info($test->id);
                $q = new TestQuestion();
                $q->bt_id = $test->id;
                $q->bq_id = $question->id;
                $q->save();

                $options = $question->options;
                $options = $options->shuffle();
                foreach ($options as $option) {
                    $o = new TestOption();
                    $o->tq_id = $q->id;
                    $o->bo_id = $option->id;
                    $o->save();
                }
            }

            //get a new set of questions for the next test
            if ($request->sameQs != 'yes') {
                $questions = Bank::find($request->banks)->questions()->with('options')->inRandomOrder()->take($request->questions)->get();
            } else {
                $questions = $questions->shuffle();
            }

            //if all students get the same test, only generate one
            if ($request->sameT == 'yes') {
                $i = $request->tests;
            }

        }

        $test = Test::where('group', $gid)->first();
        $bank = Bank::find($request->banks)->first()->name;

        return view('print_test', ['test' => $test, 'bank' => $bank, 'number' => $request->tests]);
    }

    public function edit($course, $id)
    {
        $test = Test::find($id);
        $course = $test->course()->first()->name;
        $bank = null;
        $questions = null;
        $num = null;
        Log::info($test);

        if ($test->answer_key == null) {
            $bank = $test->questions()->first()->bankQuestion()->first()->bank()->first()->name;
            $questions = $test->questions()->count();
            $num = Test::where('group', $test->group)->count();
        }


        return view('edit_test', ['test' => $test, 'course' => $course, 'bank' => $bank, 'questions' => $questions, 'tests' => $num]);

    }

    public function update(Request $request, $course, $id)
    {
        $i = 0;
        $key = null;

        while ($request->$i !== null) {
            $key = $key . $request->$i;
            $i++;
        }

        $test = Test::find($id);
        $test->answer_key = $key;

        //creating a test, and editing a test both use this function
        //however, the date and time only gets passed when editing, not creating
        //so check if the date and time were passed, if so, set them and update
        //them in the database
        if (isset($request->sdate)) {
            if ($test->answer_key == null) {
                $ts = Test::where('group', $test->group)->get();
                foreach ($ts as $t) {
                    $t->start = new \Carbon\Carbon($request->sdate . ' ' . $request->stime);
                    $t->end = new \Carbon\Carbon($request->edate . ' ' . $request->etime);
                    $t->save();
                }
            } else {
                $test->start = new \Carbon\Carbon($request->sdate . ' ' . $request->stime);
                $test->end = new \Carbon\Carbon($request->edate . ' ' . $request->etime);
            }
        }

        $test->save();

        return redirect('course/' . $test->course_id . '/tests');
    }

    public function destroy($course, $id)
    {
        Test::where('group', $id)->get()->each(function ($test) {
            $test->delete();
        });

        return back();
    }

    public function printTests(Request $request, $course, $group)
    {
        Log::info('print the test!');
        Log::info($group);

        $tests = Test::where('group', $group)->get();

        $return = [];

        foreach ($tests as $test) {

            $qs = Test::find($test->id)->questions()->with('options')->get();

            $questions = [];
            foreach ($qs as $i => $q) {
                $options = [];
                foreach ($q->options as $option) {
                    $bo = BankOption::withTrashed()->where('id', $option->bo_id)->first();
                    array_push($options, $bo);
                }

                $bq = BankQuestion::withTrashed()->where('id', $q->bq_id)->first();

                $question = collect([
                    'question' => $bq,
                    'options' => $options,
                ]);

                array_push($questions, $question);
            }

            $testObject = collect([
                'test' => $test,
                'qs' => $questions
            ]);

            array_push($return, $testObject);
        }

        $tests = collect($return);

        log::info($tests);

        $pdf = App::make('dompdf.wrapper');
        $view = view('pdf.tests', ['tests' => $tests])->render();
        $pdf->loadHTML($view);
        return $pdf->stream();

    }

    //returns statistics on a selected test
    public function stats(Request $request, $course, $id)
    {
        //get the test
        $test = Test::find($id);

        //if test answerkey is null then this is a test generated from a testbank
        if ($test->answer_key == null) {
            $scores = [];
            $tests = Test::where('group', $test->group)->with('submissions')->get();

            foreach ($tests as $i => $test) {
                $points = 0;
                foreach ($test->submissions as $j => $submission) {
                    if ($submission != null) {
                        $answer = $submission->answer;
                        $answerArray = str_split($answer);
                        $questions = $test->questions()->with('options')->get();
                        foreach ($questions as $k => $question) {
                            if ($answerArray[$k] != '0') {
                                $option = $question->options[HomeController::keyEnum[$answerArray[$k]]]->bo_id;
                                $bo = BankOption::withTrashed()->where('id', $option)->first();
                                if ($bo->correct) {
                                    $points += 1;
                                    Log::info($points);
                                }
                            }
                        }
                    }
                    array_push($scores, $points);
                }
            }
            $stats = HomeController::BankTestStats($test->group);
            $bank = $test->questions()->first()->bankQuestion()->first()->bank()->first()->id;

        } else {
            //call the scores function to calculate the score for each student
            $scores = HomeController::scores($test);

            $stats = HomeController::NoBankStats($test);
            $bank = null;
        }


        $mean = 0;
        $median = 0;
        $mode = 0;
        $range = 0;
        $max = 0;
        $min = 0;

        if (count($scores) > 0) {
            $mean = HomeController::mmmr($scores, 'mean');
            $median = HomeController::mmmr($scores, 'median');
            $mode = HomeController::mmmr($scores, 'mode');
            $range = HomeController::mmmr($scores, 'range');
            $max = max($scores);
            $min = min($scores);
        }

        return view('stats', ['bank' => $bank, 'test' => $test, 'stats' => $stats, 'mean' => $mean, 'median' => $median, 'mode' => $mode, 'range' => $range, 'highest' => $max, 'lowest' => $min, 'scores' => $scores]);
    }


}
