<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\TestQuestion;
use App\TestOption;
use App\Bank;
use App\BankQuestion;
use App\BankOption;
use App\Course;

use Auth;
use Log;

class TestSubmissionController extends Controller
{
    //include authentication middleware
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('valid');
    }

    public function index($course, $id)
    {
        //get the test
        $test = Test::find($id);
        //get answer key for the test
        $key = $test->answer_key;
        $scores = [];

        if ($key == null) {
            $tests = Test::where('group', $test->group)->with('submissions')->get();

            foreach ($tests as $i => $test) {
                $points = 0;
                foreach ($test->submissions as $j => $submission) {
                    if ($submission != null && $submission->answer != '0') {
                        $answer = $submission->answer;
                        $answerArray = str_split($answer);
                        $questions = $test->questions()->with('options')->get();
                        foreach ($questions as $k => $question) {

                            $option = $question->options[HomeController::keyEnum[$answerArray[$k]]]->bo_id;
                            $bo = BankOption::withTrashed()->where('id', $option)->first();

                            if ($bo->correct) {
                                $points += 1;
                            }
                        }
                    }
                    array_push($scores, $points);
                }
            }

            $questions = Test::where('group', $test->group)->first()->questions()->count();
            return view('submissions', ['tests' => $tests, 'scores' => $scores, 'test' => $test, 'questions' => $questions]);
        }

        $keyArray = str_split($key);

        //calculate the scores for each student.
        $submissions = $test->submissions()->get();

        foreach ($submissions as $submission) {
            $answer = $submission->answer;
            $ansArray = str_split($answer);
            $points = 0;

            foreach ($ansArray as $i => $ans) {
                if ($ans == $keyArray[$i]) {
                    $points += 1;
                }
            }

            array_push($scores, $points);
        }


        return view('scores', ['submissions' => $submissions, 'scores' => $scores, 'test' => $test]);
    }

    public function show($course, $test_id, $submission)
    {
        $test = Test::find($test_id);

        //if answer key is null we have a test generated from a test bank
        if ($test->answer_key == null) {
            $tests = [];

            $qs = Test::find($test_id)->questions()->with('options')->get();
            $sub = Test::find($test_id)->submissions()->first();
            $as = [];
            if ($sub != null) {
                $key = $sub->answer;
                $as = str_split($key);
            }

            Log::info("==============as============");
            Log::info($as);

            foreach ($qs as $i => $q) {
                $options = [];
                foreach ($q->options as $option) {
                    $bo = BankOption::withTrashed()->where('id', $option->bo_id)->first();
                    array_push($options, $bo);
                }

                $answer = null;
                if ($sub != null && isset($as[$i]) && $as[$i] != '0') {
                    $answer = HomeController::keyEnum[$as[$i]];
                }

                $bq = BankQuestion::withTrashed()->where('id', $q->bq_id)->first();

                $question = collect([
                    'question' => $bq,
                    'options' => $options,
                    'answer' => $answer
                ]);

                array_push($tests, $question);
            }

            $tests = collect($tests);

            log::info($tests);
            return view('bank_answers', ['vals' => $tests]);
        } else {

            $submission = $test->submissions()->where('id', $submission)->first();
            $course = Course::find($test->course_id)->first()->name;

            //calculate the score for this student
            $points = 0;
            $keys = str_split($test->answer_key);
            $answers = str_split($submission->answer);

            foreach ($keys as $i => $key) {
                if ($key == $answers[$i]) {
                    $points += 1;
                }
            }
            return view('answersheet', ['student' => $submission, 'test' => $test, 'score' => $points, 'course' => $course]);
        }
    }
}
