<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Auth\ThrottlesLogins;

use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Course;
use App\Classroom;
use App\Submission;


//All transactions require the user
//to be logged in, which supplies them with a token, all exchanges
//require the token for authentication.

class MobileController extends Controller
{

    use ThrottlesLogins;

    //generates a random string of numbers and capital letters.
    private function randomString($length = 5) {
        $str = "";
        $characters = array_merge(range('A','Z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    //Takes two strings, returns the grade as a string in the format correct/total
    private function calcGrade($key, $submission) {
        Log::info("=============calculating grade=================");
        Log::info($key);
        Log::info($submission);

        $grade = 0;
        $total = strlen($key);
        for( $i = 0; $i < $total; $i++ ) {
            $char1 = substr($key, $i, 1);
            $char2 = substr($submission, $i, 1);

            if ($char1 == $char2) {
                $grade++;
            }
        }

        return $grade."/".$total;
    }

    //same as above, put just returns score
    private function calcScore($key, $submission) {

        $grade = 0;
        $total = strlen($key);
        for( $i = 0; $i < $total; $i++ ) {
            $char1 = substr($key, $i, 1);
            $char2 = substr($submission, $i, 1);

            if ($char1 == $char2) {
                $grade++;
            }
        }

        Log::info("=======calcScore==========");
        Log::info($key);
        Log::info($submission);
        Log::info($grade);
        return $grade;
    }

    //Returns a 7 string code that is unique for courses (i.e. no courses have this code)
    private function getCourseCode() {

        $unique = false;
        $code = "";
        while(!($unique)){

            $code = $this->randomString(7);
            $temp = Course::where('code',$code)->first();
            if($temp == null) {
                $unique = true;
            }
        }

        return $code;
    }

    //Returns a 5 string code that is unique for tests (i.e. no tests have this code)
    private function getTestCode() {

        $unique = false;
        $code = "";
        while(!($unique)){

            $code = $this->randomString(5);
            $temp = Test::where('code',$code)->first();
            if($temp == null) {
                $unique = true;
            }
        }

        return $code;
    }

    /**
     * Takes username and password if valid combo returns api_token
     *
     * @param Request $request->email  email
     * @param Request $request->pass  password
     *
     * @return string   api_token
     */
    public function login(Request $request) {

        Log::info($request);

        $this->validate($request, [
           'email' => 'required',
            'pass' => 'required'
        ]);

        $user = User::where('email',$request->email)->first();
        if($user == null) {
            return 'Invalid username or password. Please try again.';
        }

        //if a user was locked out, the must wait 30 minutes before we let them try again
        $now = Carbon::now();
        $last = Carbon::parse($user->last_attempt);
        $diff = $now->diffInMinutes($last);
        if ( $user->last_attempt != null && $diff < 30 ) {
            $diff = 30-$diff;
            return "too many attempts, please try again in ".$diff." minutes";
        }

        //user gets three login attempts before we lock them out.
        $attempts = $user->login_attempts;
        if($attempts > 2) {
            $user->login_attempts = 0;
            $user->last_attempt = $now;
            $user->save();
            return "Too many login attempts, please try again later";
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->pass])) {

            $user = Auth::user();
            $user->api_token = str_random(60);
            $user->login_attempts = 0;
            $user->save();
            return $user->type.$user->api_token;
        }

        $user->login_attempts = $attempts + 1;
        $user->save();

        return 'Invalid username or password. Please try again.';
    }

    /**
     * Takes username, email and password, if email is not used, returns new user information
     *
     * @param Request $request->name
     * @param Request $request->email
     * @param Request $request->pass
     *
     * @return string
     */
    public function register(Request $request) {

        Log::info("==============Register User==============");
        Log::info($request);

        $this->validate($request, [
           'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'type' => 'required',
            'id' => 'bail|required_if:type,0|unique:users,stu_id'
        ]);

        Log::info("valid");

        $stu_id = null;
        if (isset($request->id)) {
            $stu_id = $request->id;
        }
        //convert request data into an array
        $data = ['name' => $request->name, 'email' => $request->email, 'password' => $request->pass, 'type' => $request->type, 'id' => $stu_id];

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'type' => $data['type'],
            'stu_id' => $data['id']
        ]);

        return "1";
    }

    /**
     * Takes a test code, if that test belongs to the authorized user
     * test is deleted from db. Note: Deleting a test does not delete its submissions.
     *
     * @param Request $request->code
     *
     * @return string
     */
    public function delete(Request $request) {

        Log::info("===========Delete Test==============");
        Log::info($request);
        //get the authorized user
        $user = Auth::user();

        //get the test that was requested for deletiong
        $test = Test::where('code', $request->code)->first();

        //verify the test exists
        if (!isset($test)) {
            return "test no longer exists";
        }

        Log::info($user->id);
        Log::info($test->course()->first()->user_id);
        //if this test belongs to this user, delete it
        if($user->id == $test->course()->first()->user_id) {
            $test->delete();
            return 'success';
        }

        return 'not authorized';
    }

    /**
     * Takes a test code, if that test belongs to the authorized user
     * answersheet for that test is returned
     *
     * @param Request $request->code
     * @return string
     */
    public function answersheet(Request $request) {

        //get the authorized user
        $user = Auth::user();

        //get the test that was requested
        $test = Test::where('code', $request->code)->first();

        //verify the test exists
        if (!isset($test)) {
            return "test no longer exists";
        }

        //if this test belongs to this user, return answer_key
        if($user->id == $test->course()->first()->user_id) {
            return $test->answer_key;
        }

        return 'not authorized';
    }

    /**
     * Takes a test code, if that test belongs to the authorized user
     * then update the answer key to that test with the one supplied.
     *
     * @param Request $request
     * @return string
     */
    public function update(Request $request) {

        //get the authorized user
        $user = Auth::user();

        //get the test that was requested
        $test = Test::where('code', $request->code)->first();

        //verify the test exists
        if(!isset($test)) {
            return "test no longer exists";
        }

        //if this test belongs to this user, update the answerkey, otherwise do nothing

        if($user->id == $test->user_id) {
            //get student submission then update the answers with the new submitted answers
            Test::where('code', $request->code)->first()->update(array('answer_key' => $request->answers));

        } else {
            return "not authorized";
        }
    }

    /**
     * Takes a test code, if that test belongs to the authorized user
     * number of questions for that test is returned
     *
     * @param Request $request->code
     * @return string
     */
    public function questions(Request $request) {

        //get the authorized user
        $user = Auth::user();

        //get the test that was requested
        $test = Test::where('code', $request->code)->first();

        //verify the test exists
        if (!isset($test)) {
            return "test no longer exists";
        }

        //if this test belongs to this user, return the answer key
        if($user->id == $test->user_id) {
            $key = $test->answer_key;
            $data = strlen($key);
            return $data;
        }

        return 'not authorized';
    }

    /**
     * Takes a test code, if that test belongs to the authorized user
     * return a list of all the students for the test
     *
     * @param Request $request->code
     * @return array|string
     */
    public function students(Request $request) {

        //get the authorized user
        $user = Auth::user();

        //get the test that was requested
        $test = Test::where('code', $request->code)->first();

        //verify test exits
        if (!isset($test)) {
            return "test no longer exists";
        }

        //get all the submissions for the test
        $submissions = $test->submission()->select('stu_name')->get();

        //verify test belongs to user
        if($user->id == $test->user_id) {

            //verify there are submissions
            if (!isset($submissions)) {
                return "";
            } else {

                //Return a string - I want to return a string with each value seperated by comma.
                //this is because the original code did it this way, and I don't have time to refactor it
                //this can be changed in a future update.
                $data = "";

                foreach ($submissions as $submission) {
                    $data = $data.$submission->stu_name.",";
                }

                return $data;

            }
        } else {
            return 'not authorized';
        }
    }

    /**
     * Takes a test code, if that test belongs to the authorized user
     * return an array of each students score.
     *
     * @param Request $request->code
     * @return array|string
     */
    public function scores(Request $request) {

        //get the authorized user
        $user = Auth::user();

        //get the test that was requested
        $test = Test::where('code', $request->code)->first();

        //verify the test exists
        if (!isset($test)) {
            return 'test no longer exists';
        }

        //before bothering with calculations verify user has access to test
        if(!($user->id == $test->user_id)) {
            return 'not authorized';
        }

        //get all the submissions for the test
        $submissions = $test->submission()->get();

        //answer key for the test
        $key = str_split($test->answer_key);

        //return string
        $scores = "";

        //calculate scores and push to an array for returning
        foreach ($submissions as $submission) {

            $answers = str_split($submission->answer);
            $points = 0;

            foreach ($answers as $i=>$answer) {
                if ($answer == $key[$i]) {
                    $points += 1;
                }
            }

            $scores = $scores.$points.",";
        }

        return $scores;

    }

    /**
     * @param Request $request
     * $request->name : test name
     * $request->course_id : id for course this test belongs to
     * $request->code : the test code
     * $request->key : answer key
     * $request->submission : attempts allowed default 1
     * @return Response
     */
    public function create(Request $request) {

        Log::info("=============create test=============");
        Log::info($request);

        //the validator will validate the request information sent in prior to processing.
        $this->validate($request, [
           'name' => 'bail|required',
            'course_code' => 'bail|required|exists:courses,code',
            'code' => 'bail|required|unique:tests,code',
            'start' => 'required',
            'end' => 'required'
        ]);

        $course = Course::where("code", $request->course_code)->first();

        $test = new Test();
        $test->name = $request->name;
        $test->course_id = $course->id;
        $test->code = $request->code;
        $test->answer_key = "0";
        $test->start = new Carbon($request->start);
        $test->end = new Carbon($request->end);

        //check if the submission parameter was passed - if not db is set to default to 1.
        if(isset($request->submission) && $request->submission > 0) {
            $test->submissions = $request->submission;
        }

        try {
            $test->save();
            return response('success',200);
        } catch (Exception $e) {
            return response('error saving, try again later',500);
        }

    }

    /**
     * Returns a list of all test codes for the tests
     * created by the authorized user
     *
     * @param Request $request //not used
     * @return array|string
     */
    public function codes(Request $request) {

        //get the authorized user
        $user = Auth::user();

        $tests = $user->test()->select('code')->get();

        if(!isset($tests)) {
            return "";
        } else {

            //Return a string - I want to return a string with each value seperated by comma.
            //this is because the original code did it this way, and I don't have time to refactor it
            //this can be changed in a future update.
            $data = "";

            foreach($tests as $test) {
                $data = $data.$test->code.",";
            }

            return $data;

        }


    }

    /**
     * Returns a list of all test titles for the tests
     * created by the authorized user
     *
     * @param Request $request //not used
     * @return array|string
     */
    public function titles(Request $request) {

        //get the authorized user
        $user = Auth::user();

        $tests = $user->test()->select('name')->get();

        if(!isset($tests)) {
            return "";
        } else {

            //Return a string - I want to return a string with each value seperated by comma.
            //this is because the original code did it this way, and I don't have time to refactor it
            //this can be changed in a future update.
            $data = "";

            foreach($tests as $test) {
                $data = $data.$test->name.",";
            }

            return $data;

        }


    }

    /**
     * Returns a list of all test course names for the tests
     * created by the authorized user
     *
     * @param Request $request //not used
     * @return array|string
     */
    public function courses(Request $request) {

        //get the authorized user
        $user = Auth::user();

        $tests = $user->test()->select('course')->get();

        if(!isset($tests)) {

            return "";
        } else {

            //Return a string - I want to return a string with each value seperated by comma.
            //this is because the original code did it this way, and I don't have time to refactor it
            //this can be changed in a future update.
            $data = "";

            foreach($tests as $test) {
                $data = $data.$test->course.",";
            }

            return $data;

        }


    }

    /**
     *From the app, the teacher can reset the course code.
     *
     * @param Request $request->course_id : matches with course code in db
     * @return string
     */
    public function resetCourseID (Request $request) {

        Log::info("===============Reset Course Code=================");
        Log::info($request);


        $old = $request->code;

        $user = Auth::user();
        //Verify the user Owns this course before updating.
        $verify = $user->courses()->where("code", $old)->first();

        if($verify == null) {
            return response('test not found',404);
        }

        //* -------- Generate the course id
        $new = $this->randomString(7);

        Course::where("code", $old)->first()->update(["code" => $new]);

        return response($new, 200);
    }

    /**
     * Request a list of all students registered for a course.
     *
     * @param Request $request->course_id
     * @return string
     */
    public function getCourseList (Request $request) {
        $this->validate($request, [
           'code' => [
               'bail',
               'required',
               Rule::exists('courses','code')->where('user_id',Auth::user()->id)
           ]
        ]);

        $class = Course::where("code",$request->code)->first()->classroom()->get();

        //NOTE: returning class will return a JSON array containing all the student information
        //however, the app currently requires that this be a string, instead of changing the app at this time
        //this bit of code converts $class into a string format that the app is already coded to handle.
        //in later versions the app should be updated to handle JSON objects.
        $returnString = "";
        foreach($class as $student) {
            $returnString = $returnString.$student->name.",";
        }
        $returnString=$returnString."-";
        foreach ($class as $student) {
            $returnString = $returnString.$student->stu_id.",";
        }

        return $returnString;
    }

    /**
     * Generates a unique ID and creates a new course for the logged in user.
     *
     * @param Request $request
     * @return string
     */
    public function createCourse (Request $request) {

        Log::info("===========Create Course==============");
        Log::info($request);

        //make sure this user doesn't already have a course with this name.
        $this->validate($request, [
           'name' => [
               'bail',
               'required',
               Rule::unique('courses')->where(function ($query) {
                   $query->where('user_id', Auth::user()->id);
               })
           ]
        ]);


        //generate a unique code for the class
        $unique = false;
        $code = $this->getCourseCode();

        $course = new Course();
        $course->name = $request->name;
        $course->code = $code;
        $course->user_id = Auth::user()->id;
        $course->save();


        return $code;
    }

    //This one needs work ... not 100% understanding the php code jacob wrote
    //need to play with it in app to see what it does.
    public function getDash(Request $request) {

        //Note: the app is designed to handle strings, so i'm returning the results in a string
        //format that the app is alreayd coded to handle. In the future, this should be changed
        //to send the results as a JSON object.

        $courses = Auth::user()->courses()->get();
        $titles = "";
        $codes = "";
        foreach ($courses as $course) {
            $titles = $titles.$course->name.",";
            $codes = $codes.$course->code.",";
        }

        if(Auth::user()->type == 0) {
           return "Error: can't retrieve teacher dashboard while logged in as a student";
        }
        else {
            //for teachers return all course id's
            return $titles."-".$codes;
        }


    }

    /**
     * Delete a requested Course
     *
     * @param Request $request
     * @return string
     */
    public function deleteCourse(Request $request) {

        Log::info("=============Delete Course=================");
        Log::info($request);

        //Note Deleting a course will delete all the tests created for that course
        //but it will not delete all the submissions. Submissions will automatically
        //be deleted using Laravel Scheduler.
        $this->validate($request, [
           'code' => [
               'bail',
               'required',
               Rule::exists('courses','code')->where('user_id', Auth::user()->id)
           ]
        ]);

        $course = Course::where("code",$request->code)->first();
        Classroom::where("course_id",$course->id)->delete();
        Test::where("course_id", $course->id)->delete();
        $course->delete();

        return "1";

    }

    /**
     * Deletes student from a course
     *
     * @param Request $request
     * @return Response
     */
    public function deleteStudent(Request $request) {

        Log::info("=============Delete Student from Class==============");
        Log::info($request);

        $this->validate($request, [
           'stu_id' => 'required|exists:users,stu_id',
            'course' => 'required|exists:courses,code'
        ]);

        $course = Course::where("code",$request->course)->first();
        $student = User::where("stu_id", $request->stu_id)->first();

        Log::info($course);
        Log::info($student);

        if ( $course->user_id == Auth::user()->id) {
            //Delete student from the classroom
            Classroom::where([
                ['stu_id',$student->id],
                ['course_id', $course->id]
            ])->delete();

            //delete tests student took for that classroom (if any)
            $tests = $course->tests()->get();
            foreach ($tests as $test) {
                $test->submissions()->where('stu_id', $student->id)->delete();
            }

            return response('success',200);
        } else {
            return response("this user is not authorized to delete this resource", 403);
        }

    }

    /**
     * Takes an test id and returns the answer key for that test
     * And the logged in users submission answer for that test
     *
     * @param Request $request
     * @return string
     */
    public function getSubmission(Request $request) {
        $this->validate($request, [
           'id' => [
               'bail',
               'required',
               Rule::exists('submissions','test_id')->where('stu_id',Auth::user()->id),
               Rule::exists('tests')
           ]
        ]);

        //get answer from submission
        $submissions = Submission::where('test_id', $request->id)->where('stu_id', Auth::user()->id)->first();
        //get answer key from test
        $test = Test::find($request->id)->first();

        return $test->answer_key."-".$submissions->answer;
    }

    /**
     * Return information for logged in user.
     *
     * @param Request $request
     * @return string
     */
    public function getUser(Request $request) {
        $user = Auth::user();

        $returnString = $user->name."-".$user->email;

        if($user->type === 0) {
            $returnString = $returnString."-".$user->stu_id;
        }

        return $returnString;
    }

    /**
     * Takes a test ID and returns all grades and student names for that test
     * Like all other functions this returns a string instead of a JSON object
     * due to how the app was originally coded, this should be changed in a future update.
     *
     * @param Request $request
     * @return string
     */
    public function getGrades(Request $request) {
        $this->validate($request, [
           'code' => 'bail|required|exists:tests,code'
        ]);

        $test = Test::where("code",$request->code)->first();
        $submissions = $test->submissions()->get();
        $returnString = "";
        $names = "";

        foreach ($submissions as $submission) {
            $grade = $this->calcGrade($test->answer_key, $submission->answer);
            $names = $names.$submission->stu_name.",";
            $returnString = $returnString.$grade.",";
        }

        return $returnString."-".$names;
    }

    public function registerForClass(Request $request) {

        Log::info("============register for class================");

        $this->validate($request, [
           'course_id' => 'required|exists:courses,code'
        ]);

        $course = Course::where('code', $request->course_id)->first();

        $check = Auth::user()->classrooms()->where("course_id",$course->id)->get();
        Log::info($check);

        if($check->count()) {
            return response("You're already registered for this class",422);
        }

        $class = new Classroom();
        $class->stu_id = Auth::user()->id;
        $class->course_id = $course->id;
        $class->save();

        return $course->code."-".$course->name;

    }

    public function deleteClass(Request $request) {
        $this->validate($request, [
           'code' => 'required|exists:courses,code'
        ]);

        $course = Course::where("code",$request->code)->first();
        if ($course->user_id != Auth::user()->id) {
            return response('you are not authorized to delete this resource', 403);
        }

        Classroom::where('course_id',$request->id)->delete();

        //generate a unique code for the class
        $unique = false;
        $code = $this->getCourseCode();

        $course->code = $code;
        $course->save();

        return $code;

    }

    public function newCourseCode(Request $request) {
        $this->validate($request, [
           'id' => 'required|exists:courses'
        ]);

        $course = Course::find($request->id);
        if ( $course->user_id != Auth::user()->id) {
            return response('User is not authorized to modify this resource', 403);
        }

        $code = $this->getCourseCode();
        $course->code = $code;
        $course->save();

        return $code;
    }

    public function studentDash(Request $request) {

        Log::info($request);

        //select all the courses this student is registered for
        $courses = Auth::user()->classrooms()->get();

        $titles = "";   //course names
        $ids = "";      //course codes
        $grades = "";   //course grades

        //for each course do the following:
        foreach ($courses as $course) {

            $grade = 0; //grade for the course
            $total = 0; //grade total for the course


            $titles = $titles.$course->name.",";
            $ids = $ids.$course->id.",";

            //get all the tests created for this course;
            $tests = $course->tests()->get();

            //for each test do the following:
            foreach ($tests as $test) {

                //check if test is closed - if not, go to next test.
                $now = Carbon::now();
                $end = Carbon::parse($test->end);
                if ($now->lt($end)) {
                    continue;
                }

                //find the submission for this test by the student
                $submission = $test->submissions()->where('stu_id', Auth::user()->id)->first();

                //if there is no submission for this student - go to next test.
                if($submission == null || $submission->submission_number == 0) {
                    continue;
                }

                //calculate the score for this test
                $score = $this->calcScore($test->answer_key, $submission->answer);
                $grade = $grade + $score;
                $total = $total + (strlen($test->answer_key));
                Log::info($grade);
                Log::info($total);

            }

            $grades = $grades.$grade."/".$total.",";

        }

        //Return the strings in the following format
        // "titles-ids-grades"
        return $titles."-".$ids."-".$grades;
    }

    public function getCourse(Request $request) {
        $this->validate($request, [
           'code' => [
               'bail',
               'required',
               Rule::exists('courses')->where('user_id', Auth::user()->id)
           ]
        ]);

        //get all tests created for the requested course
        $tests = Course::where("code",$request->code)->first()->tests()->get();


        $titles = "";
        $codes = "";

        //for each test do the following
        foreach($tests as $test) {

            //add the test name to the titles string
            $titles = $titles.$test->name.",";

            //add the test code to the codes string
            $codes = $codes.$test->code.",";
        }

        //return test titles and codes
        return $codes."-".$titles;
    }

    public function updateTest(Request $request) {

        Log::info("==============Update Test================");
        Log::info($request);

        $this->validate($request, [
           'answers' => 'required',
            'code' => 'required|exists:tests'
        ]);

        //validate that the user owns this test
        $test = Test::where("code",$request->code)->first();
        $cID = $test->course_id;
        $check = Course::find($cID)->where('user_id', Auth::user()->id)->first();
        if ($check == null) {
            return 'You are not authorized to modify this resource';
        }

        //if valid, update the answersheet
        $test->answer_key = $request->answers;
        $test->save();

        return response('success', 200);
    }

    public function testCode (Request $request) {
        $code = $this->getTestCode();

        return $code;
    }

    public function updateCourseTitle (Request $request) {
        Log::info("===============update title===============");
        Log::info($request);

        $this->validate($request, [
           'title' => 'required',
            'code' => [
                'required',
                Rule::exists('courses')->where('user_id', Auth::user()->id)
            ]
        ]);

        //get course and update title with new title
        Course::where("code",$request->code)->first()->update(['name' => $request->title]);

        return response($request->title, 200);
    }

    public function updateUser (Request $request) {

        Log::info("==========update user==============");
        Log::info($request);

        $this->validate($request, [
            'email' => [
                'email',
              Rule::unique('users','email')->ignore(Auth::user()->id)
            ],
            'id' => [
                Rule::unique('users','stu_id')->ignore(Auth::user()->id)
            ]
        ]);

        $user = Auth::user();

        if( isset($request->name)) {
            $user->name = $request->name;
        }

        if ( isset($request->email)) {
            $user->email = $request->email;
        }


        if ( isset($request->type)) {
            $user->type = $request->type;
        }

        if ( isset($request->id)) {
            $user->stu_id = $request->id;
        }

        $user->save();

        return $user;
    }

    public function getTests (Request $request) {
        $this->validate($request, [
           'id' => 'required|exists:courses'
        ]);

        //get all tests for the requested course
        $tests = Course::find($request->id)->tests()->get();

        $titles = ""; //test names
        $ids = ""; //test codes
        $grades = ""; //test grades

        //for each test do the following
        foreach ($tests as $test) {
            Log::info("=========get tests============");

            //check if test is closed - if not, go to next test.
            $now = Carbon::now();
            $end = Carbon::parse($test->end);
            if ($now->lt($end)) {
                continue;
            }

            //get this students submission
            $submission = $test->submissions()->where('stu_id', Auth::user()->id)->first();

            //if there are no submissions skip to the next test.
            if ($submission == null || $submission->submission_number == 0) {
                continue;
            }


            Log::info($submission);
            //get the grade for that submission
            $grade = $this->calcGrade($test->answer_key, $submission->answer);
            Log::info($grade);

            $titles = $titles.$test->name.",";
            $ids = $ids.$test->id.",";
            $grades = $grades.$grade.",";
        }

        return $grades."-".$titles."-".$ids;
    }

    public function resetClass (Request $request) {

        Log::info("==============Reset Class=================");
        Log::info($request);

        $this->validate($request, [
           'code' => [
               'required',
               Rule::exists('courses','code')->where('user_id',Auth::user()->id)
           ]
        ]);

        $course = Course::where('code', $request->code)->first();
        Classroom::where('course_id', $course->id)->delete();

        $tests = $course->tests()->get();
        foreach ($tests as $test) {
            $code = $this->getTestCode();
            $test->code = $code;
            $test->save();
        }

        return "1";
    }
}
