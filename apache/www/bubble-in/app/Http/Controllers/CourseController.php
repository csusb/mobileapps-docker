<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use Auth;

class CourseController extends Controller
{

    //include authentication middleware
    public function __construct()
    {
        $this->middleware('auth');

    }

    //list all courses
    public function index() {
        $courses = Auth::user()->courses()->get();
        return view('courses', ['courses' => $courses]);
    }

    //update course
    public function update(Request $request, $id) {
        Course::find($id)->update(['name' => $request->name]);

        return redirect('/');
    }

    //create course
    public function store(Request $request) {
        $course = new Course();
        $course->code = 'TEMP';
        $course->name = $request->name;
        $course->user_id = Auth::user()->id;
        $course->save();

        return redirect('/');
    }

    //delete course
    public function destroy($id) {
        Course::find($id)->delete();

        return redirect('/');
    }

    //list students for a course
    public function getStudents($id) {
        $students = Auth::user()->courses()->where("id", $id)->first()->classroom()->get();
        return view('students', ['students' => $students]);
    }
}
