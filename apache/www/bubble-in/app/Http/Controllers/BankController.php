<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Bank;
use App\BankQuestion;
use App\BankOption;
use Log;
use Auth;

class BankController extends Controller
{
    //include authentication middleware
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('valid');
    }


    public function index($id)
    {
        $banks = Course::find($id)->banks()->get();

        return view('banks', ['banks' => $banks, 'course' => $id]);
    }

    public function show($course, $id)
    {
        Log::info("=====GET BANK=====");
        Log::info($id);
        $questions = Bank::find($id)->questions()->with('options')->get();
        $bank = Bank::find($id);
        Log::info($bank);
        Log::info($questions);

        return view('bank', ['questions' => $questions, 'name' => $bank->name, 'bank' => $bank]);
    }

    public function store(Request $request, $course)
    {
        $bank = new Bank();
        $bank->name = $request->bname;
        $bank->user_id = Auth::user()->id;
        $bank->course_id = $course;
        $bank->save();

        Log::info("======MAKE BANK======");
        Log::info($bank->id);

        return back();
    }

    public function destroy($course, $bank_id)
    {
        $bank = Bank::find($bank_id);

        if ($bank->tests()->count() == 0) {
            $bank->forceDelete();
        } else {
            $bank->delete();
        }

        return back();
    }

    public function addQuestion(Request $request, $course, $bank_id)
    {
        Log::info($request);

        $q = new BankQuestion();
        $q->question = $request->question;
        $q->bank_id = $bank_id;
        $q->save();

        $correct = new BankOption();
        $correct->correct = true;
        $correct->name = $request->correct;
        $correct->bq_id = $q->id;
        $correct->save();

        $wrongs = $request->wrong;
        foreach ($wrongs as $wrong) {
            $w = new BankOption();
            $w->correct = false;
            $w->bq_id = $q->id;
            $w->name = $wrong;
            $w->save();
        }


        return 'success';
    }

    public function BulkAddQuestions(Request $request, $course, $bank_id)
    {
        Log::info($request);

        $handle = fopen($request->file('file'), "r");
        $bid = $bank_id;
        $qid = 0;

        while ($csvLine = fgetcsv($handle, 1000, ",")) {
            if ($csvLine[0] == 'Q') {
                $q = new BankQuestion();
                $q->question = $csvLine[1];
                $q->bank_id = $bid;
                $q->save();
                $qid = $q->id;
            } else {
                $correct = 0;
                if ($csvLine[0] == 'T') {
                    $correct = 1;
                }

                $op = new BankOption();
                $op->name = $csvLine[1];
                $op->bq_id = $qid;
                $op->correct = $correct;
                $op->save();
            }
        }

        return back();
    }

    public function stats(Request $request, $course, $bank_id) {
        $tests = Bank::find($bank_id)->first()->tests();
        $bank_name = Bank::find($bank_id)->first()->name;

        $tests = $tests->unique('group');
        Log::info($tests);
        $results = collect([
            'questions'=>collect([]),
            'options'=>collect([])
        ]);

        foreach ($tests as $test) {

            $stats = collect(HomeController::BankTestStats($test->group));
            $results['questions']->push($stats['questions']);
            $results['options']->push($stats['options']);
        }

        $results['questions'] = $results['questions']->collapse();
        $results['options'] = $results['options']->collapse();


        //Add up the counts so we can remove duplicates from the collection
        //this will make it smaller and easier to parse on the HTML view.
        foreach ($results['questions'] as $i=>$question) {
            $dupes = $results['questions']->where('id',$question['id']);
            $count = 0;

            foreach ($dupes as $dupe) {
                $count += $dupe['count'];
            }

            $results['questions'][$i]['count'] = $count;

        }

        foreach ($results['options'] as $i=>$option) {
            $dupes = $results['options']->where('bo', $option['bo']);
            $count = 0;

            foreach ($dupes as $dupe) {
                $count += $dupe['count'];
            }

            $results['options'][$i]['count'] = $count;
        }

        $results['questions'] = $results['questions']->unique('id');
        $results['options'] = $results['options']->unique('bo');

        //Send results to a view.
        return view('bank_stats', ['stats'=>$results, 'name'=>$bank_name]);
    }
}
