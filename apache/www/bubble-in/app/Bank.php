<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Test;
use App\TestQuestion;
use Log;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'user_id', 'course_id'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function course() {
        return $this->belongsTo('App\Course');
    }

    public function questions() {
        return $this->hasMany('App\BankQuestion');
    }

    public function tests() {
        $testIDs = [];
        $questions = $this->questions()->get();
        foreach ($questions as $question) {
            $tqs = TestQuestion::where('bq_id',$question->id)->get();
            if( !is_null($tqs)) {
                foreach ($tqs as $tq) {
                    array_push($testIDs,Test::find($tq->bt_id)->id);
                }
            }
        }

        return Test::find($testIDs);
    }

    //deleting a bank deletes all tests created from that bank.
    //deleting a bank deletes all questions associated with that bank.
    protected static function boot()
    {
        parent::boot();

        static::deleting(function($bank){
            if($bank->forceDeleting) {
                $bank->questions()->get()->each(function($question) {
                    $question->forceDelete();
                });
            } else {
                $bank->questions()->get()->each(function($question) {
                    $question->delete();
                });
            }
        });
    }
}
