<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

    protected $fillable = [
        'name', 'code'
    ];

    public function owner() {
        return $this->belongsTo('App\User');
    }

    public function tests() {
        return $this->hasMany('App\Test');
    }

    public function classroom() {
        return $this->belongsToMany('App\User', 'classrooms', 'course_id', 'stu_id')->select([
            'name',
            'email',
            'type',
            'users.stu_id',
            'course_id'
        ]);
    }

    public function banks() {
        return $this->hasMany('App\Bank');
    }
}
