<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestOption extends Model
{

    public function bankOption() {
        return $this->belongsTo('App\BankOption', 'bo_id');
    }
}
