<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TestOption;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankOption extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'bq_id', 'correct'];

    public function question() {
        return $this->belongsTo('App\BankQuestion', 'id', 'bq_id');
    }

}
