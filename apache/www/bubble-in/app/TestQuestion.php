<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestQuestion extends Model
{
    protected $fillable = ['bt_id', 'bq_id'];

    public function test() {
        return $this->belongsTo('App\Test', 'bt_id');
    }

    public function bankQuestion() {
        return $this->belongsTo('App\BankQuestion', 'bq_id');
    }

    public function options() {
        return $this->hasMany('App\TestOption', 'tq_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($question) {

            //deleting a question deletes all its options
            $question->options()->delete();
        });
    }
}
