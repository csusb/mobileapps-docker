<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    protected $fillable = ['test_id','id','stu_name','answer','submission_number'];

    public function test() {
        return $this->belongsTo('App\Test');
    }

    public function student() {
        return $this->belongsTo('App\User', 'id', 'stu_id');
    }
}
