<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankTest extends Model
{
    protected $fillable = ['name', 'code', 'course_id'];

    public function course() {
        return $this->belongsTo('App\Course');
    }

    public function questions() {
        return $this->hasMany('App\TestQuestion');
    }

    public function submissions() {
        return $this->hasMany('App\Submission');
    }
}
