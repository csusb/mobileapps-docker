<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    public function course() {
        return $this->belongsTo('App\Course');
    }

    public function students() {
        return $this->hasMany('App\User', 'id', 'stu_id');
    }
}
