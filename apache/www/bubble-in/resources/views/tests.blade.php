@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tests</div>

                <div class="panel-body">

                    @if(count($tests) < 1)
                        <a href="{{ url('/create/'.$course) }}">Create Your First Test!</a>
                    @else
                        <table class="testtable">
                            <thead>
                                <th>Test Name</th>
                                <th></th>
                            </thead>
                            <tbody>
                            @foreach($tests as $test)
                                <tr class="testrow">
                                    <td>{{ $test->name }}</td>
                                    <td><a href="{{ url('/course/'.$test->course_id.'/tests/'.$test->id.'/submissions') }}">scores</a></td>
                                    <td><a href="{{ url('/course/'.$test->course_id.'/tests/'.$test->id.'/edit') }}">edit</a></td>
                                    <td> {!! Form::open(['method'=>'DELETE', 'url'=>'/course/'.$test->course_id.'/tests/'.$test->group]) !!}
                                        <button data-toggle="tooltip" data-placement="top" class="delIcon" title="Delete" type="submit" onclick="return confirm('Are you sure you want to delete this item?');"><img src="{{ URL::asset('img/ic_delete_forever_black_48px.svg') }}" class="delete icon"></img></button>
                                        {!! Form::close() !!}
                                    </td>

                                </tr>

                            @endforeach
                            </tbody>
                        </table>

                        <button class="newtest"><a href="{{ url('/course/'.$course.'/tests/create') }}">New Test</a></button>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
