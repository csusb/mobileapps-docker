@extends('layouts.app')

@section('content')

    <?php
        $stu_answer = str_split($student->answer);
        $answers = str_split($test->answer_key);
    ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="heading-left">Test Results For <a href="{{ url('/scores/'.$test->id) }}">{{$test->name}} : {{$test->code}}</a></div>
                    <div class="heading-right">{{$course}}</div>
                </div>

                <div class="panel-body">
                    Name: {{ $student->stu_name }} <br />
                    ID: {{$student->stu_id}} <br />
                    Score: {{$score}}/{{strlen($test->answer_key)}} <br />

                    <ol>
                        @foreach($answers as $i=>$answer)
                            <li>
                            {{ $answer }}  {{ $stu_answer[$i] }}
                                @if($answer == $stu_answer[$i])
                                    <p class="testmark correct">&#10004</p>
                                @else
                                    <p class="testmark wrong">&#10006</p>
                                @endif
                          </li>
                        @endforeach
                    </ol>

                    <a class="printme" href="javascript:window.print()">print this page</a>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
