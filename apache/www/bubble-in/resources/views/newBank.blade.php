
@extends('layouts.app')

@php
    $q=1;
@endphp

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">New Test Bank</div>

                <div class="panel-body">

                    {{ Form::open(['url'=>'addQuestion','id'=>'addQ']) }}

                    {{ Form::label('question', 'Question '.$q) }} <br />
                    {{ Form::text('question') }}
                    <br />

                    {{ Form::label('correct', 'correct answer') }} <br />
                    {{ Form::text('correct')}}
                    <br />

                    {{ Form::label('wrong', 'wrong answer') }} <br />
                    {{ Form::text('wrong')}}
                    <br />
                    {{ Form::text('wrong')}}
                    <br />
                    {{ Form::text('wrong')}}
                    <br />
                    {{ Form::text('wrong')}}
                    <br />
                    {{ Form::text('wrong')}}
                    <br />


                    {{ Form::submit('create') }}
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
