@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tests</div>

                <div class="panel-body">

                    @if(count($banks) < 1)
                        <a href="#" role="button" data-toggle='modal' data-target="#newProject" >Create Your First Test Bank!</a>
                    @else
                        <table class="testtable">
                            <thead>
                                <th>Bank Name</th>
                                <th></th>
                            </thead>
                            <tbody>
                            @foreach($banks as $bank)
                                <tr class="testrow">
                                    <td>{{ $bank->name }}</td>
                                    <td><a href="{{ url('course/'.$bank->course_id.'/banks/'.$bank->id) }}">view</a></td>
                                    <td><a href="{{ url('/course/'.$bank->course_id.'/banks/'.$bank->id.'/stats') }}">Stats</a></td>
                                    <td> {!! Form::open(['method'=>'DELETE', 'url'=>'/course/'.$bank->course_id.'/banks/'.$bank->id]) !!}
                                        <button data-toggle="tooltip" data-placement="top" class="delIcon" title="Delete" type="submit" onclick="return confirm('Are you sure you want to delete this item?');"><img src="{{ URL::asset('/img/ic_delete_forever_black_48px.svg') }}" class="delete icon"></img></button>
                                        {!! Form::close() !!}
                                    </td>

                                </tr>

                            @endforeach
                            </tbody>
                        </table>

                        <button href="#" role="button" data-toggle='modal' data-target="#newProject" >New Test Bank</button>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<!------------------------------ New Bank Modal -------------------------------->
<div class="modal fade" id="newProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Test Bank</h4>
            </div>
            {{ Form::open(['url'=>'/course/'.$course.'/banks']) }}
            <div class="modal-body">
                {{ Form::label('bname', 'Bank Name') }}
                {{ Form::text('bname', '',['required']) }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{ Form::submit('create', ['class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!------------------------------ New Bank Modal -------------------------------->
@endsection
