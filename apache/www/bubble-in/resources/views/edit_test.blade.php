@extends('layouts.app')

@section('content')

    <?php

        $answers = str_split($test->answer_key);
        $today = new \Carbon\Carbon();

            $start = explode(" ", $test->start);
            $startdate = new \Carbon\Carbon($start[0]);
            $starttime = new \Carbon\Carbon($start[1]);

            $end = explode(" ", $test->end);
            $enddate = new \Carbon\Carbon($end[0]);
            $endtime = new \Carbon\Carbon($end[1]);

    ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="heading-left">Edit {{$test->name}} : {{$test->code}}</div>
                    <div class="heading-right">{{$course}}</div>

                </div>

                <div class="panel-body">

                    {{ Form::open(['url'=>'/course/'.$test->course_id.'/tests/'.$test->id, 'method'=>'PUT']) }}
                    @if($test->answer_key == null)
                        <h3>This test was generated from a test bank</h3>
                        <h4>Bank: {{$bank}}</h4>
                        <p>Number of questions: {{$questions}}</p>
                        <p>Number of tests generated: {{$tests}}</p>
                    @else
                        @for ($i=0; $i < strlen($test->answer_key); $i++)
                                {{ $i+1 }}
                                {{ Form::label( $i,'A') }}
                                {{ Form::radio($i,'A',($answers[$i] == 'A'),['class'=>'ans','required']) }}
                                {{ Form::label($i,'B') }}
                                {{ Form::radio($i,'B',($answers[$i] == 'B'),['class'=>'ans','required']) }}
                                {{ Form::label($i,'C') }}
                                {{ Form::radio($i,'C',($answers[$i] == 'C'),['class'=>'ans','required']) }}
                                {{ Form::label($i,'D') }}
                                {{ Form::radio($i,'D',($answers[$i] == 'D'),['class'=>'ans','required']) }}
                                <br />
                        @endfor
                    @endif

                    @if ($answers[0] != "0")


                            {{ Form::label('sdate', 'start date') }} <br />
                            {{ Form::date('sdate', $startdate) }}
                            <br />

                            {{ Form::label('stime', 'start time') }} <br />
                            {{ Form::time('stime', $starttime->format('H:m')) }}
                            <br />

                        @if ($test->end < $today)
                            <p class="expired">This test has expired. Change the end date to open it again</p>
                        @endif
                            {{ Form::label('edate', 'end date') }} <br />
                            {{ Form::date('edate', $enddate) }}
                            <br />

                            {{ Form::label('etime', 'end time') }} <br />
                            {{ Form::time('etime', $endtime->format('H:m')) }}
                            <br />

                    @endif


                    {{ Form::submit('Save') }}
                    {{ Form::close() }}


                </div>
            </div>
        </div>
    </div>
</div>

@endsection

