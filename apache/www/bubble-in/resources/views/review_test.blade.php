@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <ol>
                            @foreach($questions as $question)
                                <li>{{$question['question']->question}}</li>
                                <ol type="a">
                                    @foreach($question['options'] as $i=>$option)
                                        <li
                                            @if($option->correct)
                                                class="correct"
                                            @endif
                                        >{{$option->name}}</li>
                                    @endforeach
                                </ol>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
