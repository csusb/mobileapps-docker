@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tests</div>

                <div class="panel-body">

                    @if(count($courses) < 1)
                        <a href="{{ url('/new-course') }}">Add Your First Course</a>
                    @else
                        <table class="testtable">
                            <tbody>
                            @foreach($courses as $course)
                                <tr class="testrow">
                                    <td>{{ $course->name }}</td>
                                    <td><a href="{{ url('/course/'.$course->id.'/banks') }}">banks</a></td>
                                    <td><a href="{{ url('/course/'.$course->id.'/tests') }}">tests</a></td>
                                    <td><a href="{{ url('/students/'.$course->id) }}">students</a></td>
                                    <td><a href="#" role="button" data-toggle='modal' data-target="#editCourse" >edit</a></td>
                                    <td> {!! Form::open(['method'=>'DELETE', 'url'=>'/course/'.$course->id]) !!}
                                        <button data-toggle="tooltip" data-placement="top" class="delIcon" title="Delete" type="submit" onclick="return confirm('Are you sure you want to delete this item?');"><img src="{{ URL::asset('img/ic_delete_forever_black_48px.svg') }}" class="delete icon"></img></button>
                                        {!! Form::close() !!}
                                    </td>

                                </tr>

                            @endforeach
                            </tbody>
                        </table>

                        <button href="#" role="button" data-toggle='modal' data-target="#newCourse" >Add Course</button>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<!------------------------------ New Course Modal -------------------------------->
<div class="modal fade" id="newCourse" tabindex="-1" role="dialog" aria-labelledby="New Course">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Course</h4>
            </div>
            {{ Form::open(['url'=>'/course']) }}
            <div class="modal-body">
                {{ Form::label('name', 'Course Name') }}
                {{ Form::text('name', '',['required']) }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{ Form::submit('create', ['class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!------------------------------ New Course Modal -------------------------------->

<!------------------------------ Edit Course Modal -------------------------------->
<div class="modal fade" id="editCourse" tabindex="-1" role="dialog" aria-labelledby="Edit Course">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Course</h4>
            </div>
            {{ Form::open(['url'=>'/course/'.$course->id, 'method'=>'PUT']) }}
            <div class="modal-body">
                {{ Form::label('name', 'Course Name') }}
                {{ Form::text('name', '',['required']) }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{ Form::submit('edit', ['class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!------------------------------ New Course Modal -------------------------------->
@endsection
