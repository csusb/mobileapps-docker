@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        {{ $number }} tests created <br />
                        Name: {{$test->name}} <br />
                        Bank: {{ $bank }} <br />
                        Start: {{ $test->start }} <br />
                        End: {{ $test->end }} <br />

                        {{ Form::open(['url'=>'/course/'.$test->course_id.'/tests/'.$test->group.'/printTests']) }}
                        {{ Form::submit('Print',['class'=>'btn btn-primary']) }}
                        <a class="btn btn-default" href="{{ url('/course/'.$test->course_id.'/tests/') }}">Return</a>
                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
