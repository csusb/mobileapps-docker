@extends('layouts.app')

@php
    $prefix = '/course/'.$test->course_id.'/tests/'.$test->id.'/submissions/';
@endphp

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="heading-left">Test Results For {{$test->name}} : {{$test->code}}</div>
                        <div class="heading-right">{{$test->code}}</div>

                    </div>

                    <div class="panel-body">

                        <table>
                            <thead>
                            <th>Test CODE</th>
                            <th>Student ID</th>
                            <th>Student Name</th>
                            <th>Scores</th>
                            </thead>
                            <tbody>
                            @foreach ($tests as $i=>$test)
                                @foreach($test->submissions as $submission)
                                    <tr>
                                        <td>{{ $test->code }}</td>
                                        <td>{{ $submission->stu_id }}</td>
                                        <td>{{ $submission->stu_name }}</td>
                                        <td>{{ $scores[$i] }}/{{ $questions }}</td>
                                        <td>
                                            <a href="{{ url('/course/'.$test->course_id.'/tests/'.$test->id.'/submissions/'.$submission->id) }}">Review</a>
                                        </td>
                                    </tr>
                                @endforeach
                                @if(count($test->submissions) < 1)
                                    <tr>
                                        <td>{{ $test->code }}</td>
                                        <td>No Submissions</td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <a href="{{ url('/course/'.$test->course_id.'/tests/'.$test->id) }}">Review</a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>

                        </table>

                        <a class="btn btn-default" href="{{ url('/scores/'.$test->id.'/export') }}">Download</a>
                        <a class="btn btn-default"
                           href="{{ url('/course/'.$test->course_id.'/tests/'.$test->id.'/stats') }}">View
                            Statistics</a>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
