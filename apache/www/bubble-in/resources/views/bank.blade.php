@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="heading-left">{{$name}} Test Bank</div>
                    <div class="heading-right"><button href="#" role="button" data-toggle='modal' data-target="#newQuestion" >Add Questions</button></div>
                    <div class="heading-right"><button href="#" role="button" data-toggle='modal' data-target="#bulkBank" >Bulk Upload</button></div>
                </div>
                <div class="panel-body">
                    <ol id="questions">
                    @foreach ($questions as $q)
                        <li class="panel-title">{{$q->question}} </li>
                            <ol>
                            @foreach($q->options as $option)
                                <li type="a"
                                    @if($option->correct)
                                        class="correct"
                                    @endif
                                >{{ $option->name }}</li>
                            @endforeach
                            </ol>
                    @endforeach
                    </ol>

                </div>
            </div>
        </div>
    </div>
</div>

<!------------------------------ Add Question Modal -------------------------------->
<div class="modal fade" id="newQuestion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Test Bank</h4>
            </div>
            {{ Form::open(['url'=>'addQuestion','id'=>'addQ']) }}
            <div class="modal-body">
                {{ Form::label('question', 'Question') }} <br />
                {{ Form::text('question', null,['required' => true]) }}
                <br />

                {{ Form::label('correct', 'correct answer') }} <br />
                {{ Form::text('correct', null,['required' => true])}}
                <br />

                {{ Form::label('wrong[]', 'wrong answer') }} <br />
                {{ Form::text('wrong[]', null,['required' => true])}}
                <br />
                {{ Form::text('wrong[]')}}
                <br />
                {{ Form::text('wrong[]')}}
                <br />
                {{ Form::text('wrong[]')}}
                <br />
                {{ Form::text('wrong[]')}}
                <br />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{ Form::submit('Add Question', ['class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!------------------------------ Add Question Modal -------------------------------->

<!------------------------------ Bulk Upload Modal -------------------------------->
<div class="modal fade" id="bulkBank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Test Bank</h4>
            </div>
            {{ Form::open(['url'=>'/course/'.$bank->course_id.'/banks/'.$bank->id.'/bulkAddQuestion','id'=>'addQ','file'=>true, 'enctype'=>'multipart/form-data']) }}
            <div class="modal-body">
                {{ Form::label('file', 'Select a .csv file to upload') }} <br />
                {{ Form::file('file', ['required' => true]) }}
                <br />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{ Form::submit('Add Question', ['class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!------------------------------ Bulk Upload Modal -------------------------------->
    <script>
        var bank = {{$bank->id}}
        var course = {{$bank->course_id}}
    </script>
@endsection
