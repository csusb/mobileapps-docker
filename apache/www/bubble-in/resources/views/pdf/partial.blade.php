<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>Name</h1>
                    <h1>Student ID</h1>
                    <h2>Test Code: {{ $test['test']->code }}</h2>
                </div>

                <div class="panel-body">
                    <ol>
                        @foreach($test['qs'] as $q)
                            <li>{{$q['question']->question}}</li>
                            <ol type="a">
                                @foreach($q['options'] as $option)
                                    <li>{{$option['name']}}</li>
                                @endforeach
                            </ol> <br />
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div style="page-break-after: always"></div>
</div>