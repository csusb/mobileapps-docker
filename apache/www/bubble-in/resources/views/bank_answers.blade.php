@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <ol>
                            @foreach($vals as $val)
                                <li>{{$val['question']->question}}</li>
                                <ol type="a">
                                    @foreach($val['options'] as $i=>$option)
                                        <li
                                            @if($option->correct)
                                                class="correct"
                                            @elseif($val['answer'] === ($i))
                                                class="wrong"
                                            @endif
                                        >{{$option->name}}</li>
                                    @endforeach
                                </ol>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
