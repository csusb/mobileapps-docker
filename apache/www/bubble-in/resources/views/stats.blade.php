@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="heading-left">Statistics For {{$test->name}}</div>
                        <div class="heading-right">{{$test->code}}</div>
                    </div>

                    <div class="panel-body">

                        @if (count($scores) < 1)
                            <p>This test has no statistics. No one has taken it.</p>
                        @else
                            <p>Mean: {{$mean}}</p>
                            <p>Median: {{$median}}</p>
                            <p>Mode: {{$mode}}</p>
                            <p>Highest Score: {{$highest}}</p>
                            <p>Lowest Score: {{$lowest}}</p>
                            <p>Range: {{$range}}</p>

                            @if($bank == null)
                                <ol>
                                    @foreach($stats as $i=>$stat)
                                        <li>
                                            @foreach($stat as $i=>$option)
                                                {{ $i }}: {{ $option/count($scores) }}
                                            @endforeach
                                        </li>
                                    @endforeach
                                </ol>
                            @else
                                <ol>
                                    @foreach($stats['questions'] as $question)
                                        <li>{{ $question['question'] }}</li>
                                        <li style="list-style-type:none"> Asked {{$question['count']}} times</li>
                                        <ol type="A">
                                            @foreach($question['options'] as $option)
                                                <li
                                                        @if($option['correct'])
                                                            class="correct"
                                                        @endif

                                                >{{$option['name']}}</li>
                                                <li style="list-style-type:none">
                                                    @php
                                                        $picked = $stats['options'];
                                                        $p = $picked->where('bo',$option['id']);
                                                        $p = $p->values();
                                                    @endphp
                                                    @if($p->isNotEmpty())
                                                        {{ $p[0]['count']/$question['count']}}
                                                    @else
                                                        {{ 0 }}
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ol>
                                    @endforeach
                                </ol>
                                <a class="btn btn-primary"
                                   href="{{ url('/course/'.$test->course_id.'/banks/'.$bank.'/stats') }}">Bank
                                    Stats</a>
                            @endif
                        @endif

                        <a class="btn btn-primary"
                           href="{{ url('/course/'.$test->course_id.'/tests/'.$test->id.'/submissions') }}">View
                            Scores</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
