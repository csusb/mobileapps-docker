@extends('layouts.app')

@section('content')

    <?php
        $answers = str_split($test->answer_key);
    ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You Are Trying to Edit a Test
                    <ul>
                        <li>{{ $test->code }}</li>
                        <li>{{ $test->name }}</li>
                        <li>{{ $test->course }}</li>
                    </ul>

                    {{ Form::open() }}
                    @for ($i=0; $i < strlen($test->answer_key); $i++)
                                {{ $i+1 }}
                                {{ Form::label( $i,'A') }}
                                {{ Form::radio($i,'A',($answers[$i] == 'A'),['class'=>'ans','required']) }}
                                {{ Form::label($i,'B') }}
                                {{ Form::radio($i,'B',($answers[$i] == 'B'),['class'=>'ans','required']) }}
                                {{ Form::label($i,'C') }}
                                {{ Form::radio($i,'C',($answers[$i] == 'C'),['class'=>'ans','required']) }}
                                {{ Form::label($i,'D') }}
                                {{ Form::radio($i,'D',($answers[$i] == 'D'),['class'=>'ans','required']) }}
                                <br />

                    @endfor
                    {{ Form::submit('Save') }}
                    {{ Form::close() }}


                </div>
            </div>
        </div>
    </div>
</div>

@endsection

