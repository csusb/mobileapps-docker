@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tests</div>

                <div class="panel-body">

                    @if(count($students) < 1)
                        <p>There aren't any students registered for this class</p>
                    @else
                        <table class="testtable">
                            <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th></th>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr class="testrow">
                                    <td>{{ $student->stu_id }}</td>
                                    <td>{{ $student->name }}</td>
                                    <td> {!! Form::open(['method'=>'DELETE', 'route'=>'test.delete']) !!}
                                        {{ Form::hidden('id', $student->stu_id) }}
                                        <button data-toggle="tooltip" data-placement="top" class="delIcon" title="Delete" type="submit" onclick="return confirm('Are you sure you want to delete this item?');"><img src="{{ URL::asset('img/ic_delete_forever_black_48px.svg') }}'" class="delete icon"></img></button>
                                        {!! Form::close() !!}
                                    </td>

                                </tr>

                            @endforeach
                            </tbody>
                        </table>

                        <button class="newtest"><a href="{{ url('/add-student') }}">Add Student</a></button>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
