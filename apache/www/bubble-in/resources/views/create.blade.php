@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create A New Test</div>

                <div class="panel-body">
                    @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            <strong>{{$errors->first()}}</strong>
                            <a href="{{ url('course/'.$course.'/banks/'.session('bank')[0]) }}">View Bank</a>
                        </div>
                    @endif

                    {{ Form::open(['url' => '/course/'.$course.'/tests','method'=>'POST']) }}
                        {{ Form::hidden('cid',$course) }}
                        {{ Form::label('name', 'test name') }} <br />
                        {{ Form::text('name') }}
                    <br />

                    {{ Form::label('questions', 'number of questions') }} <br />
                    {{ Form::number('questions') }}
                    <br />

                    {{ Form::label('tests', 'number of tests') }} <br />
                    {{ Form::number('tests') }}
                    <br />

                    {{ Form::label('banks', 'Select a test bank') }} <br />
                    {{ Form::select('banks', $banks, null, ['placeholder' => 'None']) }}
                    <br />

                    {{ Form::label('sameQs', 'Use same questions on all tests') }} <br />
                    {{ Form::checkbox('sameQs','yes', false) }}
                    <br />

                    {{ Form::label('sameT', 'Generate Exact same test for all students') }} <br />
                    {{ Form::checkbox('sameT','yes', false) }}
                    <br />

                    {{ Form::label('sdate', 'start date') }} <br />
                    {{ Form::date('sdate')}}
                    <br />

                    {{ Form::label('stime', 'start time') }} <br />
                    {{ Form::time('stime')}}
                    <br />

                    {{ Form::label('edate', 'end date') }} <br />
                    {{ Form::date('edate') }}
                    <br />

                    {{ Form::label('etime', 'end time') }} <br />
                    {{ Form::time('etime') }}
                    <br />

                        {{ Form::submit('new test') }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
