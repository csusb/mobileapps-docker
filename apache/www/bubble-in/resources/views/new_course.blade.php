@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="heading-left">New Course</div>
                    <div class="heading-right">Create</div>
                </div>

                <div class="panel-body">

                    {{ Form::open() }}

                    {{ Form::label('code', 'Course Code') }}
                    {{ Form::text('code', null, ['class' => 'resetCourseCode']) }}
                    <br />

                    {{ Form::label('name', 'Course Name') }}
                    {{ Form::text('name', null) }}
                    <br/>

                    <button class="resetCourse btn btn-danger">Reset Course</button>
                    <br />

                    {{ Form::submit('Save') }}
                    {{ Form::close() }}


                </div>
            </div>
        </div>
    </div>
</div>

@endsection

