@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="heading-left">{{$name}} Test Bank</div>
                    </div>
                    <div class="panel-body">
                        <ol id="questions">
                            @foreach ($stats['questions'] as $question)
                                <li class="panel-title">{{$question['question']}} </li>
                                <li>Asked {{$question['count']}} times</li>
                                <ol type="A">
                                    @foreach($question['options'] as $option)
                                        <li>{{$option['name']}}</li>
                                        <li>
                                            @php
                                                $picked = $stats['options']->where('bo',$option['id']);
                                                $picked = $picked->values();
                                            @endphp
                                            @if($picked->isNotEmpty())
                                             {{ $picked[0]['count']/$question['count'] }}
                                            @else
                                            {{ 0 }}
                                            @endif
                                        </li>
                                    @endforeach
                                </ol>
                            @endforeach
                        </ol>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
